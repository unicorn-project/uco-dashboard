
## Set up

First, we need Node.JS. As it is available for many different plattforms and there are many installation steps, just leaving the link here: https://nodejs.org. You can obtain the latest stable Node.JS version there.

Node.JS ships with two terminal commands:
* `node` for executing the JavaScript
* `npm` for packet management. Similar to `apt-get` in linux, `npm` manages packets in Node.JS

We need Angular and we wand to access it from terminal, so we're going to install Angular CLI (command-line interface). Execute in terminal from any folder:
```
> npm install -g @angular/cli
```

Then switch to the folder of this repository and execute
```
> npm install
```
This downloads all dependencies of the project.

Then with
```
ng serve
```
the dashboard should be available at http://localhost:4200/

## Configuration

* To connect to the backend API, you need to insert the backend IP and port in the following two configuration files:
    * `src/assets/config/config.dev.json -> apiServer -> metadata` **AND** `src/app/shared/services/authentication.service.ts`
    * Note: `casmetadata` was used for storing of cards configuration in the backend, now this feature is removed
* Port Angular is listening to:
`--port` argument by `ng serve`. See https://stackoverflow.com/a/44941501 for more details

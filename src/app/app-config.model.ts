export interface IAppConfig {
    env: {
        name: string;
    };

    apiServer: {
        metadata: string;
        casmetadata: string;
    };

    devServer: {
        metadata: string;
        casmetadata: string;
        monitoring: string;
    };
}

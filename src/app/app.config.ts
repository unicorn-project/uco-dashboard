import { Injectable } from '@angular/core';
import { IAppConfig } from './app-config.model';
import { environment } from '../environments/environment';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class AppConfig {

    static settings: IAppConfig;

    constructor(private http: HttpClient) {}

    load() {
        const jsonFile = `assets/config/config.${environment.name}.json`;
        // const jsonFile = `assets/config/config.dev.json`;
        // console.log(jsonFile);

        return new Promise<void>((resolve, reject) => {
            this.http.get(jsonFile).toPromise().then((resp: IAppConfig) => {
                AppConfig.settings = <IAppConfig>resp;
                resolve();
            }).catch((resp: any) => {
                reject(`Could not load file '${jsonFile}': ${JSON.stringify(resp)}`);
            });
        });
    }

}

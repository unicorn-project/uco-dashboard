import { ComponentNodeCard } from 'src/app/shared/entities/ComponentNodeCard';

export interface ComponentNode {
    index: number;
    selfRef: ComponentNode;
    card: ComponentNodeCard;
    compInteraction: MyInterface;
}

export interface MyInterface {
    remove(index: number);
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentNodeCardComponent } from './component-node-card.component';

describe('ComponentNodeCardComponent', () => {
  let component: ComponentNodeCardComponent;
  let fixture: ComponentFixture<ComponentNodeCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentNodeCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentNodeCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

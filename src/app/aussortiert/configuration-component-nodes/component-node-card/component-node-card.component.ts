import { Component, OnInit, Input } from '@angular/core';
import { MyInterface } from './ComponentNodeCardInterface';
import { ComponentNodeCard } from 'src/app/shared/entities/ComponentNodeCard';

@Component({
  selector: 'app-component-node-card',
  templateUrl: './component-node-card.component.html',
  styleUrls: ['./component-node-card.component.scss']
})
export class ComponentNodeCardComponent implements OnInit {

  public index: number;
  public selfRef: ComponentNodeCardComponent;
  public compInteraction: MyInterface;
  public card: ComponentNodeCard;

  constructor() { }

  ngOnInit() {
    console.log(this.card);
  }

}

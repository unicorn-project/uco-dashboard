import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentSpawnerComponent } from './component-spawner.component';

describe('ComponentSpawnerComponent', () => {
  let component: ComponentSpawnerComponent;
  let fixture: ComponentFixture<ComponentSpawnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentSpawnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentSpawnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

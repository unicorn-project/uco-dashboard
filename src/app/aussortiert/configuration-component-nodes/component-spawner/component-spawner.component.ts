import { Component, OnInit, ViewChild, ViewContainerRef, Input, ComponentFactoryResolver, ComponentRef } from '@angular/core';
import { ComponentNodeCard } from 'src/app/shared/entities/ComponentNodeCard';
import { ComponentNodeCardComponent } from '../component-node-card/component-node-card.component';
import { ComponentNode } from '../component-node-card/ComponentNodeCardInterface';

@Component({
  selector: 'app-component-spawner',
  templateUrl: './component-spawner.component.html',
  styleUrls: ['./component-spawner.component.scss']
})
export class ComponentSpawnerComponent implements OnInit {

@ViewChild('componentNodeCards', {read: ViewContainerRef}) VCR: ViewContainerRef;
@Input() card: ComponentNodeCard;

componentIndex = 0;
componentReferences = [];

  constructor(  private CFR: ComponentFactoryResolver) { }

  ngOnInit() {
    this.loadComponent();
  }

  loadComponent () {

    const compfactory = this.CFR.resolveComponentFactory(ComponentNodeCardComponent);

    const compRef: ComponentRef<ComponentNode> = this.VCR.createComponent(compfactory);

    const currentComponent = compRef.instance;

    currentComponent.selfRef = currentComponent;
    currentComponent.index = ++this.componentIndex;
    currentComponent.card = this.card;
    currentComponent.compInteraction = this;

    this.componentReferences.push(currentComponent);
  }

  remove(index: number) {
    if (this.VCR.length < 1) {
      return;
    }

    const compRef = this.componentReferences.filter(x => x.instance.index === index)[0];

    const vcrIndex: number = this.VCR.indexOf(compRef);

    this.VCR.remove(vcrIndex);

    this.componentReferences = this.componentReferences.filter(x => x.instance.index !== index);

  }

}

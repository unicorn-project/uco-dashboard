import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurationComponentNodesComponent } from './configuration-component-nodes.component';

describe('ConfigurationComponentNodesComponent', () => {
  let component: ConfigurationComponentNodesComponent;
  let fixture: ComponentFixture<ConfigurationComponentNodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigurationComponentNodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationComponentNodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

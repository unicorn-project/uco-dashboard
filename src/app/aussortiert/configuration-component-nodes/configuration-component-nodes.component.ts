import { Component, OnInit, Input } from '@angular/core';
import { ComponentNodeCard } from 'src/app/shared/entities/ComponentNodeCard';
import { ComponentNode } from '../../shared/entities/ComponentNode';

@Component({
  selector: 'app-configuration-component-nodes',
  templateUrl: './configuration-component-nodes.component.html',
  styleUrls: ['./configuration-component-nodes.component.scss']
})
export class ConfigurationComponentNodesComponent implements OnInit {

  @Input() compNodesList: ComponentNode[];

  cards: ComponentNodeCard[] = [];

  constructor() { }

  ngOnInit() {
    // loop over all component nodes to get the cards
    console.log(this.compNodesList);
    console.log(this.compNodesList.length);
    for (let i = 0; i < this.compNodesList.length; i++) {
      const compNode: ComponentNode = this.compNodesList[i];
      console.log(compNode);
    }
    console.log(this.cards);
  }

}

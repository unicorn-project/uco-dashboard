import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigurationComponentNodesRoutingModule } from './configuration-component-nodes-routing.module';
import { ConfigurationComponentNodesComponent } from './configuration-component-nodes.component';
import { MaterialModule } from 'src/app/material.module';
import { ComponentSpawnerComponent } from './component-spawner/component-spawner.component';
import { ComponentNodeCardComponent } from './component-node-card/component-node-card.component';

@NgModule({
  declarations: [ConfigurationComponentNodesComponent, ComponentSpawnerComponent, ComponentNodeCardComponent],
  imports: [
    CommonModule,
    ConfigurationComponentNodesRoutingModule,
    MaterialModule
  ],
  exports: [
    ConfigurationComponentNodesComponent
  ],
  entryComponents: [
    ComponentNodeCardComponent
  ]
})
export class ConfigurationComponentNodesModule { }

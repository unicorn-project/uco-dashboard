import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppDetailViewComponent } from './app-detail-view.component';

describe('AppDetailViewComponent', () => {
  let component: AppDetailViewComponent;
  let fixture: ComponentFixture<AppDetailViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppDetailViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppDetailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApplicationService } from '../../../shared/services/application.service';
import { Application, ApplicationOwner } from '../../../shared/entities/Application';
import { TreeNode } from 'primeng/primeng';
import { MatDialogRef, MatDialog, MatTableDataSource } from '../../../../../node_modules/@angular/material';
import { EditAppSettingsDialogComponent } from '../../dialogues/edit-app-settings-dialog/edit-app-settings-dialog.component';
import { LinkCompToAppDialogComponent } from '../../dialogues/link-comp-to-app-dialog/link-comp-to-app-dialog.component';
import { ComponentsService } from 'src/app/shared/services/components.service';
import { ApplicationInstance } from '../../../shared/entities/ApplicationInstance';
import { ComponentNode } from '../../../shared/entities/ComponentNode';
import { AppComponent as ApplicationComponent } from '../../../shared/entities/AppComponent';
import { InstanceService } from 'src/app/shared/services/instance.service';

@Component({
  selector: 'app-app-detail-view',
  templateUrl: './app-detail-view.component.html',
  styleUrls: ['./app-detail-view.component.scss'],
  providers: [ApplicationService, ComponentsService, InstanceService]
})
export class AppDetailViewComponent implements OnInit, OnDestroy {

  appName: string;
  appID: number;
  sub: any;
  app: Application;
  applist: any = [];
  chart = [];
  chartData = [];
  chartLabels = [];
  treeData: TreeNode[];
  // only names are needed for component list at the moment
  complist = [];
  componentNodes: ComponentNode[] = [];
  appOwnerList: ApplicationOwner[];
  noCompLinkedMessage = false;
  noInstancesAvailableMessage = false;
  componentDataSource = [];
  applicationInstanceDataSource: ApplicationInstance[] = [];
  collectAppInstances = [];
  dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['identifier', 'name', 'component', 'details'];
  displayedInstancesColumns: string[] = ['identifier', 'name', 'status', 'details'];
  record: object;

  editAppSettingsDialogRef: MatDialogRef<EditAppSettingsDialogComponent>;
  linkComptToAppDialogRef: MatDialogRef<LinkCompToAppDialogComponent>;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private appService: ApplicationService,
              private componentService: ComponentsService,
              private instanceService: InstanceService,
              private dialog: MatDialog) { }

  ngOnInit() {

      this.sub = this.route.params.subscribe(params => {
        this.appID = params['appID'];
    });

    this.appService.getAppById(this.appID).subscribe(
      application => {
        this.app = application;
        this.appName = application.name;
        this.getComponentNodes();

        this.instanceService.getApplicationInstancesByApplicationId(this.appID).subscribe(applicationInstances => {
          this.applicationInstanceDataSource = applicationInstances;
        });
      }
    );
  }

  getComponentNodes() {
    this.componentNodes = this.app.componentNodes;

    if (this.app.componentNodes.length === 0) {
      this.noCompLinkedMessage = true;
    }
  }

  // Detail View for Components
  detailCompView(componentId: number) {
    this.router.navigate(['/dashboard/components/details', componentId ]);
  }

  openCreateNewInstance(app: Application) {
    this.router.navigate(['/dashboard/create-instance', app.name]);
  }

  openDetailInstanceView(instance: ApplicationInstance) {
      this.router.navigate(['/dashboard/instances', instance.applicationInstanceID]);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}

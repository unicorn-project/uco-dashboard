import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationsComponent } from './applications.component';
import { AppConfigurationComponent } from './app-configuration/app-configuration.component';
import { AppDetailViewComponent } from './app-detail-view/app-detail-view.component';
import { CreateApplicationComponent } from './create-application/create-application.component';

const routes: Routes = [
  { path: '', component: ApplicationsComponent },
  { path: 'create', component: CreateApplicationComponent },
  { path: 'details/:appID', component: AppDetailViewComponent },
  { path: ':appName/config', component: AppConfigurationComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationsRoutingModule { }

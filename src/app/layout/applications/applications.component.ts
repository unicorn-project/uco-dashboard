import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import { ApplicationService } from '../../shared/services/application.service';
import { Application, ApplicationOwner } from '../../shared/entities/Application';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MatDialog, MatSnackBar, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { filter } from 'rxjs/operators';
import { ComponentsService } from 'src/app/shared/services/components.service';


@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.scss'],
  providers: [ApplicationService]
})
export class ApplicationsComponent implements OnInit {

  appDataTable = [];
  appDataSource = new MatTableDataSource<Application>();
  publicAppDataTable = [];
  privateAppDataTable = [];
  displayedColumns: string[] = ['name', 'dateCreated', 'publicApplication', 'actions'];

  possibilities = new FormControl();
  possibilityList: string[] = ['all', 'private', 'public'];
  showPublicApps = new FormControl(false);
  showPrivateApps = new FormControl(false);
  showAllApps = new FormControl(true);

  componentList = [];

  snackAppCreatedMessage: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(  private appService: ApplicationService,
                private componentService: ComponentsService,
                private router: Router,
                private dialog: MatDialog,
                private snackbar: MatSnackBar) {}

  ngOnInit() {

    // test
    // get application data
    this.getApplicationDataTable();
    // get componentList for new app dialog
    this.componentService.getComponents().subscribe(components => {
      // create component List
      this.componentList = components;
    });
  }

  getApplicationDataTable() {
    this.appService.getApps().subscribe(applications => {
      this.appDataTable = applications;
      // change visibility entries
      for (let i = 0; i < this.appDataTable.length; i++) {
        this.appDataTable[i].id = this.appDataTable[i].id;
        this.appDataTable[i].dateCreated = this.appDataTable[i].dateCreated.toString().slice(0, 19);
        this.appDataTable[i].lastModified = this.appDataTable[i].lastModified.toString().slice(0, 19);
        if (this.appDataTable[i].publicApplication) {
          this.appDataTable[i].publicApplication = 'public';
        } else {
          this.appDataTable[i].publicApplication = 'private';
        }
      }

      this.appDataSource.data = this.appDataTable;
      this.appDataSource.paginator = this.paginator;
      this.appDataSource.sort = this.sort;
    });
  }

  openDetailView(app: Application) {
    this.router.navigate(['/dashboard/application/details', app.id]);
  }

  createApplicationInstance(application: Application): void {
    this.router.navigate(['/dashboard/create-instance/', application.name])
  }

  delete(app: Application) {
    this.appService.deleteApp(app.id).subscribe();

    this.appDataSource.data = this.appDataSource.data.filter((application: Application) => application.id !== app.id);
  }

  applyTableFilter(filterValue: string) {
    this.appDataSource.filter = filterValue.trim().toLowerCase();
  }

  openNewAppDialog() {
    this.router.navigate(['/dashboard/application/create']);
  }
}

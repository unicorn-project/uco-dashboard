import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ApplicationsRoutingModule } from './applications-routing.module';
import { ApplicationsComponent } from './applications.component';
import { MaterialModule } from '../../material.module';
import { ChartsModule } from 'ng2-charts';
import { TreeModule } from 'primeng/primeng';
import { AppConfigurationComponent } from './app-configuration/app-configuration.component';
import { MonitoringModule } from '../monitoring/monitoring.module';
import { MonitoringComponent } from '../monitoring/monitoring.component';
import { AppDetailViewComponent } from './app-detail-view/app-detail-view.component';
import { CreateApplicationComponent } from './create-application/create-application.component';

@NgModule({
  imports: [
    CommonModule,
    ApplicationsRoutingModule,
    MonitoringModule,
    MaterialModule,
    ChartsModule,
    ReactiveFormsModule,
    TreeModule
  ],
  declarations: [ApplicationsComponent, AppConfigurationComponent, AppDetailViewComponent, CreateApplicationComponent]
})
export class ApplicationsModule { }

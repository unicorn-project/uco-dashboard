import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, FormArray } from '@angular/forms';
import { difference } from 'lodash';
import { forkJoin } from 'rxjs';
import { AppComponent as ApplicationComponent } from '../../../shared/entities/AppComponent';
import { ApplicationService } from '../../../shared/services/application.service';
import { ComponentsService } from '../../../shared/services/components.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { RestResponseSPA } from 'src/app/shared/entities/RestResponseSPA';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-create-application',
  templateUrl: './create-application.component.html',
  styleUrls: ['./create-application.component.scss'],
  providers: [ApplicationService, ComponentsService]
})
export class CreateApplicationComponent implements OnInit {

  components: ApplicationComponent[];
  selectedComponents: ApplicationComponent[] = [];
  selectedComponentsHavingRequiredInterfaces: ApplicationComponent[] = [];

  componentsSatisfyingInterfaceCachedByIds: { [componentId: number]: { [graphLinkId: number]: ApplicationComponent[] } } = {};
  selectedComponentsSatisfyingInterface: { [componentId: number]: { [graphLinkId: number]: ApplicationComponent } } = {};

  form: FormGroup;

  private componentsCachedById: { [componentId: number]: ApplicationComponent } = {};
  private previousSelectedComponentsValue: ApplicationComponent[] = [];

  constructor(
    private applicationService: ApplicationService,
    private componentService: ComponentsService,
    private router: Router,
    private snackbar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      appName: new FormControl(''),
      selectedComponents: new FormControl([]),
      isPublic: new FormControl(false),
      test: new FormControl(),
    });

    this.componentService.getComponents().subscribe(components => {
      this.components = components;
    });
  }

  selectedComponentsValueChanged(newSelectedComponentsValue?: ApplicationComponent[]): void {
    if (!newSelectedComponentsValue) {
      return;
    }

    const addedComponents: ApplicationComponent[] = difference(newSelectedComponentsValue, this.previousSelectedComponentsValue);
    if (addedComponents.length > 0) {
      // Means that a component was added to the selection
      const addedComponent = addedComponents[0];

      if (this.componentsCachedById[addedComponent.id]) {
        if (this.componentsCachedById[addedComponent.id].requiredInterfaces.length > 0) {
          this.selectedComponentsHavingRequiredInterfaces.push(this.componentsCachedById[addedComponent.id]);
        }
      } else {
        this.componentService.getComponentById(addedComponent.id).subscribe(
          component => {
            if (component.requiredInterfaces.length > 0) {
              // Paralellize all the requests
              forkJoin(
                component.requiredInterfaces.map(
                  graphLink => {
                    const componentId = component.id;
                    const graphLinkId = graphLink.graphLinkID;
                    const interfaceId = graphLink.interfaceObj.interfaceID;

                    return this.componentService.getComponentsSatisfyingInterface(componentId, interfaceId)
                      .map(
                        results => ({
                          componentId,
                          graphLinkId,
                          components: results,
                        })
                      );
                  }
                )
              ).subscribe(
                results => {
                  results.map(
                    result => {
                      if (!this.componentsSatisfyingInterfaceCachedByIds[result.componentId]) {
                        this.componentsSatisfyingInterfaceCachedByIds[result.componentId] = {};
                        this.selectedComponentsSatisfyingInterface[result.componentId] = {};
                      }

                      this.componentsSatisfyingInterfaceCachedByIds[result.componentId][result.graphLinkId] = result.components;
                    }
                  );

                  // Only show the component when all options were loaded
                  this.selectedComponentsHavingRequiredInterfaces.push(component);
                }
              );
            }

            this.componentsCachedById[component.id] = component;
          });
      }
    } else {
      const removedComponents: ApplicationComponent[] = difference(this.previousSelectedComponentsValue, newSelectedComponentsValue);
      if (removedComponents.length > 0) {
        // Means that a component was removed from the selection
        const removedComponent = removedComponents[0];

        this.selectedComponentsHavingRequiredInterfaces = this.selectedComponentsHavingRequiredInterfaces.filter(
          component => !this.areComponentsEqual(component, removedComponent)
        );

        // Unselect this component everywhere it was selected
        Object.keys(this.selectedComponentsSatisfyingInterface).forEach(
          componentId => {
            Object.keys(this.selectedComponentsSatisfyingInterface[componentId]).forEach(
              graphLinkId => {
                if (this.areComponentsEqual(this.selectedComponentsSatisfyingInterface[componentId][graphLinkId], removedComponent)) {
                  this.selectedComponentsSatisfyingInterface[componentId][graphLinkId] = undefined;
                }
              }
            );
          }
        );
      }
    }

    this.previousSelectedComponentsValue = newSelectedComponentsValue;
  }

  satisfyingComponentValueChanged(satisfyingComponent?: ApplicationComponent): void {
    if (!satisfyingComponent) {
      return;
    }

    if (!this.selectedComponents.find(component => this.areComponentsEqual(component, satisfyingComponent))) {
      this.selectedComponents.push(satisfyingComponent);

      // No idea why two-way binding is not working here, so needed to trigger the update manually
      this.form.controls['selectedComponents'].setValue(this.selectedComponents);
    }
  }

  submit(form: AbstractControl): void {
    const application = form.value;
    application.graphLinkNodes = [];

    try {
      Object.keys(this.selectedComponentsSatisfyingInterface).forEach(
        requiringComponentId => {
          Object.keys(this.selectedComponentsSatisfyingInterface[requiringComponentId]).forEach(
            graphLinkId => {
              const graphLinkNode = {
                componentNodeFrom: this.componentToComponentNode(this.componentsCachedById[requiringComponentId]),
                componentNodeTo: this.componentToComponentNode(this.selectedComponentsSatisfyingInterface[requiringComponentId][graphLinkId]),
                graphLink: {
                  graphLinkID: graphLinkId
                }
              };

              application.graphLinkNodes.push(graphLinkNode);
            }
          );
        }
      );
    } catch (e) {
      this.snackbar.open(
        'Cannot save the application. Perhaps not all interfaces were satisfied',
        '',
        {
          duration: 3000
        }
      );

      return;
    }

    this.applicationService.saveApp(form.value).subscribe(
      () => {
        this.router.navigate(['/dashboard/application']);

        this.snackbar.open(
          'Application successfully created',
          '',
          {
            duration: 3000
          }
        );
      },
      (error: HttpErrorResponse) => {
        this.snackbar.open(
          (error.error as RestResponseSPA<void>).message,
          '',
          {
            duration: 3000
          }
        );
      }
    );
  }

  areComponentsEqual(componentA: ApplicationComponent, componentB: ApplicationComponent): boolean {
    return componentA && componentB ? componentA.id === componentB.id : componentA === componentB;
  }

  componentToComponentNode(component: any) {
    return {
      name: component.name,
      component: {
        id: component.id
      }
    };
  }

  cancel(): void {
    this.router.navigate(['/dashboard/application']);
  }
}

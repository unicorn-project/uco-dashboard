import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsComponent } from '../components/components.component';
import { DetailViewComponent } from './detail-view/detail-view.component';
import { EditViewComponent } from './edit-view/edit-view.component';
import { CreateNewComponentComponent } from './create-new-component/create-new-component.component';

const routes: Routes = [
  { path: '', component: ComponentsComponent },
  { path: 'details/:compID', component: DetailViewComponent},
  { path: 'edit/:compID', component: EditViewComponent},
  { path: 'create-component', component: CreateNewComponentComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ApplicationService } from '../../shared/services/application.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent as ApplicationComponent } from '../../shared/entities/AppComponent';
import { MatDialogRef, MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { AddCompDialogComponent } from '../dialogues/add-comp-dialog/add-comp-dialog.component';
import { filter } from '../../../../node_modules/rxjs/operators';
import { ComponentsService } from 'src/app/shared/services/components.service';

@Component({
  selector: 'app-components',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.scss'],
  providers: [ApplicationService]
})
export class ComponentsComponent implements OnInit, OnDestroy {

  // page parameters
  sub: any;
  componentName: string;

  // component list
  componentList = [];
  noComponentsAvailableMessage = false;

  dataSource = new MatTableDataSource();
  displayedColumns = ['ComponentName', 'DateCreated', 'actions'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  // add new component dialog
  addNewComponentDialogRef: MatDialogRef<AddCompDialogComponent>;

  constructor(private route: ActivatedRoute,
              private compService: ComponentsService,
              private dialog: MatDialog,
              private router: Router) { }

  ngOnInit() {

    // parameter settings
    this.sub = this.route.params.subscribe(params => {
      this.componentName = params['compName'];
  });
    // collect all components
    this.compService.getComponents().subscribe(components => {
      this.componentList = components;
      // change date format
      this.componentList.forEach(component => {
        component.dateCreated = component.dateCreated.toString().slice(0, 19);
      });
      this.dataSource.data = this.componentList;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      // print message if there are no components available in the database
      if (this.componentList.length === 0) {
        this.noComponentsAvailableMessage = true;
      }
    });
  }

  // filter table
  applyTableFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openCreateNewComponentView() {
    this.router.navigate(['/dashboard/components/create-component']);
  }

  // add new component
  openAddComponentDialog() {
    this.addNewComponentDialogRef = this.dialog.open(AddCompDialogComponent, {
      hasBackdrop: true,
      width: '90%',
    });

    this.addNewComponentDialogRef
      .afterClosed()
      .pipe(filter(componentName => componentName))
      .subscribe(result => {
        // save component in database
        this.compService.addComponent(result).subscribe(() => {
          // retrieve data from database
          this.compService.getComponents().subscribe(components => {
            this.componentList = components;
            // change date format
            this.componentList.forEach(component => {
              component.dateCreated = component.dateCreated.toString().slice(0, 19);
            });
            this.dataSource.data = this.componentList;
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          });
      });
    });
  }

  openDetailView(component: ApplicationComponent) {
    this.router.navigate(['/dashboard/components/details', component.id]);
  }

  openEditView(component: ApplicationComponent) {
    this.router.navigate(['/dashboard/components/edit', component.id]);
  }

  delete(componentToDelete: ApplicationComponent) {
    this.compService.delete(componentToDelete.id).subscribe();

    this.dataSource.data = this.dataSource.data.filter((component: ApplicationComponent) => component.id !== componentToDelete.id);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}

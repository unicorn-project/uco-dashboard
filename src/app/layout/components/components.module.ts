import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsRoutingModule } from './components-routing.module';
import { ComponentsComponent } from './components.component';
import { MaterialModule } from '../../material.module';
import { DetailViewComponent } from './detail-view/detail-view.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditViewComponent } from './edit-view/edit-view.component';
import { CreateNewComponentComponent } from './create-new-component/create-new-component.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  declarations: [ComponentsComponent, DetailViewComponent, EditViewComponent, CreateNewComponentComponent],
  exports: [
    DetailViewComponent
  ]
})
export class ComponentsModule { }

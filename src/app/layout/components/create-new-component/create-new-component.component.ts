import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Interface } from 'src/app/shared/entities/Interfaces';
import { ComponentsService } from 'src/app/shared/services/components.service';
import { InterfaceService } from 'src/app/shared/services/interface.service';
import { HttpErrorResponse } from '@angular/common/http';
import { RestResponseSPA } from 'src/app/shared/entities/RestResponseSPA';
import { MatSnackBar } from '@angular/material';

export interface Architecture {
  name: string;
}

export interface ElastictiyProfile {
  scaling: string;
  name: string;
}

export interface HyperVisor {
  name: string;
}

export interface InterfaceType {
  name: string;
  type: string;
}

@Component({
  selector: 'app-create-new-component',
  templateUrl: './create-new-component.component.html',
  styleUrls: ['./create-new-component.component.scss'],
  providers: [ComponentsService, InterfaceService]
})
export class CreateNewComponentComponent implements OnInit {

  form: FormGroup;
  environmentForm: FormGroup;

  architectureList: Architecture[] = [
    {name: 'x86'},
    {name: 'ARM'}
  ];

  hyperVisorList: HyperVisor[] = [
    {
      name: 'ESXI'
    },
    {
      name: 'KVM'
    },
    {
      name: 'KEN'
    }
  ];

  elasticityProfileList: ElastictiyProfile[] = [
    {
      scaling: 'NONE',
      name: 'None'
    },
    {
      scaling: 'HORIZONTAL',
      name: 'Horizontal'
    },
    {
      scaling: 'VERTICAL',
      name: 'Vertical'
    },
    {
      scaling: 'LAMBDA',
      name: 'Lambda Function'
    },
  ];

  interfaceTypeList: InterfaceType[] = [{
    name: 'Core',
    type: 'CORE'
  }, {
    name: 'Acces',
    type: 'ACCESS'
  }];

  interfaceProtocolList: string[] = ['TCP', 'UDP', 'TCP/UDP'];
  interfaceList: Interface[];

  numberEnvVariables = 0;
  environmentalVariablesData = [];
  envVarColumnsToDisplay: string[] = ['key', 'value', 'delete'];

  selectedKey: string;
  selectedValue: string;

  constructor(
    private formBuilder: FormBuilder,
    private compService: ComponentsService,
    private interfaceService: InterfaceService,
    private router: Router,
    private snackbar: MatSnackBar,
  ) { }

  ngOnInit() {// create form on dialog to retrieve data from the dialog
    this.form = this.formBuilder.group({});
    this.form.addControl('componentName', this.formBuilder.control(''));
    this.form.addControl('architecture', this.formBuilder.control(''));
    this.form.addControl('elastProfile', this.formBuilder.control(''));
    this.form.addControl('dockerRegistry', this.formBuilder.control(''));
    this.form.addControl('dockerImage', this.formBuilder.control(''));
    this.form.addControl('dockerCredentialsUsing', this.formBuilder.control(false));
    this.form.addControl('dockerUsername', this.formBuilder.control(''));
    this.form.addControl('dockerPassword', this.formBuilder.control(''));
    this.form.addControl('vCPUs', this.formBuilder.control(1));
    this.form.addControl('ram', this.formBuilder.control(2048));
    this.form.addControl('storage', this.formBuilder.control(500));
    this.form.addControl('hyperVisorType', this.formBuilder.control(''));
    this.form.addControl('publicComponent', this.formBuilder.control(false));
    this.form.addControl('gpuEnabled', this.formBuilder.control(false));
    this.form.addControl('healthHttp', this.formBuilder.control(''));
    this.form.addControl('healthCommand', this.formBuilder.control(''));
    this.form.addControl('healthTimeInterval', this.formBuilder.control(''));
    this.form.addControl('environmentalVariables', this.formBuilder.array([this.initEnvVar()]));
    this.form.addControl('addedExposedInterfaces', this.formBuilder.array([this.initExposedInterfaces()]));
    this.form.addControl('exposedInterfaces', this.formBuilder.control([]));
    this.form.addControl('addedRequiredInterfaces', this.formBuilder.array([this.initRequiredInterfaces()]));
    this.form.addControl('monitoringPlugins', this.formBuilder.control([]));
    this.form.addControl('volumes', this.formBuilder.array([this.initVolumes()]));
    this.form.addControl('devices', this.formBuilder.array([this.initDevices()]));
    this.form.addControl('addedLables', this.formBuilder.array([this.initLables()]));

    this.interfaceService.getInterfaces().subscribe(interfaces => {
      this.interfaceList = interfaces;
    });
  }
  // environmental variables
  initEnvVar() {
    return this.formBuilder.group({
      key: '',
      value: ''
    });
  }
  addEnvVar() {
    const variables = <FormArray>this.form.controls['environmentalVariables'];
    variables.push(this.initEnvVar());
  }
  removeEnvVar(i: number) {
    const variables = <FormArray>this.form.controls['environmentalVariables'];
    variables.removeAt(i);
  }
  // exposed interfaces
  initExposedInterfaces() {
    return this.formBuilder.group({
      name: '',
      port: '',
      interfaceType: '',
      transmissionProtocol: ''
    });
  }
  addExposedInterface() {
    const variables = <FormArray>this.form.controls['addedExposedInterfaces'];
    variables.push(this.initExposedInterfaces());
  }
  removeExposedInterface(i: number) {
    const variables = <FormArray>this.form.controls['addedExposedInterfaces'];
    variables.removeAt(i);
  }
  // required interfaces
  initRequiredInterfaces() {
    return this.formBuilder.group({
      name: '',
      port: '',
      interfaceType: '',
      transmissionProtocol: ''
    });
  }
  addRequiredInterface() {
    const variables = <FormArray>this.form.controls['addedRequiredInterface'];
    variables.push(this.initEnvVar());
  }
  removeRequiredInterface(i: number) {
    const variables = <FormArray>this.form.controls['addedRequiredInterface'];
    variables.removeAt(i);
  }
  // volumes
  initVolumes() {
    return this.formBuilder.group({
      name: ''
    });
  }
  addVolume() {
    const variables = <FormArray>this.form.controls['volumes'];
    variables.push(this.initEnvVar());
    console.log(this.form);
  }
  removeVolume(i: number) {
    const variables = <FormArray>this.form.controls['volumes'];
    variables.removeAt(i);
  }
  // devices
  initDevices() {
    return this.formBuilder.group({
      key: '',
      value: ''
    });
  }
  addDevice() {
    const variables = <FormArray>this.form.controls['devices'];
    variables.push(this.initEnvVar());
    console.log(this.form);
  }
  removeDevice(i: number) {
    const variables = <FormArray>this.form.controls['devices'];
    variables.removeAt(i);
  }
  // lables
  initLables() {
    return this.formBuilder.group({
      name: ''
    });
  }
  addLable() {
    const variables = <FormArray>this.form.controls['volumes'];
    variables.push(this.initEnvVar());
    console.log(this.form);
  }
  removeLable(i: number) {
    const variables = <FormArray>this.form.controls['volumes'];
    variables.removeAt(i);
  }

  submit(form: FormGroup) {
     // add new component
     const newComp = {
      architecture: form.controls['architecture'].value.name,
      componentName: form.controls['componentName'].value,
      elastProfile: form.controls['elastProfile'].value.scaling,
      publicComponent: form.controls['publicComponent'].value,
      dockerRegistry: form.controls['dockerRegistry'].value,
      dockerImage: form.controls['dockerImage'].value,
      dockerCustomRegistry: true,
      dockerCredentialsUsing: form.controls['dockerCredentialsUsing'].value,
      dockerUsername: form.controls['dockerUsername'].value,
      dockerPassword: form.controls['dockerPassword'].value,
      gpuEnabled: form.controls['gpuEnabled'].value,
      hyperVisorType: form.controls['hyperVisorType'].value.name,
      ram: form.controls['ram'].value,
      storage: form.controls['storage'].value,
      vCPUs: form.controls['vCPUs'].value,
      healthCommand: form.controls['healthCommand'].value,
      healthHttp: form.controls['healthHttp'].value,
      healthTimeInterval: form.controls['healthTimeInterval'].value,
      environmentalVariables: (form.controls['environmentalVariables'] as FormArray).controls.map(formGroup => formGroup.value)
        .filter(environmentalVariable => environmentalVariable.key && environmentalVariable.value),
      exposedInterfaces: (form.controls['addedExposedInterfaces'] as FormArray).controls.map(formGroup => formGroup.value)
        .filter(exposedInterface => exposedInterface.name && exposedInterface.port && exposedInterface.interfaceType && exposedInterface.transmissionProtocol),
    };

    this.compService.addComponent(newComp).subscribe(
      () => {
        this.router.navigate(['/dashboard/components']);

        this.snackbar.open(
          'Component successfully created',
          '',
          {
            duration: 3000
          }
        );
      },
      (error: HttpErrorResponse) => {
        this.snackbar.open(
          (error.error as RestResponseSPA<void>).message,
          '',
          {
            duration: 3000
          }
        );
      }
    );
  }

  cancel(): void {
    this.router.navigate(['/dashboard/components']);
  }
}

import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationService } from '../../../shared/services/application.service';
import { AppComponent } from '../../../shared/entities/AppComponent';
import { ComponentsService } from 'src/app/shared/services/components.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss'],
  providers: [ApplicationService]
})
export class DetailViewComponent implements OnInit, OnDestroy {

  encapsulation: ViewEncapsulation.None;

  sub: any;
  componentName: string;
  componentID: number;
  linkedAppDataSource: any;
  noIconAvailable = false;
  // component data
  component: AppComponent;

  // data sources for forms
  generalData: {
    dateCreated: string;
    lastModified: string;
    isPublic: string;
  };
  architecturalData: {
    architecture: string;
    scaling: string;
    dockerImage: string;
    dockerRegistry: string;
  };
  iconData: {
    fileName: string;
    path: string;
    base64: string;
    content: string;
    contentType: string;
  };

  requirementsData: {
    vCPUs: number;
    ram: number;
    storage: number;
    hypervisorType: string;
    gpuEnabled: string;
  };

  healthData: {
    healthHttp: string;
    healthCommand: string;
    healthTimeInterval: string;
  };

  constructor(private route: ActivatedRoute,
              private router: Router,
              private componentService: ComponentsService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.componentID = params['compID'];
    });

    this.generalData = {
      dateCreated: null,
      lastModified: null,
      isPublic: null
    };

    this.architecturalData = {
      architecture: null,
      scaling: null,
      dockerImage: null,
      dockerRegistry: null
    };

    this.iconData = {
      fileName: null,
      path: null,
      base64: null,
      content: null,
      contentType: null
    };

    this.requirementsData = {
      vCPUs: null,
      ram: null,
      storage: null,
      hypervisorType: null,
      gpuEnabled: null
    };

    this.healthData = {
      healthCommand: null,
      healthHttp: null,
      healthTimeInterval: null
    }

    // get information about the application
    this.component = new AppComponent(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    this.componentService.getComponentById(this.componentID).subscribe(component => {

      this.component = component;
      this.componentName = component.name;

      // create general Data
      this.generalData = {
          dateCreated: component.dateCreated.toString().slice(0, 19),
          lastModified: component.lastModified.toString().slice(0, 19),
          isPublic: 'public'
        };

      if (!component.publicComponent) {
        this.generalData.isPublic = 'private';
      }

      // create architecture data
      this.architecturalData = {
        architecture: component.architecture,
        scaling: component.scaling,
        dockerImage: component.dockerImage,
        dockerRegistry: component.dockerRegistry
      };
      // create icon data
      this.iconData = {
        fileName: component.iconFilename,
        path: component.iconPath,
        base64: component.iconBase64,
        content: component.iconContent,
        contentType: component.iconContentType
      };

      if ( this.iconData.fileName === null &&
           this.iconData.path === null &&
           this.iconData.base64 === null &&
           this.iconData.content === null &&
           this.iconData.contentType === null) {
            this.noIconAvailable = true;
          }

this.requirementsData = {gpuEnabled: component.requirement.gpuRequired ? 'yes' : 'no', ...component.requirement};

        this.healthData = {
          healthCommand: component.healthCheck.args,
          healthHttp: component.healthCheck.httpURL,
          healthTimeInterval: component.healthCheck.interval
        };
      });
  }

  openEditComponentPage() {
    this.router.navigate(['dashboard/components/edit/', this.componentID]);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}

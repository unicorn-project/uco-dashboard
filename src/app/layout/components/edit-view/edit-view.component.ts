import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Architecture, HyperVisor, ElastictiyProfile } from '../../dialogues/add-comp-dialog/add-comp-dialog.component';
import { InterfaceType } from '../create-new-component/create-new-component.component';
import { Interface } from 'src/app/shared/entities/Interfaces';
import { ComponentsService } from 'src/app/shared/services/components.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from 'src/app/shared/entities/AppComponent';
import { InterfaceService } from 'src/app/shared/services/interface.service';
import { GraphLink } from 'src/app/shared/entities/GraphLinks';
import { MatSnackBar } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';
import { RestResponseSPA } from 'src/app/shared/entities/RestResponseSPA';

@Component({
  selector: 'app-edit-view',
  templateUrl: './edit-view.component.html',
  styleUrls: ['./edit-view.component.scss'],
  providers: [ComponentsService, InterfaceService]
})
export class EditViewComponent implements OnInit {

  componentID: number;
  component: AppComponent;
  form: FormGroup;
  architectureList: Architecture[] = [
    {name: 'X86'},
    {name: 'ARM'}
  ];

  hyperVisorList: HyperVisor[] = [
    {
      name: 'ESXI'
    },
    {
      name: 'KVM'
    },
    {
      name: 'KEN'
    }
  ];

  elasticityProfileList: ElastictiyProfile[] = [
    {
      scaling: 'NONE',
      name: 'None'
    },
    {
      scaling: 'HORIZONTAL',
      name: 'Horizontal'
    },
    {
      scaling: 'VERTICAL',
      name: 'Vertical'
    },
    {
      scaling: 'LAMBDA',
      name: 'Lambda Function'
    },
  ];

  interfaceTypeList: InterfaceType[] = [{
    name: 'Core',
    type: 'CORE'
  }, {
    name: 'Acces',
    type: 'ACCESS'
  }];

  interfaceProtocolList: string[] = ['TCP', 'UDP', 'TCP/UDP'];
  interfaceList: Interface[];

  private existingExposedInterfaces: Interface[];
  private existingRequiredInterfaceGraphLinks: GraphLink[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private componentService: ComponentsService,
    private interfaceService: InterfaceService,
    private snackbar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({});
    this.form.addControl('componentName', this.formBuilder.control(''));
    this.form.addControl('architecture', this.formBuilder.control(''));
    this.form.addControl('elastProfile', this.formBuilder.control(''));
    this.form.addControl('dockerRegistry', this.formBuilder.control(''));
    this.form.addControl('dockerImage', this.formBuilder.control(''));
    this.form.addControl('dockerCredentialsUsing', this.formBuilder.control(false));
    this.form.addControl('dockerUsername', this.formBuilder.control(''));
    this.form.addControl('dockerPassword', this.formBuilder.control(''));
    this.form.addControl('vCPUs', this.formBuilder.control(1));
    this.form.addControl('ram', this.formBuilder.control(2048));
    this.form.addControl('storage', this.formBuilder.control(500));
    this.form.addControl('hyperVisorType', this.formBuilder.control(this.hyperVisorList[0]));
    this.form.addControl('publicComponent', this.formBuilder.control(false));
    this.form.addControl('gpuEnabled', this.formBuilder.control(false));
    this.form.addControl('healthHttp', this.formBuilder.control(''));
    this.form.addControl('healthCommand', this.formBuilder.control(''));
    this.form.addControl('healthTimeInterval', this.formBuilder.control(''));
    this.form.addControl('environmentalVariables', this.formBuilder.array([]));
    this.form.addControl('exposedInterfaces', this.formBuilder.array([]));
    this.form.addControl('requiredInterfaces', this.formBuilder.control([]));
    // this.form.addControl('monitoringPlugins', this.formBuilder.array([]));
    // this.form.addControl('volumes', this.formBuilder.array([this.initVolumes()]));
    // this.form.addControl('devices', this.formBuilder.array([this.initDevices()]));
    // this.form.addControl('addedLables', this.formBuilder.array([this.initLables()]));

    this.route.params.subscribe(params => {
      this.componentID = parseInt(params['compID']);
    });

    this.componentService.getComponentById(this.componentID).subscribe(
      component => {
        this.existingExposedInterfaces = component.exposedInterfaces;
        this.existingRequiredInterfaceGraphLinks = component.requiredInterfaces;

        const environmentalVariablesControls = this.form.controls['environmentalVariables'] as FormArray;
        if (component.environmentalVariables.length > 0) {
          component.environmentalVariables.forEach(
            environmentalVariable => {
              environmentalVariablesControls.push(
                this.formBuilder.group({
                  key: this.formBuilder.control(environmentalVariable.key),
                  value: this.formBuilder.control(environmentalVariable.value),
                })
              );
            }
          );
        } else {
          environmentalVariablesControls.push(
            this.formBuilder.group({
              key: '',
              value: '',
            })
          );
        }

        const exposedInterfacesControls = this.form.controls['exposedInterfaces'] as FormArray;
        if (component.exposedInterfaces.length > 0) {
          component.exposedInterfaces.forEach(
            // Can not name `interface` because is reserved word
            interfaceEntity => {
              exposedInterfacesControls.push(
                this.formBuilder.group({
                  name: this.formBuilder.control(interfaceEntity.name),
                  port: this.formBuilder.control(interfaceEntity.name),
                  interfaceType: this.formBuilder.control(interfaceEntity.interfaceType),
                  transmissionProtocol: this.formBuilder.control(interfaceEntity.transmissionProtocol),
                })
              );
            }
          );
        } else {
          exposedInterfacesControls.push(
            this.formBuilder.group({
              name: '',
              port: '',
              interfaceType: '',
              transmissionProtocol: '',
            })
          );
        }

        this.interfaceService.getInterfaces().subscribe(interfaces => {
          this.interfaceList = interfaces;

          this.form.setValue(
            {
              componentName: component.name,
              architecture: this.architectureList.find(elem => elem.name.toLowerCase() === component.architecture.toLowerCase()),
              elastProfile: this.elasticityProfileList.find(elem => elem.scaling === component.scaling),
              dockerRegistry: component.dockerRegistry,
              dockerImage: component.dockerImage,
              dockerCredentialsUsing: component.dockerCredentialsUsing,
              dockerUsername: component.dockerUsername,
              dockerPassword: '',
              vCPUs: component.requirement.vCPUs,
              ram: component.requirement.ram,
              storage: component.requirement.storage,
              hyperVisorType: this.hyperVisorList.find(elem => elem.name === component.requirement.hypervisorType),
              publicComponent: component.publicComponent,
              gpuEnabled: component.requirement.gpuRequired,
              healthHttp: component.healthCheck.httpURL,
              healthCommand: component.healthCheck.args,
              healthTimeInterval: component.healthCheck.interval,
              environmentalVariables: this.form.controls['environmentalVariables'].value, // Do not overwrite as was set above
              exposedInterfaces: this.form.controls['exposedInterfaces'].value, // Do not overwrite as was set above
              requiredInterfaces: component.requiredInterfaces.map(
                graphLink => this.interfaceList.find(interfaceEntity => interfaceEntity.interfaceID === graphLink.interfaceObj.interfaceID)
              ),
              // monitoringPlugins: [],
              // volumes: [],
              // devices: [],
              // addedLables: [],
            }
          );
        });
    });
  }

  getExposedInterfaces(formBuilder: FormBuilder, interfaces: Interface[]) {
    return interfaces.map(
      exposedInterface => formBuilder.group({
        name: exposedInterface.name,
        port: exposedInterface.port,
        type: exposedInterface.interfaceType,
        protocol: exposedInterface.transmissionProtocol
      })
    );
  }

  initEnvVar() {
    return this.formBuilder.group({
      key: '',
      value: ''
    });
  }
  addEnvVar() {
    const variables = <FormArray>this.form.controls['environmentalVariables'];
    variables.push(this.initEnvVar());
  }
  removeEnvVar(i: number) {
    const variables = <FormArray>this.form.controls['environmentalVariables'];
    variables.removeAt(i);
  }
  // exposed interfaces
  initExposedInterfaces() {
    return this.formBuilder.group({
      name: '',
      port: '',
      interfaceType: '',
      transmissionProtocol: ''
    });
  }
  addExposedInterface() {
    const variables = <FormArray>this.form.controls['exposedInterfaces'];
    variables.push(this.initExposedInterfaces());
    console.log(this.form);
  }
  removeExposedInterface(i: number) {
    const variables = <FormArray>this.form.controls['exposedInterfaces'];
    variables.removeAt(i);
  }
  // required interfaces
  initRequiredInterfaces() {
    return this.formBuilder.group({
      name: '',
      port: '',
      interfaceType: '',
      transmissionProtocol: ''
    });
  }
  addRequiredInterface() {
    const variables = <FormArray>this.form.controls['addedRequiredInterface'];
    variables.push(this.initEnvVar());
  }
  removeRequiredInterface(i: number) {
    const variables = <FormArray>this.form.controls['addedRequiredInterface'];
    variables.removeAt(i);
  }
  // volumes
  initVolumes() {
    return this.formBuilder.group({
      name: ''
    });
  }
  addVolume() {
    const variables = <FormArray>this.form.controls['volumes'];
    variables.push(this.initEnvVar());
    console.log(this.form);
  }
  removeVolume(i: number) {
    const variables = <FormArray>this.form.controls['volumes'];
    variables.removeAt(i);
  }
  // devices
  initDevices() {
    return this.formBuilder.group({
      key: '',
      value: ''
    });
  }
  addDevice() {
    const variables = <FormArray>this.form.controls['devices'];
    variables.push(this.initEnvVar());
    console.log(this.form);
  }
  removeDevice(i: number) {
    const variables = <FormArray>this.form.controls['devices'];
    variables.removeAt(i);
  }
  // lables
  initLables() {
    return this.formBuilder.group({
      name: ''
    });
  }
  addLable() {
    const variables = <FormArray>this.form.controls['volumes'];
    variables.push(this.initEnvVar());
    console.log(this.form);
  }
  removeLable(i: number) {
    const variables = <FormArray>this.form.controls['volumes'];
    variables.removeAt(i);
  }

  submit(form: FormGroup) {
    const exposedInterfaces = [];
    (form.controls['exposedInterfaces'] as FormArray)
      .controls.map(formGroup => formGroup.value)
      .filter(exposedInterface => exposedInterface.name && exposedInterface.port && exposedInterface.interfaceType && exposedInterface.transmissionProtocol)
      .forEach(
        (exposedInterface: Interface) => {
          const existingExposedInterface = this.existingExposedInterfaces.find(
            interfaceEntity => interfaceEntity.name === exposedInterface.name
          );

          if (existingExposedInterface) {
            exposedInterfaces.push(existingExposedInterface);
          } else {
            exposedInterfaces.push(exposedInterface);
          }
        }
      );

    const requiredInterfaces = [];
    form.controls['requiredInterfaces'].value.forEach(
      (requiredInterface: Interface) => {
        const existingRequiredInterfaceGraphLink = this.existingRequiredInterfaceGraphLinks.find(
          graphLink => graphLink.interfaceObj.interfaceID === requiredInterface.interfaceID
        );

        if (existingRequiredInterfaceGraphLink) {
          requiredInterfaces.push(
            {
              graphLinkID: existingRequiredInterfaceGraphLink.graphLinkID,
              interfaceObj:  {
                interfaceID: requiredInterface.interfaceID
              }
            }
          );
        } else {
          requiredInterfaces.push(
            {
              interfaceObj:  {
                interfaceID: requiredInterface.interfaceID
              }
            }
          );
        }
      }
    );

     const newComp = {
      architecture: form.controls['architecture'].value.name,
      componentName: form.controls['componentName'].value,
      elastProfile: form.controls['elastProfile'].value.scaling,
      publicComponent: form.controls['publicComponent'].value,
      dockerRegistry: form.controls['dockerRegistry'].value,
      dockerImage: form.controls['dockerImage'].value,
      dockerCustomRegistry: true,
      dockerCredentialsUsing: form.controls['dockerCredentialsUsing'].value,
      dockerUsername: form.controls['dockerUsername'].value,
      dockerPassword: form.controls['dockerPassword'].value,
      gpuEnabled: form.controls['gpuEnabled'].value,
      hyperVisorType: form.controls['hyperVisorType'].value.name,
      ram: form.controls['ram'].value,
      storage: form.controls['storage'].value,
      vCPUs: form.controls['vCPUs'].value,
      healthCommand: form.controls['healthCommand'].value,
      healthHttp: form.controls['healthHttp'].value,
      healthTimeInterval: form.controls['healthTimeInterval'].value,
      environmentalVariables: (form.controls['environmentalVariables'] as FormArray).controls.map(formGroup => formGroup.value)
        .filter(environmentalVariable => environmentalVariable.key && environmentalVariable.value),
      exposedInterfaces: exposedInterfaces,
      requiredInterfaces: requiredInterfaces,
    };

    this.componentService.updateComponent(this.componentID, newComp).subscribe(
      () => {
        this.router.navigate(['/dashboard/components']);

        this.snackbar.open(
          'Component successfully saved',
          '',
          {
            duration: 3000
          }
        );
      },
      (error: HttpErrorResponse) => {
        this.snackbar.open(
          (error.error as RestResponseSPA<void>).message,
          '',
          {
            duration: 3000
          }
        );
      }
    );
  }

  cancel(): void {
    this.router.navigate(['/dashboard/components']);
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateInstanceComponent } from './create-instance.component';

const routes: Routes = [
  { path: ':appName', component: CreateInstanceComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateInstanceRoutingModule { }

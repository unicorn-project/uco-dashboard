import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ApplicationService } from 'src/app/shared/services/application.service';
import { ComponentsService } from 'src/app/shared/services/components.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ComponentNode } from '../../shared/entities/ComponentNode';
import { InstanceService } from 'src/app/shared/services/instance.service';
import { MatSnackBar } from '@angular/material';
import { ComponentNodeCard } from 'src/app/shared/entities/ComponentNodeCard';

@Component({
  selector: 'app-create-instance',
  templateUrl: './create-instance.component.html',
  styleUrls: ['./create-instance.component.scss'],
  providers: [ApplicationService, ComponentsService, InstanceService]
})
export class CreateInstanceComponent implements OnInit, OnDestroy {

  appName: string;
  sub: any;

  dataSource = [];
  compNodesList: ComponentNode[] = [];
  compNode: ComponentNode;
  componentConfigList: ComponentNodeCard[];
  displayedColumns: string[] = ['hexID', 'name'];
  ipsList = [];
  pluginList = [];

  // form controls for saving data
  instanceForm: FormGroup;
  configForm: FormGroup;

  // flags
  seeConfig = false;

  constructor(  private route: ActivatedRoute,
                private router: Router,
                private appService: ApplicationService,
                private compService: ComponentsService,
                private instanceService: InstanceService,
                private snackbar: MatSnackBar) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.appName = params['appName'];
    });

    this.appService.getAppByName(this.appName).subscribe(
      applicationInShortForm => {
        this.appService.getAppById(applicationInShortForm.id).subscribe(
          application => {
            this.dataSource = application.componentNodes;
            this.compNodesList = this.compNodesList.concat(application.componentNodes);
          }
        );
      }
    );

    /***************** create forms for saving data ***********************/
    this.instanceForm = new FormGroup({
      instanceName: new FormControl()
    });

    this.configForm = new FormGroup({
      sshKey: new FormControl('testjulia'),
      provider: new FormControl(),
      region: new FormControl(),
      activeIDS: new FormControl(false),
      activeIPS: new FormControl(false),
      ipsRule: new FormControl(),
      minWorker: new FormControl(),
      maxWorker: new FormControl(),
      http: new FormControl(),
      healthCommand: new FormControl(),
      healthTimeInterval: new FormControl(),
      overrideCommand: new FormControl(),
      interfaceName: new FormControl(),
      interfacePort: new FormControl(),
      environmentKey: new FormControl(),
      environmentValue: new FormControl(),
      plugins: new FormControl(),
      volumes: new FormControl(),
      devices: new FormControl()
    });
    this.configForm.disable();
  }

  deleteCompNode() {}

  saveComponentConfiguration() {
    console.log(this.configForm.value);
  }

  saveInstance() {

    // check instance name
    this.instanceService.searchInstanceByName(this.instanceForm.value.instanceName).subscribe(appInstData => {
      // create appInstance field
      this.appService.getAppByName(this.appName).subscribe(appData => {console.log(appData);
        const appInstance = {
          application: {
            id: appData.id
          },
          name: this.instanceForm.value.instanceName,
          overlay: false,
          provider: {
            providerID: 1
          }
        };

        // save to database
        this.instanceService.saveAppInstance(appInstance).subscribe(
          () => {
            this.router.navigate(['/dashboard/instances']);

            this.snackbar.open(
              'Instance ' + this.instanceForm.value.instanceName + ' was created',
              '',
              {
                verticalPosition: 'bottom',
                duration: 3000,
                panelClass: ['unicorn-snackbar']
              }
            );
          }
        );
      });
    },
    err => {
      this.snackbar.open('Instance ' + this.instanceForm.value.instanceName + ' already exists', '', {
        verticalPosition: 'bottom',
        duration: 3000,
        panelClass: ['unicorn-snackbar']
      });
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}

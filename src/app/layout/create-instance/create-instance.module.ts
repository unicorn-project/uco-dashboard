import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateInstanceRoutingModule } from './create-instance-routing.module';
import { MaterialModule } from 'src/app/material.module';
import { CreateInstanceComponent } from './create-instance.component';
import { MonitoringModule } from '../monitoring/monitoring.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [CreateInstanceComponent],
  imports: [
    CommonModule,
    CreateInstanceRoutingModule,
    MaterialModule,
    MonitoringModule,
    ReactiveFormsModule
  ]
})
export class CreateInstanceModule { }

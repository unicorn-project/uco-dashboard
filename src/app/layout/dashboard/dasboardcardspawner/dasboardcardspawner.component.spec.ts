import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DasboardcardspawnerComponent } from './dasboardcardspawner.component';

describe('DasboardcardspawnerComponent', () => {
  let component: DasboardcardspawnerComponent;
  let fixture: ComponentFixture<DasboardcardspawnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DasboardcardspawnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DasboardcardspawnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

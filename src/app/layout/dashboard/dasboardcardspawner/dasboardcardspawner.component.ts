import { Component, OnInit, Input, ViewChild, ViewContainerRef, ComponentFactoryResolver, ComponentRef } from '@angular/core';
import { DashboardcardComponent } from '../dashboardcard/dashboardcard.component';
import { DashboardCard } from '../dashboardcard/DashboardCard';
import { Dashboard } from '../dashboardcard/dashboard';

@Component({
  selector: 'app-dasboardcardspawner',
  templateUrl: './dasboardcardspawner.component.html',
  styleUrls: ['./dasboardcardspawner.component.scss']
})
export class DasboardcardspawnerComponent implements OnInit {

  @ViewChild('dashboardCards', {read: ViewContainerRef}) VCR: ViewContainerRef;
  @Input() card: DashboardCard;
  componentIndex = 0;
  componentReferences = [];

  constructor(private CFR: ComponentFactoryResolver) { }

  ngOnInit() {
    this.loadComponent();
  }
  loadComponent() {
    // const injector = Injector.create(this.VCR.parentInjector);
    const compfactory = this.CFR.resolveComponentFactory(DashboardcardComponent);

    const compRef: ComponentRef<Dashboard> = this.VCR.createComponent(compfactory);

    const currentComponent = compRef.instance;

    currentComponent.selfRef = currentComponent;
    currentComponent.index = ++this.componentIndex;
    currentComponent.card = this.card;
    currentComponent.compInteraction = this;

    this.componentReferences.push(currentComponent);
  }

  remove(index: number) {
    if (this.VCR.length < 1) {
      return;
    }

    console.log(this.componentReferences);
    const compRef = this.componentReferences.filter(x => x.instance.index === index)[0];
    const component: DashboardcardComponent = <DashboardcardComponent>compRef.instance;

    const vcrIndex: number = this.VCR.indexOf(compRef);

    this.VCR.remove(vcrIndex);

    this.componentReferences = this.componentReferences.filter(x => x.instance.index !== index);
  }

}

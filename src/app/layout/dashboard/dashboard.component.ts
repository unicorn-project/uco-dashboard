import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { ApplicationInstance } from 'src/app/shared/entities/ApplicationInstance';
import { ComponentsService } from 'src/app/shared/services/components.service';
import { InstanceService } from 'src/app/shared/services/instance.service';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource } from '../../../../node_modules/@angular/material';
import { routerTransition } from '../../router.animations';
import { AppComponent } from '../../shared/entities/AppComponent';
import { Application } from '../../shared/entities/Application';
import { DashboardConfigData } from '../../shared/entities/DashboardConfigData';
import { User } from '../../shared/entities/User';
import { ApplicationService } from '../../shared/services/application.service';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { CardsService } from '../../shared/services/cards.service';
import { UserService } from '../../shared/services/user.service';
import { AddInstanceDialogComponent } from '../dialogues/add-instance-dialog/add-instance-dialog.component';
import { EditDashboardDialogComponent } from '../dialogues/edit-dashboard-dialog/edit-dashboard-dialog.component';
import { DashboardCard } from './dashboardcard/DashboardCard';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()],
    providers: [ApplicationService,
                AuthenticationService,
                UserService,
                CardsService,
                LocalStorageService
            ]
})
export class DashboardComponent implements OnInit {

    currentUser: User;
    currentUsername: string;
    applist: Application[] = [];
    testApp: Application;
    sub: any;
    appName: string;

    cards: DashboardCard[];

    testApps: Array<any> = [];

    // counters
    noAppInstances = 0;
    noApps = 0;
    noComponents = 0;
    noPublicComponents = 0;
    noPrivateComponents = 0;
    noPublicApps = 0;
    noPrivateApps = 0;

    noAppInstancesError = false;

    // notifications
    notification_messages: string;

    // data tables
    appInstancesTableData = [];
    appTableData = [];
    compTableData = [];
    displayedColumnsApp: string[] = ['name', 'dateCreated', 'visibility', 'actions'];
    displayedColumnsAppInstance: string[] = ['name', 'dateCreated', 'status', 'details', 'logs'];
    displayedColumnsComp: string[] = ['name', 'dateCreated', 'visibility', 'details'];

    appDataSource = new MatTableDataSource();
    compDataSource = new MatTableDataSource();
    instanceDataSource = new MatTableDataSource();

    // indicators
    noAppInstancesAvailable = false;
    noAppsAvailable = false;
    noCompsAvailable = false;

    // user flags
    dashboardConfig: DashboardConfigData;

    seeAppInstanceTable: boolean;
    seeAppTable: boolean;
    seeCompTable: boolean;
    seeNumbers: boolean;
    seeNoAppInstances: boolean;
    seeNoApps: boolean;
    seeNoComponents: boolean;
    seeNoPublicComponents: boolean;
    seeNoPrivateComponents: boolean;
    seeNoPublicApps: boolean;
    seeNoPrivateApps: boolean;

    editDashboardDialogRef: MatDialogRef<EditDashboardDialogComponent>;
    addNewInstanceDialogRef: MatDialogRef<AddInstanceDialogComponent>;

    @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
    @ViewChildren(MatSort) sort = new QueryList<MatSort>();

    constructor(private appService: ApplicationService,
                private componentService: ComponentsService,
                private instanceService: InstanceService,
                private authService: AuthenticationService,
                private cardService: CardsService,
                private localStorageService: LocalStorageService,
                private router: Router,
                private dialog: MatDialog,
    ) {}



    ngOnInit() {
        // get current user using username in localstorage
        this.currentUsername = this.authService.getCurrentUser();
        this.seeNumbers = false;
        // get dashboard config
        this.retrieveAndSetDashboardConfig();

        // applications
        this.appService.getApps().subscribe(applications => {
            this.noApps = applications.length;
            this.appTableData = applications;

            for (let i = 0; i < applications.length; i++) {
                this.appTableData[i].dateCreated = this.appTableData[i].dateCreated.toString().slice(0, 19);
            }

            if (this.noApps === 0) {
                this.noAppsAvailable = true;
            }

            // search for public apps
            for (let i = 0; i < applications.length; i++) {
                if (applications[i].publicApplication) {
                    this.noPublicApps++;
                    this.appTableData[i].visibility = 'public';
                } else {
                    this.noPrivateApps++;
                    this.appTableData[i].visibility = 'private';
                }
            }
            this.appDataSource.data = this.appTableData;
            this.appDataSource.paginator = this.paginator.toArray()[1];
            this.appDataSource.sort = this.sort.toArray()[1];
        });

        this.retrieveComponents();

        // app instances
        this.getAppInstances();

        // get dashboard cards
        this.getDashboardCards();
    }

    // get app instances
    getAppInstances() {
        this.instanceService.getAppInstances().subscribe(applicationInstances => {
            this.noAppInstances = applicationInstances.length;
            this.appInstancesTableData = applicationInstances;

            this.appInstancesTableData.forEach(applicationInstance => {
                applicationInstance.dateCreated = applicationInstance.dateCreated.toString().slice(0, 19);
            });

            this.instanceDataSource.data = this.appInstancesTableData;
            this.instanceDataSource.paginator = this.paginator.toArray()[0];
            this.instanceDataSource.sort = this.sort.toArray()[0];

            /*********** compute number of instances ***************/
            if (this.noAppInstances === 0) {
                this.noAppInstancesAvailable = true;
            }
        },
        () => {
            this.noAppInstancesError = true;
        });
    }

    getDashboardCards() {
        this.cardService.getDashboardCards().subscribe( cards => {
            this.cards = cards;
        });
    }

    openDetailApplicationView(app: Application) {
        this.router.navigate(['/dashboard/application', app.name]);
    }

    openDetailInstanceView(instance: ApplicationInstance) {
        this.router.navigate(['/dashboard/instances', instance.applicationInstanceID]);
    }

    openApplicationInstanceLogsView(applicationInstance: ApplicationInstance) {
        this.router.navigate(['/dashboard/instances/logs', applicationInstance.applicationInstanceID]);
    }

    openDetailComponentView(comp: AppComponent) {
        this.router.navigate(['/dashboard/components', comp.name]);
    }

    deleteApp(card: DashboardCard) {
        // get app by appname
        this.appService.getAppByName(card.appName).subscribe(data => {
            // delete app
            this.appService.deleteApp(data.id).subscribe();
        });

        // delete afterwards dashboard card
        this.cardService.deleteDashboardCard(card.id).subscribe( data => {
            // get new list of cards
            this.getDashboardCards();
        });
    }

    openNewInstanceDialog(application: Application) {
        this.router.navigate(['/dashboard/create-instance', application.name]);
    }

    openNewAppDialog() {
        this.router.navigate(['/dashboard/application/create']);
    }

    openNewComponentDialog() {
        this.router.navigate(['/dashboard/components/create-component']);
    }

    openEditDashboardDialog() {
        this.editDashboardDialogRef = this.dialog.open(EditDashboardDialogComponent, {
            hasBackdrop: true,
            data: {
                seeAppInstanceTable: this.seeAppInstanceTable,
                seeAppTable: this.seeAppTable,
                seeCompTable: this.seeCompTable,
                seeNoAppInstances: this.seeNoAppInstances,
                seeNoApps: this.seeNoApps,
                seeNoComponents: this.seeNoComponents,
                seeNoPublicComponents: this.seeNoPublicComponents,
                seeNoPrivateComponents: this.seeNoPrivateComponents,
                seeNoPublicApps: this.seeNoPublicApps,
                seeNoPrivateApps: this.seeNoPrivateApps
            },
            width: '80%'
        });

        this.editDashboardDialogRef
            .afterClosed()
            .pipe()
            .subscribe(data => {
                const updatedConfig = new DashboardConfigData(
                    data.seeAppInstanceTable,
                    data.seeAppTable,
                    data.seeCompTable,
                    data.seeNoAppInstances,
                    data.seeNoApps,
                    data.seeNoComponents,
                    data.seeNoPublicComponents,
                    data.seeNoPrivateComponents,
                    data.seeNoPublicApps,
                    data.seeNoPrivateApps);
                console.log(updatedConfig);
                this.localStorageService.editDashboardConfig(updatedConfig);

                this.retrieveAndSetDashboardConfig();
            });
    }

    private retrieveComponents(): void {
        this.componentService.getComponents().subscribe(components => {
            this.noComponents = components.length;
            this.compTableData = components;

            if (this.noComponents === 0) {
                this.noCompsAvailable = true;
            }

            // search for no of public components
            for (let i = 0; i < components.length; i++) {
                this.compTableData[i].dateCreated = this.compTableData[i].dateCreated.toString().slice(0, 19);
                if (components[i].publicComponent) {
                    this.noPublicComponents++;
                    this.compTableData[i].visibility = 'public';
                } else {
                    this.noPrivateComponents++;
                    this.compTableData[i].visibility = 'private';
                }
            }

            this.compDataSource.data = this.compTableData;
            this.compDataSource.paginator = this.paginator.toArray()[2];
            this.compDataSource.sort = this.sort.toArray()[2];
        });
    }

    private retrieveAndSetDashboardConfig(): void {
        const dashboardConfig = this.localStorageService.getDashboardConfig();

        this.dashboardConfig = dashboardConfig;
        this.seeNoAppInstances = dashboardConfig.seeNoAppInstances;
        this.seeAppInstanceTable = dashboardConfig.seeAppInstanceTable;
        this.seeAppTable = dashboardConfig.seeAppTable;
        this.seeCompTable = dashboardConfig.seeCompTable;
        this.seeNoApps = dashboardConfig.seeNoApps;
        this.seeNoComponents = dashboardConfig.seeNoComponents;
        this.seeNoPublicComponents = dashboardConfig.seeNoPublicComponents;
        this.seeNoPrivateComponents = dashboardConfig.seeNoPrivateComponents;
        this.seeNoPublicApps = dashboardConfig.seeNoPublicApps;
        this.seeNoPrivateApps = dashboardConfig.seeNoPrivateApps;

        if (
            this.seeNoAppInstances ||
            this.seeNoApps ||
            this.seeNoComponents ||
            this.seeNoPrivateApps ||
            this.seeNoPrivateComponents ||
            this.seeNoPublicApps ||
            this.seeNoPublicComponents
        ) {
            this.seeNumbers = true;
        }
    }
}

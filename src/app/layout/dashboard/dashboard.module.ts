import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import {
    TimelineComponent,
    NotificationComponent
} from './components';
import { StatModule } from '../../shared';
import { MaterialModule } from '../../material.module';
import { DasboardcardspawnerComponent } from './dasboardcardspawner/dasboardcardspawner.component';
import { DashboardcardComponent } from './dashboardcard/dashboardcard.component';

@NgModule({
    imports: [
        CommonModule,
        // NgbCarouselModule.forRoot(),
        // NgbAlertModule.forRoot(),
        DashboardRoutingModule,
        StatModule,
        MaterialModule
    ],
    declarations: [
        DashboardComponent,
        TimelineComponent,
        NotificationComponent,
        DasboardcardspawnerComponent,
        DashboardcardComponent
    ],
    entryComponents: [
        DashboardcardComponent
    ]
})
export class DashboardModule {}

export class DashboardCard {

    id: number;
    title: string;
    appName: string;

    constructor(id: number, title: string, appName: string) {
        this.id = id;
        this.title = title;
        this.appName = appName;
    }

}

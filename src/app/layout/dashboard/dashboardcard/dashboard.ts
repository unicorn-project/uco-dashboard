import { DashboardCard } from './DashboardCard';


export interface Dashboard {
    index: number;
    selfRef: Dashboard;
    card: DashboardCard;
    compInteraction: MyInterface;
  }

export interface MyInterface {
remove(index: number);
}

import { Component, OnInit, Input } from '@angular/core';
import { Dashboard, MyInterface } from './dashboard';
import { DashboardCard } from './DashboardCard';
import { CardsService } from '../../../shared/services/cards.service';
import { ApplicationService } from '../../../shared/services/application.service';
import { ComponentsService } from 'src/app/shared/services/components.service';

@Component({
  selector: 'app-dashboardcard',
  templateUrl: './dashboardcard.component.html',
  styleUrls: ['./dashboardcard.component.scss']
})
export class DashboardcardComponent implements Dashboard, OnInit {
  @Input() card: DashboardCard;

  public index: number;
  public selfRef: DashboardcardComponent;
  public compInteraction: MyInterface;

  content: string;
  complist: any;
  noCompAvailable = false;

  constructor(  private appService: ApplicationService,
                private componentService: ComponentsService) { }

  ngOnInit() {
    this.appService.getAppByName(this.card.appName).subscribe(
      applicationInShortForm => {
        this.appService.getAppById(applicationInShortForm.id).subscribe(
          application => {
            application.componentNodes.forEach(
              componentNode => {
                this.componentService.getComponentById(componentNode.component.id).subscribe(
                  component => {
                    this.complist.push(component.name);
                  }
                );
              }
            );

            if (application.componentNodes.length === 0) {
              this.noCompAvailable = true;
            }
          }
        );
      }
    );
  }

  removeMe(index) {
    this.compInteraction.remove(index);
  }


}

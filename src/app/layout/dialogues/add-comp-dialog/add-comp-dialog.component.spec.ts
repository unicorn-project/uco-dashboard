import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCompDialogComponent } from './add-comp-dialog.component';

describe('AddCompDialogComponent', () => {
  let component: AddCompDialogComponent;
  let fixture: ComponentFixture<AddCompDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCompDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCompDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

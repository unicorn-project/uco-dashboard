import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '../../../../../node_modules/@angular/forms';
import { MatDialogRef } from '../../../../../node_modules/@angular/material';

export interface Architecture {
  name: string;
}

export interface ElastictiyProfile {
  scaling: string;
  name: string;
}

export interface HyperVisor {
  name: string;
}

@Component({
  selector: 'app-add-comp-dialog',
  templateUrl: './add-comp-dialog.component.html',
  styleUrls: ['./add-comp-dialog.component.scss']
})

export class AddCompDialogComponent implements OnInit {

  form: FormGroup;

  architectureList: Architecture[] = [
    {name: 'x86'},
    {name: 'ARM'}
  ];

  hyperVisorList: HyperVisor[] = [
    {
      name: 'ESXI'
    },
    {
      name: 'KVM'
    },
    {
      name: 'KEN'
    }
  ];

  elasticityProfileList: ElastictiyProfile[] = [
    {
      scaling: 'NONE',
      name: 'None'
    },
    {
      scaling: 'HORIZONTAL',
      name: 'Horizontal'
    },
    {
      scaling: 'VERTICAL',
      name: 'Vertical'
    },
    {
      scaling: 'LAMBDA',
      name: 'Lambda Function'
    },
  ];

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddCompDialogComponent>
  ) { }

  ngOnInit() {
    // create form on dialog to retrieve data from the dialog
    this.form = this.formBuilder.group({});
    this.form.addControl('componentName', this.formBuilder.control(''));
    this.form.addControl('architecture', this.formBuilder.control(''));
    this.form.addControl('elastProfile', this.formBuilder.control(''));
    this.form.addControl('dockerRegistry', this.formBuilder.control(''));
    this.form.addControl('dockerImage', this.formBuilder.control(''));
    this.form.addControl('vCPUs', this.formBuilder.control(1));
    this.form.addControl('ram', this.formBuilder.control(2048));
    this.form.addControl('storage', this.formBuilder.control(500));
    this.form.addControl('hyperVisorType', this.formBuilder.control({}));
    this.form.addControl('publicComponent', this.formBuilder.control(false));
    this.form.addControl('gpuEnabled', this.formBuilder.control(false));
    this.form.addControl('healthHttp', this.formBuilder.control(''));
    this.form.addControl('healthCommand', this.formBuilder.control(''));
    this.form.addControl('healthTimeInterval', this.formBuilder.control(''));
  }

  // submit function
  submit(form) {
    this.dialogRef.close(this.form.value);
  }

}

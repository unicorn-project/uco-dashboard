import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddElasticityPoliciesComponent } from './add-elasticity-policies.component';

describe('AddElasticityPoliciesComponent', () => {
  let component: AddElasticityPoliciesComponent;
  let fixture: ComponentFixture<AddElasticityPoliciesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddElasticityPoliciesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddElasticityPoliciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

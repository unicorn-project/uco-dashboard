import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { InstanceService } from 'src/app/shared/services/instance.service';
import { ComponentsService } from 'src/app/shared/services/components.service';
import { ComponentNodeInstance } from 'src/app/shared/entities/ComponentNodes';

@Component({
  selector: 'app-add-elasticity-policies',
  templateUrl: './add-elasticity-policies.component.html',
  styleUrls: ['./add-elasticity-policies.component.scss']
})
export class AddElasticityPoliciesComponent implements OnInit {

  form: FormGroup;
  metrics: string[] = ['_cpu_cpu_percentage_average (percentage)'];
  functions: string[] = ['No Function', 'Sum', 'Average'];
  components = [];
  dimensions: string[] = ['All dimensions'];
  operants: string[] = ['<', '>'];
  actionComponents = [];
  types: string[] = ['Scale In', 'Scale Out', 'Info', 'Push into Topic'];

  instanceID: number;
  componentInstanceData = [];

  constructor(  private formBuilder: FormBuilder,
                private dialogRef: MatDialogRef<AddElasticityPoliciesComponent>,
                private instanceService: InstanceService,
                private compService: ComponentsService,
                @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {

    // get component nodes
    this.instanceID = this.data.instanceID;
    this.instanceService.getAppInstanceById(this.instanceID).subscribe(instanceData => {
      this.compService.getComponentNodeInstanceByLink(instanceData._links.componentNodeInstances.href).subscribe(compInstData => {
        this.componentInstanceData = compInstData._embedded.componentNodeInstances;

        // loop over all componentNodeInstances to get the componentNode.hexID
        for (let i = 0; i < compInstData._embedded.componentNodeInstances.length; i++) {
          const compNodeInstance: ComponentNodeInstance = compInstData._embedded.componentNodeInstances[i];
          this.compService.getComponentNodeByLink(compNodeInstance._links.componentNode.href).subscribe(compNodeData => {
            this.components.push({name: compNodeInstance.name, compNodeHexID: compNodeData.hexID});
          });
        }
      });
    });

    this.form = this.formBuilder.group({
      name: '',
      selectedMetric: {},
      selectedFunction: {},
      selectedComponent: {},
      selectedDimension: {},
      selectedOperant: {},
      threshold: '',
      period: '',
      inertiaTime: '',
      actionSelectedComponent: {},
      actionSelectedType: {},
      workers: ''
    });
  }

  submit(form) {
    this.dialogRef.close(this.form.value);
  }
}

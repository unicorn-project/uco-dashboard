import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddIDSRuleSetComponent } from './add-idsrule-set.component';

describe('AddIDSRuleSetComponent', () => {
  let component: AddIDSRuleSetComponent;
  let fixture: ComponentFixture<AddIDSRuleSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddIDSRuleSetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddIDSRuleSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

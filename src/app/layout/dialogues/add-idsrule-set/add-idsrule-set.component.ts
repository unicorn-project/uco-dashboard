import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-idsrule-set',
  templateUrl: './add-idsrule-set.component.html',
  styleUrls: ['./add-idsrule-set.component.scss']
})
export class AddIDSRuleSetComponent implements OnInit {

  form: FormGroup;

  constructor(  private formBuilder: FormBuilder,
                private dialogRef: MatDialogRef<AddIDSRuleSetComponent>) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: ''
    });
  }

  submit(form) {
    this.dialogRef.close(this.form.value);
  }
}

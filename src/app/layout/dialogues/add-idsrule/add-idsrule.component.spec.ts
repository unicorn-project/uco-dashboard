import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddIDSRuleComponent } from './add-idsrule.component';

describe('AddIDSRuleComponent', () => {
  let component: AddIDSRuleComponent;
  let fixture: ComponentFixture<AddIDSRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddIDSRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddIDSRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

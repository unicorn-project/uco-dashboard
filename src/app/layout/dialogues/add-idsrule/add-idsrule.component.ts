import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-idsrule',
  templateUrl: './add-idsrule.component.html',
  styleUrls: ['./add-idsrule.component.scss']
})
export class AddIDSRuleComponent implements OnInit {

  form: FormGroup;

  constructor(  private formBuilder: FormBuilder,
                private dialogRef: MatDialogRef<AddIDSRuleComponent>) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: ''
    });
  }

  submit(form) {
    this.dialogRef.close(this.form.value);
  }
}

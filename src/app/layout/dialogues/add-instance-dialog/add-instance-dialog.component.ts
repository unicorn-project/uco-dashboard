import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Application } from 'src/app/shared/entities/Application';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-add-instance-dialog',
  templateUrl: './add-instance-dialog.component.html',
  styleUrls: ['./add-instance-dialog.component.scss']
})
export class AddInstanceDialogComponent implements OnInit {

  form: FormGroup;
  Applications = [];
  applications = new FormControl();
  applicationsList: Application[];

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddInstanceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {

    this.applicationsList = this.data;

    // create form on dialog to retrieve data from the dialog
    this.form = new FormGroup({
      selectedApplication: new FormControl()
    });
  }

  // submit function
  submit(form) {
    this.dialogRef.close(this.form.value);
  }

}

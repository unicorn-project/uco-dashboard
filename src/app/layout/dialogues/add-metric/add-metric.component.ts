import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { MonitoringPluginService } from 'src/app/shared/services/monitoring-plugin.service';
import { Metric, MetricOb } from 'src/app/shared/entities/Metric';

@Component({
  selector: 'app-add-metric',
  templateUrl: './add-metric.component.html',
  styleUrls: ['./add-metric.component.scss']
})
export class AddMetricComponent implements OnInit {

  form: FormGroup;

  selectedMetricName: string;
  metrics: Metric[];
  metricOb: MetricOb;

  constructor(  private formBuilder: FormBuilder,
                private dialogRef: MatDialogRef<AddMetricComponent>,
                private metricService: MonitoringPluginService) { }

  ngOnInit() {

    // get list of metrics
    this.metricService.getMetrics().subscribe(metricData => {
      this.metricOb = metricData;
      this.metrics = metricData._embedded.metrics;

      console.log(metricData);
    });
    this.form = this.formBuilder.group({
      selectedMetric: {}
    });
  }

  submit(form) {
    this.dialogRef.close(this.form.value);
  }
}

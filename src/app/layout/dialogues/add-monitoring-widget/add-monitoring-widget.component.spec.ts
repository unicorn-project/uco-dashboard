import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMonitoringWidgetComponent } from './add-monitoring-widget.component';

describe('AddMonitoringWidgetComponent', () => {
  let component: AddMonitoringWidgetComponent;
  let fixture: ComponentFixture<AddMonitoringWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMonitoringWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMonitoringWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

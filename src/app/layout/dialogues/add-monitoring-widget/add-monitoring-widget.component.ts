import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Metrics, Datepicker, Timesteps, SelectMetric, SelectTimestep } from '../../monitoring/Metrics';
import { AppComponent } from 'src/app/shared/entities/AppComponent';

@Component({
  selector: 'app-add-monitoring-widget',
  templateUrl: './add-monitoring-widget.component.html',
  styleUrls: ['./add-monitoring-widget.component.scss']
})
export class AddMonitoringWidgetComponent implements OnInit {

  // creating selectedQuery
  Metric: Metrics[];
  Components: AppComponent[];
  startDate: Datepicker;
  pickedSteps: number;
  timesteps: Timesteps[];
  timeStepWeight: string;
  selectedMetric: string;

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddMonitoringWidgetComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    // create form on dialog
    this.Metric = new SelectMetric().metrics;
    this.timesteps = new SelectTimestep().times;
    this.startDate = new Datepicker();
    this.Components = this.data;

    this.form = this.formBuilder.group({
      cardname: '',
      selectedMetric: {},
      pickedStartDate: Date,
      selectedComponent: {}

    });
  }

  submit(form) {
    this.dialogRef.close(this.form.value);
  }

}

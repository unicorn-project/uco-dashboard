import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMonitoringruleDialogComponent } from './add-monitoringrule-dialog.component';

describe('AddMonitoringruleDialogComponent', () => {
  let component: AddMonitoringruleDialogComponent;
  let fixture: ComponentFixture<AddMonitoringruleDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMonitoringruleDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMonitoringruleDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

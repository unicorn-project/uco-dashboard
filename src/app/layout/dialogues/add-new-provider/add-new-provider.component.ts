import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { ProviderType } from 'src/app/shared/entities/Provider';
import { ResourcesService } from 'src/app/shared/services/resources.service';

@Component({
  selector: 'app-add-new-provider',
  templateUrl: './add-new-provider.component.html',
  styleUrls: ['./add-new-provider.component.scss'],
  providers: [ResourcesService]
})
export class AddNewProviderComponent implements OnInit {

  form: FormGroup;
  initialForm: FormGroup;

  initFormValid = false;

  providerType: ProviderType;
  providerTypeName: string;

  resourceTypes: ProviderType[];

  constructor(  private formBuilder: FormBuilder,
                private dialogRef: MatDialogRef<AddNewProviderComponent>,
                private resourceService: ResourcesService
                ) { }

  ngOnInit() {

    this.resourceService.getProviderTypes().subscribe(providerTypeData => {
      this.resourceTypes = providerTypeData._embedded.providerTypes;
    });

    this.initialForm = new FormGroup({
      name: new FormControl(),
      resourceType: new FormControl()
    });
  }

  onSubmit() {
    console.log(this.initialForm);
        // create formGroups
        this.initFormValid = true;
        this.providerType = this.initialForm.controls['resourceType'].value;
        this.providerTypeName = this.providerType.name;

        this.form = this.formBuilder.group({
          name: this.initialForm.controls['name'].value,
          resourceType: this.initialForm.controls['resourceType'].value,
          imageID: '',
          networkID: '',
          nexusIP: '',
          proxy: '',
          endpoint: '',
          domain: '',
          username: '',
          password: '',
          project: '',
          publicKey: '',
          privateKey: '',
          meshIdentifier: ''
        });
  }

  submit(form) {
    this.dialogRef.close(this.form.value);
  }

}

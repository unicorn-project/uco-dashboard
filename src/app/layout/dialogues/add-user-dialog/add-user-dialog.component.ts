import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '../../../../../node_modules/@angular/material';
import { FormBuilder, FormGroup } from '../../../../../node_modules/@angular/forms';

@Component({
  selector: 'app-add-user-dialog',
  templateUrl: './add-user-dialog.component.html',
  styleUrls: ['./add-user-dialog.component.scss']
})
export class AddUserDialogComponent implements OnInit {

  form: FormGroup;

  constructor(  private dialogRef: MatDialogRef<AddUserDialogComponent>,
                private formBuilder: FormBuilder) { }

  ngOnInit() {
    // create form elements for dialog
    // TODO: Check for inconsistency in fields!
    this.form = this.formBuilder.group({
      password: '',
      username: '',
      role: '',
      phone: '',
      email: ''
    });
  }

  // submit function
  submit(form) {
    this.dialogRef.close(this.form.value);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddnewsshkeydialogComponent } from './addnewsshkeydialog.component';

describe('AddnewsshkeydialogComponent', () => {
  let component: AddnewsshkeydialogComponent;
  let fixture: ComponentFixture<AddnewsshkeydialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddnewsshkeydialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddnewsshkeydialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

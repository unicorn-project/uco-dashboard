import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-addnewsshkeydialog',
  templateUrl: './addnewsshkeydialog.component.html',
  styleUrls: ['./addnewsshkeydialog.component.scss']
})
export class AddnewsshkeydialogComponent implements OnInit {

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddnewsshkeydialogComponent>
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: '',
      sshkey: '',
      isDefault: false
    });
  }

  submit(form) {
    this.dialogRef.close(this.form.value);
  }

}

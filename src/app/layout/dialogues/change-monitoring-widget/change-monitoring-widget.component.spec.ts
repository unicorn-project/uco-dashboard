import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeMonitoringWidgetComponent } from './change-monitoring-widget.component';

describe('ChangeMonitoringWidgetComponent', () => {
  let component: ChangeMonitoringWidgetComponent;
  let fixture: ComponentFixture<ChangeMonitoringWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeMonitoringWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeMonitoringWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

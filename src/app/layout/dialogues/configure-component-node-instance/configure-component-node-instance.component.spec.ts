import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureComponentNodeInstanceComponent } from './configure-component-node-instance.component';

describe('ConfigureComponentNodeInstanceComponent', () => {
  let component: ConfigureComponentNodeInstanceComponent;
  let fixture: ComponentFixture<ConfigureComponentNodeInstanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigureComponentNodeInstanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureComponentNodeInstanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

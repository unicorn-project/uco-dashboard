import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ComponentsService } from 'src/app/shared/services/components.service';
import { ComponentNode } from '../../../shared/entities/ComponentNode';
import { ComponentNodeInstance, EnvironmentalVariableInstance } from 'src/app/shared/entities/ComponentNodes';
import { ApplicationsComponent } from '../../applications/applications.component';
import { AppComponent } from 'src/app/shared/entities/AppComponent';
import { SSHKey } from 'src/app/shared/entities/SSHKey';
import { SshkeysService } from 'src/app/shared/services/sshkeys.service';
import { ResourcesService } from 'src/app/shared/services/resources.service';
import { Provider } from 'src/app/shared/entities/Provider';
import { Region, LocationInstance } from 'src/app/shared/entities/Regions';
import { IdsruleService } from 'src/app/shared/services/idsrule.service';
import { IDRuleSet, IDRuleSetInstance } from 'src/app/shared/entities/IdsRule';
import { InterfaceInstance } from 'src/app/shared/entities/Interfaces';
import { MonitoringPluginService } from 'src/app/shared/services/monitoring-plugin.service';
import { PluginInstance, Plugin } from 'src/app/shared/entities/Plugins';
import { group } from '@angular/animations';
import { VolumeInstance } from 'src/app/shared/entities/Volumes';
import { DeviceInstance } from 'src/app/shared/entities/Devices';
import { FlavourInstance } from 'src/app/shared/entities/Flavour';

@Component({
  selector: 'app-configure-component-node-instance',
  templateUrl: './configure-component-node-instance.component.html',
  styleUrls: ['./configure-component-node-instance.component.scss']
})
export class ConfigureComponentNodeInstanceComponent implements OnInit {

  form: FormGroup;
  submissionForm: FormGroup;
  testForm: FormGroup;

  interfaceNameTest: FormControl;

  componentNodeInstance: ComponentNodeInstance;
  componentNodeInstanceName: string;
  componentNodeInstanceID: number;
  componentName: string;
  componentNode: ComponentNode;
  component: AppComponent;

  sshKeyList: SSHKey[];
  providerList: Provider[];
  regionList: Region[];
  idsRuleSetList: IDRuleSet[];
  interfaceList: InterfaceInstance[];
  envVarList: EnvironmentalVariableInstance[];
  pluginInstanceList: PluginInstance[];
  pluginInstanceFullList: PluginInstance[];
  pluginInstanceChoosableList: PluginInstance[];
  idsRuleSetInstanceList: IDRuleSetInstance[];
  selectedProvider: Provider;
  selectedSSHKeyName: string;
  volumeInstanceList: VolumeInstance[];
  deviceInstanceList: DeviceInstance[];
  flavourInstance: FlavourInstance;

  noVolumesAvailableMessage: string;
  noVolumesFlag: boolean;
  noDevicesAvailableMessage: string;
  noDevicesFlag: boolean;
  noFlavoursAvailableFlag: boolean;
  noRuleSetInstancesAvailableMessage: string;
  noRuleSetsAvailableFlag: boolean;

  displayedPlugins: string[] = ['name', 'moduleName', 'actions'];
  pluginDataSource = new MatTableDataSource();
  displayedRuleSets: string[] = ['name', 'actions'];
  ruleSetDataSource = new MatTableDataSource();

  selectedName: string;

  componentIsScalable: boolean;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<ConfigureComponentNodeInstanceComponent>,
    private componentService: ComponentsService,
    private sshService: SshkeysService,
    private resourceService: ResourcesService,
    private pluginService: MonitoringPluginService,
    private ruleService: IdsruleService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {

    this.testForm = this.formBuilder.group({});
    this.form = this.formBuilder.group({});
    this.form.addControl('selectedSSHKey', this.formBuilder.control({}));
    this.form.addControl('provider', this.formBuilder.control({}));
    this.form.addControl('region', this.formBuilder.control({}));
    this.form.addControl('activateIDS', this.formBuilder.control(false));
    this.form.addControl('activateIPS', this.formBuilder.control(false));
    this.form.addControl('selectedRuleSet', this.formBuilder.control([]));
    this.form.addControl('httpHealthCheck', this.formBuilder.control(''));
    this.form.addControl('commandHealthCheck', this.formBuilder.control(''));
    this.form.addControl('timeIntervalHealthCheck', this.formBuilder.control(''));
    this.form.addControl('commandOverridable', this.formBuilder.control(''));
    this.form.addControl('selectedPlugins', this.formBuilder.control([]));
    this.form.addControl('vCPUs', this.formBuilder.control(''));
    this.form.addControl('memory', this.formBuilder.control(''));
    this.form.addControl('storage', this.formBuilder.control(''));
    this.form.addControl('minWorkers', this.formBuilder.control(1));
    this.form.addControl('maxWorkers', this.formBuilder.control(1));

    // get information about component node instance
    this.componentNodeInstanceID = this.data;
    this.componentService.getComponentNodeInstanceByID(this.componentNodeInstanceID).subscribe(compNodeInstanceData => {
      this.componentNodeInstanceName = compNodeInstanceData.name;
      this.componentNodeInstance = compNodeInstanceData;
      this.form.controls['commandOverridable'].setValue(compNodeInstanceData.command);
      this.form.controls['activateIDS'].setValue(compNodeInstanceData.statusIDS);
      this.form.controls['activateIPS'].setValue(compNodeInstanceData.statusIPS);
      // get componentNode
      this.componentService.getComponentNodeToComponentNodeInstance(this.componentNodeInstanceID).subscribe(compNodeData => {
        this.componentNode = compNodeData;
        // get component
        this.componentService.getComponentToComponentNode(this.componentNode.componentNodeID).subscribe(compData => {
          this.component = compData;
          this.componentName = this.component.name;

          // scalability of component
          if (this.component.scaling === 'NONE') {
            this.componentIsScalable = false;
          } else {
            this.componentIsScalable = true;
            this.form.controls['minWorkers'].setValue(this.componentNodeInstance.minimumWorkers);
            this.form.controls['maxWorkers'].setValue(this.componentNodeInstance.maximumWorkers);
          }
        });
      });

      // get interface instances
      this.componentService.getComponentNodeInstanceInterfacesByLink(this.componentNodeInstance._links.interfaceInstances.href).subscribe(interfaceData => {
        this.interfaceList = interfaceData._embedded.interfaceInstances;

        this.interfaceList.forEach(control => this.form.addControl(control.name, this.formBuilder.control(control.name)));
        this.interfaceList.forEach(control => this.form.addControl(control.name + 'Port', this.formBuilder.control(control.port)));
      });

      // get environmental variables
      this.componentService
        .getComponentNodeInstanceEnvironmentalVariablesByLink(this.componentNodeInstance._links.environmentalVariableInstances.href).subscribe( envData => {
          this.envVarList = envData._embedded.environmentalVariableInstances;
          this.envVarList.forEach(control => this.form.addControl(control.key, this.formBuilder.control(control.key)));
          this.envVarList.forEach(control => this.form.addControl(control.key + 'Value', this.formBuilder.control(control.value)));
      });

      // get list of plugins
      this.componentService
        .getComponentNodeInstancePluginInstancesByLink(this.componentNodeInstance._links.pluginInstances.href).subscribe(pluginInstanceData => {
          this.pluginInstanceFullList = pluginInstanceData._embedded.pluginInstances;
          this.pluginInstanceList = [];
          this.pluginInstanceChoosableList = [];
          this.pluginInstanceFullList.forEach(instance => {
            if ( (!instance.deletedPlugin) && (!instance.immutablePlugin) ) {
              this.pluginInstanceList.push(instance);
              this.form.addControl(instance.name, this.formBuilder.control(false));
            } else if ( (instance.deletedPlugin) && (!instance.immutablePlugin) ) {
              this.pluginInstanceChoosableList.push(instance);
            }
          });
          this.pluginDataSource.data = this.pluginInstanceList;
          this.pluginDataSource.paginator = this.paginator;
          this.pluginDataSource.sort = this.sort;
        });

      // get ids rule set instances
      this.componentService.getComponentNodeInstanceIDSRuleSetsByLink(compNodeInstanceData._links.iDRuleSetInstances.href).subscribe(ruleSetInstanceData => {
        this.idsRuleSetInstanceList = ruleSetInstanceData._embedded.iDRuleSetInstances;
        this.ruleSetDataSource.data = this.idsRuleSetInstanceList;
        this.noRuleSetsAvailableFlag = false;

        // write rule set into form control
        this.form.controls['selectedRuleSet'].setValue(this.idsRuleSetInstanceList);
        // get ips rule sets
        this.ruleService.getIDSRuleSets().subscribe(ruleSetData => {
          this.idsRuleSetList = ruleSetData._embedded.iDRuleSets;
          ruleSetInstanceData._embedded.iDRuleSetInstances.forEach(ruleSetInstance => {
            ruleSetData._embedded.iDRuleSets.forEach( (ruleSet, index) => {
              if (ruleSetInstance.idRuleSetID === ruleSet.ruleSetID) {
                this.idsRuleSetList.splice(index, 1);
              }
            });
          });
        });
      },
      err => {
        this.ruleService.getIDSRuleSets().subscribe(ruleSetData => {
          this.idsRuleSetList = ruleSetData._embedded.iDRuleSets;
        });
        this.noRuleSetsAvailableFlag = true;
        this.noRuleSetInstancesAvailableMessage = 'No IDS rule sets have been choosen';
      });
      // get volumes
      this.componentService.getComponentNodesInstanceVolumeInstanceByLink(compNodeInstanceData._links.volumeInstances.href).subscribe(volumeData => {
        this.noVolumesFlag = false;
        this.volumeInstanceList = volumeData._embedded.volumeInstances;
        this.volumeInstanceList.forEach(control => this.form.addControl('volumes', this.formBuilder.control(control)));
      },
      err => {
        this.noVolumesFlag = true;
        this.noVolumesAvailableMessage = 'There are no volumes available.';
      });

      // get devices
      this.componentService.getComponentNodesInstanceDeviceInstanceByLink(compNodeInstanceData._links.deviceInstances.href).subscribe(deviceData => {
        this.noDevicesFlag = false;
        this.deviceInstanceList = deviceData._embedded.deviceInstances;
        this.deviceInstanceList.forEach(control => this.form.addControl('devices', this.formBuilder.control(control)));
      },
      err => {
        this.noDevicesFlag = true;
        this.noDevicesAvailableMessage = 'There are no devices available.';
      });

      // get flavours
      this.componentService.getComponentNodesInstanceFlavourInstanceByLink(compNodeInstanceData._links.flavorInstance.href).subscribe(flavourData => {
        this.flavourInstance = flavourData;

        this.form.controls['vCPUs'].setValue(this.flavourInstance.vCPUs);
        this.form.controls['memory'].setValue(this.flavourInstance.ram);
        this.form.controls['storage'].setValue(this.flavourInstance.storage);

        console.log(this.form);
      },
      err => {
      });

      // get ssh key
      this.componentService.getComponentNodesInstanceSSHKeyByLink(compNodeInstanceData._links.sshKey.href).subscribe(sshKeyData => {
        this.form.controls['selectedSSHKey'].setValue(sshKeyData);
      });

      // get provider
      this.componentService.getComponentNodesInstanceProviderByLink(compNodeInstanceData._links.provider.href).subscribe(providerData => {
        this.form.controls['provider'].setValue(providerData);
        this.resourceService.getProviderRegionsByLink(providerData._links.regions.href).subscribe(regionData => {
          this.regionList = regionData._embedded.regions;
        });
      });

      // get selected location
      this.componentService.getComponentNodesInstanceLocationInstanceByLink(compNodeInstanceData._links.locationInstances.href).subscribe(locationData => {
        this.form.controls['region'].setValue(locationData.region);
      });

      // get Health Check
      this.componentService.getComponentNodeInstanceHealthCheckInstanceByLink(compNodeInstanceData._links.healthCheckInstance.href)
        .subscribe(healthCheckData => {
        this.form.controls['httpHealthCheck'].setValue(healthCheckData.httpURL);
        this.form.controls['commandHealthCheck'].setValue(healthCheckData.args);
        this.form.controls['timeIntervalHealthCheck'].setValue(healthCheckData.interval);
      });
    });

    // get ssh keys
    this.sshService.getAllSshKeys().subscribe(sshKeys => {
      this.sshKeyList = sshKeys;
    });

    // get providers
    this.resourceService.getProviders().subscribe(providers => {
      this.providerList = providers;
    });

    this.interfaceNameTest = new FormControl();
    this.interfaceNameTest.valueChanges.subscribe(data => {console.log(data);
    });
  }

  regionChangeForProvider(selectedProvider: Provider) {
    this.resourceService.getProviderRegionsByLink(selectedProvider._links.regions.href).subscribe(regionData => {
      this.regionList = regionData._embedded.regions;
    });
  }

  changeInterfaceValues(selectedName: string) {

    console.log(selectedName);
  }

  compareSSHKeys(sshKey1: SSHKey, sshKey2: SSHKey): boolean {
    return sshKey1 && sshKey2 ? sshKey1.sshKey === sshKey2.sshKey : sshKey1 === sshKey2;
  }

  compareProvider(provider1: Provider, provider2: Provider): boolean {
    return provider1 && provider2 ? provider1.name === provider2.name : provider1 === provider2;
  }

  compareLocations(loc1: LocationInstance, loc2: LocationInstance): boolean {
    return loc1 && loc2 ? loc1.locationInstanceID === loc2.locationInstanceID : loc1 === loc2;
  }

  testSubmit() {
    this.dialogRef.close(this.form.value);
  }

  submit(form) {
    const controlArray = [];
    // Object.keys (this.form.controls).forEach(key => controlArray.push(key));
    // this.form.addControl('controlNames', this.formBuilder.control(controlArray));
    this.dialogRef.close(this.form);
  }

}

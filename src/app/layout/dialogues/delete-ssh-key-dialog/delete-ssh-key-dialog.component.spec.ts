import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteSshKeyDialogComponent } from './delete-ssh-key-dialog.component';

describe('DeleteSshKeyDialogComponent', () => {
  let component: DeleteSshKeyDialogComponent;
  let fixture: ComponentFixture<DeleteSshKeyDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteSshKeyDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteSshKeyDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

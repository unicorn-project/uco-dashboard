import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SSHKey } from 'src/app/shared/entities/SSHKey';

@Component({
  selector: 'app-delete-ssh-key-dialog',
  templateUrl: './delete-ssh-key-dialog.component.html',
  styleUrls: ['./delete-ssh-key-dialog.component.scss']
})
export class DeleteSshKeyDialogComponent implements OnInit {

  form: FormGroup;
  sshKey: SSHKey;

  constructor(private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<DeleteSshKeyDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.sshKey = this.data;

    this.form = this.formBuilder.group({
      toDelete: false
    });
  }

  submit(form) {
    this.dialogRef.close(this.form.value);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAppSettingsDialogComponent } from './edit-app-settings-dialog.component';

describe('EditAppSettingsDialogComponent', () => {
  let component: EditAppSettingsDialogComponent;
  let fixture: ComponentFixture<EditAppSettingsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAppSettingsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAppSettingsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-app-settings-dialog',
  templateUrl: './edit-app-settings-dialog.component.html',
  styleUrls: ['./edit-app-settings-dialog.component.scss']
})
export class EditAppSettingsDialogComponent implements OnInit {

  form: FormGroup;

  constructor(  private dialogRef: MatDialogRef<EditAppSettingsDialogComponent>,
                private formBuilder: FormBuilder) { }

  ngOnInit() {
    // create form elements for dialog
    // TODO: Check for inconsistency in fields!
    this.form = this.formBuilder.group({
      appOwner: '',
      appName: ''
    });
  }

  // submit function
  submit(form) {
    this.dialogRef.close(this.form.value);
  }

}

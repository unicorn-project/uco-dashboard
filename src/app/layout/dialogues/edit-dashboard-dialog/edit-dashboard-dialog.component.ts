import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-edit-dashboard-dialog',
  templateUrl: './edit-dashboard-dialog.component.html',
  styleUrls: ['./edit-dashboard-dialog.component.scss']
})
export class EditDashboardDialogComponent implements OnInit {

  form: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<EditDashboardDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
    seeAppInstanceTable: this.data.seeAppInstanceTable,
    seeAppTable: this.data.seeAppTable,
    seeCompTable: this.data.seeCompTable,
    seeNoAppInstances: this.data.seeNoAppInstances,
    seeNoApps: this.data.seeNoApps,
    seeNoComponents: this.data.seeNoComponents,
    seeNoPublicComponents: this.data.seeNoPublicComponents,
    seeNoPrivateComponents: this.data.seeNoPrivateComponents,
    seeNoPublicApps: this.data.seeNoPublicApps,
    seeNoPrivateApps: this.data.seeNoPrivateApps
    });
  }

  submit(form) {
    this.dialogRef.close(this.form.value);
  }

}

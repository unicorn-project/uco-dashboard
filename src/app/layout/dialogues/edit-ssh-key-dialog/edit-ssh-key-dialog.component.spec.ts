import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSshKeyDialogComponent } from './edit-ssh-key-dialog.component';

describe('EditSshKeyDialogComponent', () => {
  let component: EditSshKeyDialogComponent;
  let fixture: ComponentFixture<EditSshKeyDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSshKeyDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSshKeyDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-edit-user-profile-dialog',
  templateUrl: './edit-user-profile-dialog.component.html',
  styleUrls: ['./edit-user-profile-dialog.component.scss']
})
export class EditUserProfileDialogComponent implements OnInit {

  form: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<EditUserProfileDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      firstname: this.data.firstName,
      lastname: this.data.lastName,
      email: this.data.email,
      phone: this.data.phone,
      emailnotifications: this.data.notificationEmailEnabled,
      webnotifications: this.data.notificationWebEnabled
    });
  }

  submit(form) {
    this.dialogRef.close(this.form.value);
  }

}

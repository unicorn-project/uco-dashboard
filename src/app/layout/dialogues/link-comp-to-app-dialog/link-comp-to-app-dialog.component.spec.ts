import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkCompToAppDialogComponent } from './link-comp-to-app-dialog.component';

describe('LinkCompToAppDialogComponent', () => {
  let component: LinkCompToAppDialogComponent;
  let fixture: ComponentFixture<LinkCompToAppDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkCompToAppDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkCompToAppDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

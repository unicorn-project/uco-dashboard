import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { AppComponent } from '../../../shared/entities/AppComponent';
import { ApplicationService } from '../../../shared/services/application.service';
import { ComponentsService } from 'src/app/shared/services/components.service';

@Component({
  selector: 'app-link-comp-to-app-dialog',
  templateUrl: './link-comp-to-app-dialog.component.html',
  styleUrls: ['./link-comp-to-app-dialog.component.scss'],
  providers: [ApplicationService]
})
export class LinkCompToAppDialogComponent implements OnInit {

  selectedComponentName: string;
  components: AppComponent[];

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<LinkCompToAppDialogComponent>,
    private componentService: ComponentsService
  ) { }

  ngOnInit() {

    // get component list
    this.componentService.getComponents().subscribe(components => {
      this.components = components;
    });
    // create dialog form
    this.form = this.formBuilder.group({
      selectedComponent: {}
    });
  }

  submit(form) {
    this.dialogRef.close(this.form.value);
  }

}

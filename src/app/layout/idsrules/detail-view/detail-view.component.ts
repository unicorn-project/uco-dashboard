import { Component, OnInit, OnDestroy } from '@angular/core';
import { IdsruleService } from 'src/app/shared/services/idsrule.service';
import { ActivatedRoute } from '@angular/router';
import { IDRuleSet } from 'src/app/shared/entities/IdsRule';

@Component({
  selector: 'app-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss'],
  providers: [IdsruleService]
})
export class DetailViewComponent implements OnInit, OnDestroy {

  sub: any;
  ruleSetID: number;
  ruleSet: IDRuleSet;
  ruleSetName: string;
  columnsToDisplay: string[] = ['identifier', 'name'];
  idsRuleDataTable = [];

  constructor( private idsRuleService: IdsruleService,
               private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.ruleSetID = params['idsrulesetID'];
    });

    // get idsruleset
    this.idsRuleService.getIDSRuleSetByID(this.ruleSetID).subscribe(idsRuleSetData => {
      this.ruleSet = idsRuleSetData;
      this.ruleSetName = idsRuleSetData.name;
      // ge corresponding idsRules
      this.idsRuleService.getIdsRulesByLink(this.ruleSet._links.iDRules.href).subscribe(idsRulesData => {
        this.idsRuleDataTable = idsRulesData._embedded.iDRules;
        console.log(idsRulesData);
      });
    });
  }

  ngOnDestroy() {}

}

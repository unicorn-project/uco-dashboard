import { Component, OnInit } from '@angular/core';
import { IdsruleService } from 'src/app/shared/services/idsrule.service';
import { IDRuleSet, IDRule } from 'src/app/shared/entities/IdsRule';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MatDialog } from '@angular/material';
import { AddIDSRuleComponent } from '../../dialogues/add-idsrule/add-idsrule.component';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-edit-view',
  templateUrl: './edit-view.component.html',
  styleUrls: ['./edit-view.component.scss'],
  providers: [IdsruleService]
})
export class EditViewComponent implements OnInit {
  sub: any;
  ruleSetID: number;
  ruleSet: IDRuleSet;
  ruleSetName: string;
  columnsToDisplay: string[] = ['identifier', 'name', 'actions'];
  idsRuleDataTable = [];

  addNewRuleDialogRef: MatDialogRef<AddIDSRuleComponent>;

  constructor(private idsRuleService: IdsruleService,
    private route: ActivatedRoute,
    private dialog: MatDialog) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.ruleSetID = params['idsrulesetID'];
    });

    this.getIdsRules();
  }

  getIdsRules() {
    // get idsruleset
    this.idsRuleService.getIDSRuleSetByID(this.ruleSetID).subscribe(idsRuleSetData => {
      this.ruleSet = idsRuleSetData;
      this.ruleSetName = idsRuleSetData.name;
      // ge corresponding idsRules
      this.idsRuleService.getIdsRulesByLink(this.ruleSet._links.iDRules.href).subscribe(idsRulesData => {
        this.idsRuleDataTable = idsRulesData._embedded.iDRules;
      },
      err => {
        console.log('No rules available in this ruleset');
        this.idsRuleDataTable = [];
      });
    });
  }

  addRule() {
    this.addNewRuleDialogRef = this.dialog.open(AddIDSRuleComponent, {
      hasBackdrop: true,
      width: '80%'
    });

    this.addNewRuleDialogRef
      .afterClosed()
      .pipe(filter(name => name))
      .subscribe(result => {
        this.idsRuleService.getIDSRuleSetByID(this.ruleSetID).subscribe(idsRuleSetData => {
          this.idsRuleService.getIdsRulesByLink(idsRuleSetData._links.iDRules.href).subscribe(idsRulesData => {
            const rules: any = idsRulesData._embedded.iDRules;
            const testRule = {
              name: result.name
            };
            rules.push(testRule);
            console.log(rules);
            this.idsRuleService.editIDSRulesOfIDSRuleSet(rules, this.ruleSetID).subscribe( res => {
              this.getIdsRules();
            });
          }, err  => {
            const rules: any = [];
            const testRule = {
              name:
              result.name
            };
            rules.push(testRule);
            this.idsRuleService.editIDSRulesOfIDSRuleSet(rules, this.ruleSetID).subscribe( res => {
              this.getIdsRules();
            });
          });
        });
      });
  }

  deleteRule(idsRule: IDRule) {
    this.idsRuleService.getIDSRuleSetByID(this.ruleSetID).subscribe(ruleSetData => {
      this.idsRuleService.getIdsRulesByLink(ruleSetData._links.iDRules.href).subscribe(rulesData => {
        // const idToRemove: number = idsRule.ruleID;
        const rules: IDRule[] = rulesData._embedded.iDRules;
        for (let i = 0; i < rules.length; i++) {
          if (idsRule.ruleID === rules[i].ruleID) {
            const indexToRemove = i;
            rules.splice(indexToRemove, 1);
          }
        }
        console.log(rules);
        this.idsRuleService.editIDSRulesOfIDSRuleSet(rules, this.ruleSetID).subscribe( res => {
          this.getIdsRules();
        });
      });
    });
  }

}

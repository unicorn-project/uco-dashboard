import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IdsrulesComponent } from './idsrules.component';
import { DetailViewComponent } from './detail-view/detail-view.component';
import { EditViewComponent } from './edit-view/edit-view.component';

const routes: Routes = [
  { path: '', component: IdsrulesComponent },
  { path: 'details/:idsrulesetID', component: DetailViewComponent},
  { path: 'edit/:idsrulesetID', component: EditViewComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IdsrulesRoutingModule { }

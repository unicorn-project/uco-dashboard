import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdsrulesComponent } from './idsrules.component';

describe('IdsrulesComponent', () => {
  let component: IdsrulesComponent;
  let fixture: ComponentFixture<IdsrulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdsrulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdsrulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

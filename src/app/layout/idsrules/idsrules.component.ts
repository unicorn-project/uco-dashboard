import { Component, OnInit, ViewChild } from '@angular/core';
import { IdsruleService } from 'src/app/shared/services/idsrule.service';
import { IDRuleSet } from 'src/app/shared/entities/IdsRule';
import { Router } from '@angular/router';
import { MatDialogRef, MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { AddIDSRuleSetComponent } from '../dialogues/add-idsrule-set/add-idsrule-set.component';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-idsrules',
  templateUrl: './idsrules.component.html',
  styleUrls: ['./idsrules.component.css'],
  providers: [IdsruleService]
})
export class IdsrulesComponent implements OnInit {

  idsRuleSetDataTable = [];
  idsRuleSetDataSource = new MatTableDataSource();
  columnsToDisplay: string[] = ['name', 'dateCreated', 'actions'];
  noIDSRuleSetsAvailable: boolean;

  addNewRuleSetDialogRef: MatDialogRef<AddIDSRuleSetComponent>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private idsRuleService: IdsruleService,
              private router: Router,
              private dialog: MatDialog) { }

  ngOnInit() {
    // get all idsrulesets
    this.getData();
  }

  getData () {
    this.idsRuleService.getIDSRuleSets().subscribe(idsRuleSetData => {
      this.noIDSRuleSetsAvailable = false;
      this.idsRuleSetDataTable = idsRuleSetData._embedded.iDRuleSets;

      // set date format
      for (let i = 0; i < this.idsRuleSetDataTable.length; i++) {
        this.idsRuleSetDataTable[i].dateCreated = this.idsRuleSetDataTable[i].dateCreated.toString().slice(0, 19);
      }
      this.idsRuleSetDataSource.data = this.idsRuleSetDataTable;
      this.idsRuleSetDataSource.paginator = this.paginator;
      this.idsRuleSetDataSource.sort = this.sort;
    });
  }

    // filter table
    applyTableFilter(filterValue: string) {
      this.idsRuleSetDataSource.filter = filterValue.trim().toLowerCase();
    }

  openDetailView(idsRuleSet: IDRuleSet) {
    this.router.navigate(['/dashboard/idsrules/details', idsRuleSet.ruleSetID]);
  }

  openEditView(idsRuleSet: IDRuleSet) {
    this.router.navigate(['/dashboard/idsrules/edit', idsRuleSet.ruleSetID]);
  }

  delete(idsRuleSet: IDRuleSet) {
    this.idsRuleService.deleteIDSRuleSet(idsRuleSet.ruleSetID).subscribe(res => {
      this.getData();
    });
  }

  createNewRuleSet() {

    this.addNewRuleSetDialogRef = this.dialog.open(AddIDSRuleSetComponent, {
      hasBackdrop: true,
      width: '80%'
    });

    this.addNewRuleSetDialogRef
      .afterClosed()
      .pipe(filter(name => name))
      .subscribe(result => {
        const newRuleSet = {
          name: result.name
        };
        this.idsRuleService.addIDSRuleSet(newRuleSet).subscribe(res => {
          this.getData();
        });
    });
  }
}

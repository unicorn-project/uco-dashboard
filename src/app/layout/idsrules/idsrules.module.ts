import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IdsrulesRoutingModule } from './idsrules-routing.module';
import { IdsrulesComponent } from './idsrules.component';
import { DetailViewComponent } from './detail-view/detail-view.component';
import { MaterialModule } from 'src/app/material.module';
import { EditViewComponent } from './edit-view/edit-view.component';

@NgModule({
  declarations: [IdsrulesComponent, DetailViewComponent, EditViewComponent],
  imports: [
    CommonModule,
    IdsrulesRoutingModule,
    MaterialModule
  ]
})
export class IdsrulesModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstancesDetailsComponent } from './instances-details.component';

describe('InstancesDetailsComponent', () => {
  let component: InstancesDetailsComponent;
  let fixture: ComponentFixture<InstancesDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstancesDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstancesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

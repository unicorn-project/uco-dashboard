import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationService } from '../../../shared/services/application.service';
import { ComponentsService } from 'src/app/shared/services/components.service';
import { ComponentNodeInstance } from 'src/app/shared/entities/ComponentNodes';
import { InstanceService } from 'src/app/shared/services/instance.service';
import { MatDialogRef, MatDialog } from '@angular/material';
import { AddElasticityPoliciesComponent } from '../../dialogues/add-elasticity-policies/add-elasticity-policies.component';
import { filter } from 'rxjs/operators';
import { ConfigureComponentNodeInstanceComponent } from '../../dialogues/configure-component-node-instance/configure-component-node-instance.component';
import { Provider } from 'src/app/shared/entities/Provider';
import { SSHKey } from 'src/app/shared/entities/SSHKey';
import { IDRuleSet } from 'src/app/shared/entities/IdsRule';
import { PluginInstance } from 'src/app/shared/entities/Plugins';
import { ApplicationInstance } from 'src/app/shared/entities/ApplicationInstance';

@Component({
  selector: 'app-instances-details',
  templateUrl: './instances-details.component.html',
  styleUrls: ['./instances-details.component.scss'],
  providers: [ApplicationService]
})
export class InstancesDetailsComponent implements OnInit, OnDestroy {

  sub: any;
  instanceID: number;
  applicationInstance: ApplicationInstance;
  instanceName: string;
  applicationName: string;
  componentList = [];
  appMonitoringData = [];
  instanceStatus: string;

  displayedCompInstanceColumns: string[];
  instanceData = [];
  componentInstanceData = [];

  generalData: {
    identifier: string;
    instanceName: string;
    applicationName: string;
    status: string;
    deploymentTimeStamp: string;
  };

  addElasticityPoliciesDialogRef: MatDialogRef<AddElasticityPoliciesComponent>;
  configureCompNodeInstanceRef: MatDialogRef<ConfigureComponentNodeInstanceComponent>;

  constructor(  private route: ActivatedRoute,
                private router: Router,
                private appService: ApplicationService,
                private componentService: ComponentsService,
                private instanceService: InstanceService,
                private dialog: MatDialog) { }

  ngOnInit() {

    // alllocate space
    this.generalData = {
      identifier: null,
      instanceName: null,
      applicationName: null,
      status: null,
      deploymentTimeStamp: null
    };

    this.sub = this.route.params.subscribe(params => {
      this.instanceID = params['instanceID'];

      // get application data
      this.instanceService.getAppInstanceById(this.instanceID).subscribe(applicationInstance => {
        this.applicationInstance = applicationInstance;
        this.instanceName = applicationInstance.name;
        this.instanceStatus = applicationInstance.status;
        this.applicationName = applicationInstance.application.name;

        this.generalData = {
          identifier: applicationInstance.hexID,
          instanceName: applicationInstance.name,
          applicationName: this.applicationName,
          status: applicationInstance.status,
          deploymentTimeStamp: new Date(applicationInstance.deploymentTimestamp).toString(),
        };

        if (this.generalData.status !== 'Deployed') {
          this.displayedCompInstanceColumns = ['hexID', 'name', 'statusIDS', 'statusIPS', 'actions' ];
        } else {
          this.displayedCompInstanceColumns = ['hexID', 'name', 'statusIDS', 'statusIPS', 'link' ];
        }

        this.appMonitoringData.push({appHexID: applicationInstance.application.hexID, appInstHexID: applicationInstance.hexID});

        this.componentInstanceData = applicationInstance.componentNodeInstances;

        // loop over all componentNodeInstances to get the componentNode.hexID
        for (let i = 0; i < applicationInstance.componentNodeInstances.length; i++) {
          const compNodeInstance: ComponentNodeInstance = applicationInstance.componentNodeInstances[i];
          this.componentList.push({name: compNodeInstance.name, compNodeHexID: compNodeInstance.componentNode.hexID});
        }
      });
    });
  }

  openApplicationInstanceLogsView() {
      this.router.navigate(['/dashboard/instances/logs', this.instanceID]);
  }

  openAddElasticityPoliciesDialog() {
    this.addElasticityPoliciesDialogRef = this.dialog.open(AddElasticityPoliciesComponent, {
      hasBackdrop: true,
      width: '90%',
      data: { instanceID: this.instanceID}
    });

    this.addElasticityPoliciesDialogRef.afterClosed()
      .pipe(filter(name => name))
      .subscribe(data => {
        console.log(data);
      });
  }

  openAddSecurityPoliciesDialog() {}

  openAddProfilingDialog() {}

  undeployInstance():void {
    this.instanceService.undeploy(this.applicationInstance).subscribe();
  }

  deployApplicationInstance(): void {
    this.instanceService.deployInstanceByID(this.instanceID).subscribe(
      () => {
        this.router.navigate(['/dashboard/instances']);
      },
    err => {
      console.log(err);
    });
  }

  openConfigureComponentNodeInstanceDialog(componentNodeInstance: ComponentNodeInstance) {
    this.configureCompNodeInstanceRef = this.dialog.open(ConfigureComponentNodeInstanceComponent, {
      hasBackdrop: true,
      width: '80%',
      data: componentNodeInstance.componentNodeInstanceID
    });

    this.configureCompNodeInstanceRef
      .afterClosed()
      .pipe(filter(provider => provider))
      .subscribe(data => {
        console.log(data);

        const newProvider: Provider = data.controls['provider'].value;
        const newIPSValue: boolean = data.controls['activateIPS'].value;
        const newIDSValue: boolean = data.controls['activateIDS'].value;

        // patch component Node instance
        const compNodeInstancePatch = {
          command: data.commandOverridable,
          componentNodeInstanceID: componentNodeInstance.componentNodeInstanceID,
          dateCreated: componentNodeInstance.dateCreated,
          hexID: componentNodeInstance.hexID,
          lastModified: new Date(),
          loadBalancer: componentNodeInstance.loadBalancer,
          maximumWorkers: data.controls['maxWorkers'].value,
          minimumWorkers: data.controls['minWorkers'].value,
          name: componentNodeInstance.name,
          provider: 'api/v2/providers/' + newProvider.providerID,
          statusIDS: newIDSValue,
          statusIPS: newIPSValue
        };

        this.componentService.patchComponentNodeInstance(compNodeInstancePatch, componentNodeInstance.componentNodeInstanceID).subscribe(newCompInstance => {
          // subscribe ssh key if it has been changed
          const newSSH: SSHKey = data.controls['selectedSSHKey'].value;
          const sshKeyPatch = {
            id: newSSH.id
          };
          this.componentService.patchComponentNodesInstanceSSHKey(newCompInstance.componentNodeInstanceID, sshKeyPatch).subscribe();

          // patch rule set
          const patchRuleSet = [];
          const newRuleSets: IDRuleSet[] = data.controls['selectedRuleSet'].value;
          newRuleSets.forEach(ruleSet => {
            patchRuleSet.push({
              label: ruleSet.name,
              idRuleSet: {
                ruleSetID: ruleSet.ruleSetID
              },
              value: ruleSet.ruleSetID
            });
          });
          this.componentService.patchComponentNodeInstanceIDSRuleSets(patchRuleSet, componentNodeInstance.componentNodeInstanceID).subscribe();

          // patch HealthCheck
          this.componentService.getComponentNodeInstanceHealthCheckInstanceByLink(newCompInstance._links.healthCheckInstance.href)
           .subscribe(healthCheckData => {
             const patchHealthCheck = {
               args: data.controls['commandHealthCheck'].value,
               healthCheckInstanceID: healthCheckData.healthCheckInstanceID,
               httpURL: data.controls['httpHealthCheck'].value,
               interval: data.controls['timeIntervalHealthCheck'].value,
               name: healthCheckData.name
             };
             console.log(patchHealthCheck);
             this.componentService.patchComponentNodeInstanceHealthCheckInstance(patchHealthCheck, newCompInstance.componentNodeInstanceID).subscribe();
          });

          // patch flavour
          this.componentService.getComponentNodesInstanceFlavourInstanceByLink(newCompInstance._links.flavorInstance.href).subscribe(flavourData => {
            const patchFlavour = {
              flavourID: flavourData.flavourID,
              ram: data.controls['memory'].value,
              storage: data.controls['storage'].value,
              vCPUs: data.controls['vCPUs'].value
            };
            console.log(patchFlavour);
            this.componentService.patchComponentNodeInstanceFlavourInstance(patchFlavour, newCompInstance.componentNodeInstanceID).subscribe();
          });

          // patch interface
          this.componentService.getComponentNodeInstanceInterfacesByLink(newCompInstance._links.interfaceInstances.href).subscribe(interfaceData => {
            const patchInterfaces = [];
            interfaceData._embedded.interfaceInstances.forEach(interfaceInstance => {
              const portName: string = interfaceInstance.name + 'Port';
              patchInterfaces.push({
                interfaceInstanceID: interfaceInstance.interfaceInstanceID,
                port: data.controls[portName].value
              });
            });
            this.componentService.patchComponentNodeInstanceInterfaceInstance(patchInterfaces, newCompInstance.componentNodeInstanceID).subscribe();
          });

          // patch environmental
          this.componentService.getComponentNodeInstanceEnvironmentalVariablesByLink(newCompInstance._links.environmentalVariableInstances.href)
          .subscribe(envVarData => {
            const patchEnvVariables = [];
            envVarData._embedded.environmentalVariableInstances.forEach(envVarInstance => {
              const variableValue: string = envVarInstance.key + 'Value';
              patchEnvVariables.push({
                environmentalVariableInstanceID: envVarInstance.environmentalVariableInstanceID,
                value: data.controls[variableValue].value
              });
            });
            this.componentService.patchComponentNodeInstanceEnvironementalVariables(patchEnvVariables, newCompInstance.componentNodeInstanceID).subscribe();
          });

          // patch plugins
          this.componentService.getComponentNodeInstancePluginInstancesByLink(newCompInstance._links.pluginInstances.href).subscribe(pluginInstanceData => {
            const patchPluginInstances = [];
            // get already activated plugins
            const activatedPlugins: PluginInstance[] = [];
            pluginInstanceData._embedded.pluginInstances.forEach(instance => {
              if ( (!instance.deletedPlugin) && (!instance.immutablePlugin) ) {
                activatedPlugins.push(instance);
              }
            });
            // check if one or more are set to true (== delete plugin), if == false push to patch list
            activatedPlugins.forEach(activePlugin => {
              if (!data.controls[activePlugin.name].value) {
                patchPluginInstances.push({
                  pluginInstanceID: activePlugin.pluginInstanceID
                });
              }
            });
            // add now new activated plugins
            const newActivatedPlugins: PluginInstance[] = data.controls['selectedPlugins'].value;
            newActivatedPlugins.forEach(activatedPlugin => {
              patchPluginInstances.push({
                pluginInstanceID: activatedPlugin.pluginInstanceID
              });
            });
            // call patch function
            this.componentService.patchComponentNodeInstancePluginInstances(patchPluginInstances, newCompInstance.componentNodeInstanceID).subscribe();
          });
        });
      });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}

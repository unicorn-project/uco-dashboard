import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InstancesComponent } from './instances.component';
import { InstancesDetailsComponent } from './instances-details/instances-details.component';
import { LogsComponent } from './logs/logs.component';

const routes: Routes = [
  { path: '', component: InstancesComponent },
  { path: ':instanceID', component: InstancesDetailsComponent },
  { path: 'logs/:instanceID', component: LogsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstancesRoutingModule { }

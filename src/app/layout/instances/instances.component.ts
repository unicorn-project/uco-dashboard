import { Component, OnInit, ViewChild } from '@angular/core';
import { ApplicationInstance } from '../../shared/entities/ApplicationInstance';
import { Router } from '@angular/router';
import { InstanceService } from 'src/app/shared/services/instance.service';
import { MatDialogRef, MatDialog, MatSnackBar, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { AddInstanceDialogComponent } from '../dialogues/add-instance-dialog/add-instance-dialog.component';
import { ApplicationService } from 'src/app/shared/services/application.service';
import { Application } from 'src/app/shared/entities/Application';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-instances',
  templateUrl: './instances.component.html',
  styleUrls: ['./instances.component.scss'],
  providers: [InstanceService, ApplicationService]
})
export class InstancesComponent implements OnInit {

  instancesDataTable = [];
  instancesSourceData = new MatTableDataSource();
  displayedColumns = ['name', 'dateCreated', 'lastModified', 'status', 'actions'];

  noAppInstancesAvailable = true;

  addNewInstanceDialogRef: MatDialogRef<AddInstanceDialogComponent>;
  applicationList: Application[];

  snackbar: MatSnackBar;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(  private instanceService: InstanceService,
                private appService: ApplicationService,
                private router: Router,
                private dialog: MatDialog) { }

  ngOnInit() {

    // get all instances
    this.getAllInstances();

    // get all applications
    this.getAllApplications();

  }

  delete(instance: ApplicationInstance) {
    this.instanceService.deleteInstanceByID(instance.applicationInstanceID).subscribe(data => {
      this.getAllInstances();
    }, err => {
      console.log(err);
    });
  }

  editElasticityPolicies(instance: ApplicationInstance) {}

  editSecurityPolicies(instance: ApplicationInstance) {}

  editProfiling(instance: ApplicationInstance) {}

  goToMonitoringPage(instance: ApplicationInstance) {}

  deploy(applicationInstance: ApplicationInstance): void {
    this.instanceService.deployInstanceByID(applicationInstance.applicationInstanceID).subscribe();
  }

  undeploy(applicationInstance: ApplicationInstance): void {
    this.instanceService.undeploy(applicationInstance).subscribe();
  }

  // get all Applications
  getAllApplications() {
    this.appService.getApps().subscribe(applications => {
      this.applicationList = applications;
    });
  }

  // get all instances
  getAllInstances() {
    this.instanceService.getAppInstances().subscribe(applicationInstances => {
      this.instancesDataTable = applicationInstances;

      if (this.instancesDataTable.length !== 0) {
        this.noAppInstancesAvailable = false;
      }
      this.instancesDataTable = applicationInstances;
      // change visibility entries
      for (let i = 0; i < this.instancesDataTable.length; i++) {
        this.instancesDataTable[i].dateCreated = this.instancesDataTable[i].dateCreated.toString().slice(0, 19);
        this.instancesDataTable[i].lastModified = this.instancesDataTable[i].lastModified.toString().slice(0, 19);
      }

      this.instancesSourceData.data = this.instancesDataTable;
      this.instancesSourceData.paginator = this.paginator;
      this.instancesSourceData.sort = this.sort;
    });
  }

  openDetailView(applicationInstance: ApplicationInstance): void {
    this.router.navigate(['/dashboard/instances/', applicationInstance.applicationInstanceID]);
  }

  navigateToLogs(applicationInstance: ApplicationInstance): void {
    this.router.navigate(['/dashboard/instances/logs', applicationInstance.applicationInstanceID]);
  }

  createNewInstance(appName: string) {
    this.router.navigate(['/dashboard/create-instance', appName]);
  }

  // new instance dialog
  openNewInstanceDialog() {

    this.addNewInstanceDialogRef = this.dialog.open(AddInstanceDialogComponent, {
        hasBackdrop: true,
        width: '80%',
        data: this.applicationList
    });

    this.addNewInstanceDialogRef
    .afterClosed()
    .pipe(filter(selectedApplication => selectedApplication))
    .subscribe(data => {
      console.log(data);
      this.createNewInstance(data.selectedApplication.name);
    });
}

}

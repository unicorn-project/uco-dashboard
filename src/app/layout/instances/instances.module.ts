import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InstancesRoutingModule } from './instances-routing.module';
import { InstancesComponent } from './instances.component';
import { InstancesDetailsComponent } from './instances-details/instances-details.component';
import { MaterialModule } from '../../material.module';
import { MonitoringModule } from '../monitoring/monitoring.module';
import { LogsComponent } from './logs/logs.component';

@NgModule({
  imports: [
    CommonModule,
    InstancesRoutingModule,
    MonitoringModule,
    MaterialModule
  ],
  declarations: [InstancesComponent, InstancesDetailsComponent, LogsComponent]
})
export class InstancesModule { }

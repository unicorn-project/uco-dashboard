import { CdkColumnDef } from '@angular/cdk/table';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { interval, Subscription } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';
import { ApplicationInstance } from 'src/app/shared/entities/ApplicationInstance';
import { ComponentNodeInstanceStatus } from 'src/app/shared/entities/ComponentNodes';
import { InstanceService } from '../../../shared/services/instance.service';
import { MatTable } from '@angular/material';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss'],
  providers: [CdkColumnDef, InstanceService]
})
export class LogsComponent implements OnInit, OnDestroy {

  applicationInstance: ApplicationInstance;
  componentNodeInstanceStatuses: ComponentNodeInstanceStatus[];
  displayedColumns = ['entity'];
  statusToColorMap = {
    'ERROR': 'red',
    'INFO': 'blue',
    'LOADING': 'grey',
    'SUCCESS': 'green',
  };

  private componentNodeInstanceStatusesSubscriber: Subscription;

  constructor(
    private route: ActivatedRoute,
    private applicationInstanceService: InstanceService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        const applicationInstanceId = parseInt(params['instanceID']);

        this.applicationInstanceService.getAppInstanceById(applicationInstanceId).subscribe(
          applicationInstance => {
            this.applicationInstance = applicationInstance;
          }
        );

        this.componentNodeInstanceStatusesSubscriber = interval(2000).pipe(
          switchMap(
            () => this.applicationInstanceService.getApplicationInstanceStatuses(applicationInstanceId)
          )
        ).subscribe(
          componentNodeInstanceStatuses => {
            // reversing is not that much needed because the array is sorted in the next line anyway, but that should increase the performance
            this.componentNodeInstanceStatuses = componentNodeInstanceStatuses.reverse();
            componentNodeInstanceStatuses.sort((a, b) => a.dateCreated.valueOf() - b.dateCreated.valueOf());

            try {
              document.getElementById('logs').children[document.getElementById('logs').children.length - 1].scrollIntoView();
            } catch (e) { }
          }
        );
      }
    );
  }

  ngOnDestroy() {
    this.componentNodeInstanceStatusesSubscriber.unsubscribe();
  }
}

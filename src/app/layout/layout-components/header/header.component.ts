import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { AuthenticationService } from '../../../shared/services/authentication.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    providers: []
})

export class HeaderComponent implements OnInit {
    pushRightClass = 'push-right';

    actualUser: string;

    constructor(public router: Router,
                private authService: AuthenticationService,
    ) {}

    ngOnInit() {
        // get username
        this.actualUser = this.authService.getCurrentUser();
    }

    openProfile() {
        this.router.navigate(['/dashboard/user', this.actualUser]);
    }

    openSettings() {}

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
        this.router.navigate(['/login']);
    }
}

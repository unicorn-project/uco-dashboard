import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApplicationService } from '../../../shared/services/application.service';
import { Application } from '../../../shared/entities/Application';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],
    providers: [ApplicationService],
})
export class SidebarComponent implements OnInit {
    isActive = false;
    showMenu = '';

    appList: Application[];

    constructor(public router: Router,
                public appService: ApplicationService) {
    }

    ngOnInit() {
        // get all apps
        this.appService.getApps().subscribe(applications => {
            this.appList = applications;
        });
    }

    openAppDetailView(app: Application) {
        this.router.navigate(['/dashboard/application', app.name]);
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            // { path: '', redirectTo: 'dashboard' },
            { path: '', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'application', loadChildren: './applications/applications.module#ApplicationsModule' },
            { path: 'components', loadChildren: './components/components.module#ComponentsModule' },
            { path: 'user', loadChildren: './user/user.module#UserModule' },
            { path: 'monitoring', loadChildren: './monitoring/monitoring.module#MonitoringModule' },
            { path: 'instances', loadChildren: './instances/instances.module#InstancesModule' },
            { path: 'sshkeys', loadChildren: './sshkeys/sshkeys.module#SshkeysModule' },
            { path: 'cloudresources', loadChildren: './resources/resources.module#ResourcesModule' },
            { path: 'idsrules', loadChildren: './idsrules/idsrules.module#IdsrulesModule' },
            { path: 'monitoringrules', loadChildren: './monitoringrules/monitoringrules.module#MonitoringrulesModule' },
            { path: 'create-instance', loadChildren: './create-instance/create-instance.module#CreateInstanceModule'}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}

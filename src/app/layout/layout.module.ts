import { NgModule } from '@angular/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './layout-components/sidebar/sidebar.component';
import { HeaderComponent } from './layout-components/header/header.component';
import { MaterialModule } from '../material.module';
import { DialogueComponent } from './dialogues/dialogue/dialogue.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddCompDialogComponent } from './dialogues/add-comp-dialog/add-comp-dialog.component';
import { MonitoringModule } from './monitoring/monitoring.module';
import { AddUserDialogComponent } from './dialogues/add-user-dialog/add-user-dialog.component';
import { EditAppSettingsDialogComponent } from './dialogues/edit-app-settings-dialog/edit-app-settings-dialog.component';
import { AddMonitoringWidgetComponent } from './dialogues/add-monitoring-widget/add-monitoring-widget.component';
import { ChangeMonitoringWidgetComponent } from './dialogues/change-monitoring-widget/change-monitoring-widget.component';
import { LinkCompToAppDialogComponent } from './dialogues/link-comp-to-app-dialog/link-comp-to-app-dialog.component';
import { EditUserProfileDialogComponent } from './dialogues/edit-user-profile-dialog/edit-user-profile-dialog.component';
import { EditDashboardDialogComponent } from './dialogues/edit-dashboard-dialog/edit-dashboard-dialog.component';
import { AddnewsshkeydialogComponent } from './dialogues/addnewsshkeydialog/addnewsshkeydialog.component';
import { EditSshKeyDialogComponent } from './dialogues/edit-ssh-key-dialog/edit-ssh-key-dialog.component';
import { DeleteSshKeyDialogComponent } from './dialogues/delete-ssh-key-dialog/delete-ssh-key-dialog.component';
import { AddNewProviderComponent } from './dialogues/add-new-provider/add-new-provider.component';
import { AddMonitoringruleDialogComponent } from './dialogues/add-monitoringrule-dialog/add-monitoringrule-dialog.component';
import { AddIDSRuleComponent } from './dialogues/add-idsrule/add-idsrule.component';
import { AddIDSRuleSetComponent } from './dialogues/add-idsrule-set/add-idsrule-set.component';
import { AddMetricComponent } from './dialogues/add-metric/add-metric.component';
import { AddElasticityPoliciesComponent } from './dialogues/add-elasticity-policies/add-elasticity-policies.component';
import { AddInstanceDialogComponent } from './dialogues/add-instance-dialog/add-instance-dialog.component';
import { ConfigureComponentNodeInstanceComponent } from './dialogues/configure-component-node-instance/configure-component-node-instance.component';

@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        MonitoringModule,
        NgbDropdownModule.forRoot(),
        MaterialModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [ LayoutComponent,
                    SidebarComponent,
                    HeaderComponent,
                    DialogueComponent,
                    AddCompDialogComponent,
                    AddUserDialogComponent,
                    EditAppSettingsDialogComponent,
                    AddMonitoringWidgetComponent,
                    ChangeMonitoringWidgetComponent,
                    LinkCompToAppDialogComponent,
                    EditUserProfileDialogComponent,
                    EditDashboardDialogComponent,
                    AddnewsshkeydialogComponent,
                    EditSshKeyDialogComponent,
                    DeleteSshKeyDialogComponent,
                    AddNewProviderComponent,
                    AddMonitoringruleDialogComponent,
                    AddIDSRuleComponent,
                    AddIDSRuleSetComponent,
                    AddMetricComponent,
                    AddElasticityPoliciesComponent,
                    AddInstanceDialogComponent,
                    ConfigureComponentNodeInstanceComponent
                ],

    entryComponents: [  DialogueComponent,
                        AddCompDialogComponent,
                        AddUserDialogComponent,
                        EditAppSettingsDialogComponent,
                        AddMonitoringWidgetComponent,
                        LinkCompToAppDialogComponent,
                        EditUserProfileDialogComponent,
                        EditDashboardDialogComponent,
                        AddnewsshkeydialogComponent,
                        DeleteSshKeyDialogComponent,
                        AddNewProviderComponent,
                        AddIDSRuleComponent,
                        AddIDSRuleSetComponent,
                        AddMetricComponent,
                        AddElasticityPoliciesComponent,
                        AddInstanceDialogComponent,
                        ConfigureComponentNodeInstanceComponent]
})
export class LayoutModule {}

export interface Metrics {
    value: string;
    viewValue: string;
    query: string;
}

export interface Timesteps {
    weight: string;
    name: string;
}

export class SelectMetric {
    metrics: Metrics[] = [
        {value: 'CPU App Time Average',
         viewValue: 'blockchain__d1__blockchainsolver_apps_cpu_cpu_time___average',
         query: 'query_range?query=blockchain__d1__blockchainsolver_apps_cpu_cpu_time___average'},
        {value: 'CPU System Time Average',
         viewValue: 'blockchain__d1__blockchainsolver_apps_cpu_system_cpu_time___average',
         query: 'query_range?query=blockchain__d1__blockchainsolver_apps_cpu_system_cpu_time___average'},
        {value: 'CPU User Time Average',
         viewValue: 'blockchain__d1__blockchainsolver_apps_cpu_user_cpu_time___average',
         query: 'query_range?query=blockchain__d1__blockchainsolver_apps_cpu_user_cpu_time___average'}
    ];
}

export class Datepicker {
    startDate = new Date();
    endDate = new Date();
}

export class SelectTimestep {
    times: Timesteps[] = [
        {weight: 's', name: 'second'},
        {weight: 'm', name: 'minute'},
        {weight: 'h', name: 'hour'},
        {weight: 'd', name: 'day'},
    ];
}

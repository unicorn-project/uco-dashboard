export class Monitoring {

    status: string;
    data: MonitoringData;

    constructor (status: string, data: MonitoringData) {

        this.status = status;
        this.data = data;

    }
}

export class MonitoringData {

    resultType: string;
    result: MonitoringResult[];

    constructor (resultType: string, result: MonitoringResult[]) {

        this.resultType = resultType;
        this.result = result;
    }
}

export class MonitoringResult {

    metric: MonitoringMetric;
    values: any;
    value: any;

    constructor (metric: MonitoringMetric, values: any, value: any) {

        this.metric = metric;
        this.values = values;
        this.value = value;
    }

}

export class MonitoringMetric {

    name: string;
    job: string;
    instance: string;
    family: string;
    chart: string;
    dimension: string;

    constructor (name: string, job: string, instance: string) {

        this.name = name;
        this.job = job;
        this.instance = instance;
    }


}

export class MonitoringCard {

    id: number;
    public name: string;
    public title: string;
    query: string;
    metric: string;
    data: any;

    constructor(id: number, name: string, title: string, query: string, metric: string, data: any) {
        this.id = id;
        this.name = name;
        this.title = title;
        this.query = query;
        this.metric = metric;
        this.data = data;
    }

    getName(): string {
        return this.name;
    }

    getTitle(): string {
        return this.title;
    }
}

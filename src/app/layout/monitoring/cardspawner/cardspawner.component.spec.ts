import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardspawnerComponent } from './cardspawner.component';

describe('CardspawnerComponent', () => {
  let component: CardspawnerComponent;
  let fixture: ComponentFixture<CardspawnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardspawnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardspawnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

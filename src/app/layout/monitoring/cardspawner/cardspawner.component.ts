import { Component, OnInit, ViewChild, ViewContainerRef, Input, ComponentFactoryResolver, ComponentRef } from '@angular/core';
import { MonitoringCard } from '../Monitoring';
import { Chart } from '../chart/chart';
import { ChartComponent } from '../chart/chart.component';
import { LinechartComponent } from '../chart/linechart/linechart.component';

@Component({
  selector: 'app-cardspawner',
  templateUrl: './cardspawner.component.html',
  styleUrls: ['./cardspawner.component.scss']
})
export class CardspawnerComponent implements OnInit {
  @ViewChild('monitoringCards', {read: ViewContainerRef}) VCR: ViewContainerRef;
  @Input() card: MonitoringCard;
  componentIndex = 0;
  componentReferences = [];

  constructor(private CFR: ComponentFactoryResolver) { }

  ngOnInit() {
    this.loadComponent();
  }

  loadComponent() {

    // const injector = Injector.create(this.VCR.parentInjector);
    const compfactory = this.CFR.resolveComponentFactory(LinechartComponent);

    const compRef: ComponentRef<Chart> = this.VCR.createComponent(compfactory);

    const currentComponent = compRef.instance;

    currentComponent.selfRef = currentComponent;
    currentComponent.index = ++this.componentIndex;
    currentComponent.card = this.card;
    currentComponent.compInteraction = this;

    this.componentReferences.push(currentComponent);
  }

  remove(index: number) {
    if (this.VCR.length < 1) {
      return;
    }

    console.log(this.componentReferences);
    const compRef = this.componentReferences.filter(x => x.instance.index === index)[0];
    const component: ChartComponent = <ChartComponent>compRef.instance;

    const vcrIndex: number = this.VCR.indexOf(compRef);

    this.VCR.remove(vcrIndex);

    this.componentReferences = this.componentReferences.filter(x => x.instance.index !== index);

  }

}

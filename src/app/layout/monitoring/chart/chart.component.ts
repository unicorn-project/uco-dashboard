export interface ChartComponent {
  index: number;
  selfRef: ChartComponent;
  data: any;
  compInteraction: MyInterface;
}

export interface MyInterface {
  remove(index: number);
}

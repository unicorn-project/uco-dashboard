import { MonitoringCard } from '../Monitoring';

export interface Chart {
    index: number;
    selfRef: Chart;
    card: MonitoringCard;
    compInteraction: MyInterface;
  }

export interface MyInterface {
remove(index: number);
}

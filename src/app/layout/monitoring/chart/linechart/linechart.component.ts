import { Component, Input, OnInit } from '@angular/core';
import { MonitoringCard, Monitoring, MonitoringData, MonitoringResult, MonitoringMetric } from '../../Monitoring';
import { Chart, MyInterface } from '../chart';
import { MonitoringService } from '../../../../shared/services/monitoring.service';
import { Observable, interval, pipe } from 'rxjs';
import { switchMap, startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-linechart',
  templateUrl: './linechart.component.html',
  styleUrls: ['./linechart.component.scss']
})
export class LinechartComponent implements Chart, OnInit {
  @Input() card: MonitoringCard;

  public index: number;
  public selfRef: LinechartComponent;
  public compInteraction: MyInterface;

  // chartelements
  chart: any;
  chartData = [];
  chartLabels = [];
  chartColors = [];
  chartOptions = {};

  // monitoring data
  query: string;
  monitoring: Monitoring;
  monitoringData: MonitoringData;
  monitoringResult: MonitoringResult[];
  metrices: MonitoringMetric[];

  pollingData: any;

  constructor(private dataService: MonitoringService) {}

  ngOnInit(): void {

    // query data
    this.query = this.card.query;
    console.log(this.query);

    // get monitoring data
    // this.pollingData = interval(100000)
    // .pipe(startWith(0))
    // .pipe(
    // switchMap(() =>

    this.dataService.getMonitoringDataWithQuery(this.query).subscribe( data => {

      console.log(data);
      this.chartData = [];

      this.monitoring = data;

      this.monitoringData = this.monitoring.data;

      // get the relevant monitoring data
      this.monitoringResult = this.getMonitoringResults(this.monitoringData);

      // count metrices
      this.metrices = this.getMetrices(this.monitoringResult);

      // create the charts
      this.createCharts(this.monitoringResult);
    });
  }

     // get monitoring results
  getMonitoringResults(monData: MonitoringData): MonitoringResult[] {
    const monRes: MonitoringResult[] = monData.result;
    return monRes;
  }

  // count metrices
  getMetrices(monRes: MonitoringResult[]): MonitoringMetric[] {
    const metrices: MonitoringMetric[] = [];
    // count metrices
    for (let i = 0; i < monRes.length; i++) {
      metrices.push(monRes[i].metric);
    }
    return metrices;
  }

  // create charts
  createCharts(monRes: MonitoringResult[]) {
    // declare charts array

    // create dataset
    const dimensions = [];
    const chartValues = [];
    const chartLabels = [];
    const colors = ['#FA5858', '#FA8258', '#FAAC58', '#F7D358', '#F4FA58', '#D0FA58', '#ACFA58', '#82FA58', '#58FA58',
                    '#58FA82', '#58FAAC', '#58FAD0', '#58FAF4'];

    if (monRes[0].value) {
      for (let i = 0; i < monRes.length; i++) {
        const monitoringValues = [];
        const monitoringLabels = [];
        monitoringValues.push(monRes[i].value[1]);
        monitoringLabels.push(new Date(Number(monRes[i].value[0]) * 1000).toTimeString().slice(0, 8));
        chartValues.push(monitoringValues);
        chartLabels.push(monitoringLabels);
        dimensions.push(monRes[i].metric.dimension);
      }
    } else {
      for (let i = 0; i < monRes.length; i++) {
        const monitoringValues = [];
        const monitoringLabels = [];
        for (let j = 0; j < monRes[i].values.length; j++) {
          monitoringValues.push(monRes[i].values[j][1]);
          monitoringLabels.push(new Date(Number(monRes[i].values[j][0]) * 1000).toTimeString().slice(0, 8));
        }
        chartValues.push(monitoringValues);
        chartLabels.push(monitoringLabels);
        dimensions.push(monRes[i].metric.dimension);
      }
    }

    for (let i = 0; i < chartValues.length; i++) {
      this.chartData.push({
      title: 'LineChart',
      data: chartValues[i],
      label: dimensions[i],
      fill: false
      });
      this.chartColors.push(
        {
          backgroundColor: colors[i],
          borderColor: colors[i]
        }
      );
    }

    this.chartLabels = chartLabels[0];
    this.chartOptions = {
      reponsive: true,
      legend: {position: 'right'},
      title: {text: this.card.metric, display: true}
    };
  }

  removeMe(index) {
    this.compInteraction.remove(index);
  }

}

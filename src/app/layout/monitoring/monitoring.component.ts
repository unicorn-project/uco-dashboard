import { Component, OnInit, OnDestroy, ElementRef, ChangeDetectorRef, Input } from '@angular/core';
import { MonitoringService } from '../../shared/services/monitoring.service';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { ChartService } from '../../shared/services/chart.service';
import { CardsService } from '../../shared/services/cards.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { AddMonitoringWidgetComponent } from '../dialogues/add-monitoring-widget/add-monitoring-widget.component';
import { MonitoringCard } from './Monitoring';
import { AppComponent } from 'src/app/shared/entities/AppComponent';

@Component({
  selector: 'app-monitoring',
  templateUrl: './monitoring.component.html',
  styleUrls: ['./monitoring.component.scss'],
  providers: [ MonitoringService, ChartService, CardsService ]
})

export class MonitoringComponent implements OnInit, OnDestroy {

  cards: MonitoringCard[];

  errMessage: string;

  appName: string;
  sub: any;
  pollingData: any;

  addNewWidgetRef: MatDialogRef<AddMonitoringWidgetComponent>;

  @Input() componentList: any;
  @Input() appMonitoringData: any;

  constructor(private route: ActivatedRoute,
              private cardService: CardsService,
              private dialog: MatDialog) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {this.appName = params['appName'];
    });

    this.getCards();

  }

  getCards() {
    this.cardService.getCards().subscribe( cards => {
      this.cards = cards;
    });
  }

  openAddNewWidgetDialog() {
    this.addNewWidgetRef = this.dialog.open(AddMonitoringWidgetComponent, {
      hasBackdrop: true,
      data: this.componentList
    });
    this.addNewWidgetRef
      .afterClosed()
      .subscribe(carddata => {
        // create query
        const query = this.createQuery(carddata.selectedMetric, carddata.selectedComponent);
        console.log(query);
        const card: MonitoringCard = new MonitoringCard(null,
          carddata.cardname,
          'chart',
          query,
          carddata.selectedMetric.value,
          {});
        console.log(card);

        // add card to server
        this.cardService.addCard(card).subscribe( cards => {
          this.getCards();
        });
      });
  }

  deleteWidget(cardId: number) {
    // delete card with this id
    this.cardService.removeCard(cardId).subscribe(cards => {
      this.getCards();
    });
  }

  // create monitoring query
  createQuery(selectedMetric: {}, selectedComponent: {name: string, compNodeHexID: string}): string {
    console.log(this.appMonitoringData);
      const query =
      'http://212.101.173.59:9090/api/v1/query_range?' +
      'query=netdata:iVb77JFucR:NstA7PtGKr:QpKQ9kuE46_cpu_cpu_percentage_average' +
      '&start=2019-02-01T10:10:30.781Z&end=2019-02-01T12:11:00.781Z&step=15m';
      /* string = 'http://212.101.173.59:9090/graph?g0.range_input=1h&g0.expr=netdata:'
        + this.appMonitoringData[0].appHexID
        + ':' + this.appMonitoringData[0].appInstHexID
        + ':' + selectedComponent.compNodeHexID
        + '_cpu_cpu_percentage_average&g0.tab=1'; */
    // const query: string = selectedQuery
    //             + '&start=' + startDate.toISOString()
    //             + '&end=' + endDate.toISOString()
    //             + '&step=' + steps.toString() + timeStepHeight;
    return query;
  }

  removeCard() {
     // remove last chart
     this.cardService.getCards().subscribe(cards => {
      this.cards = cards;
      const lastone = this.cards.length;
      this.cardService.removeCard(lastone).subscribe(card => {
        this.getCards();
      });
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonitoringRoutingModule } from './monitoring-routing.module';
import { MonitoringComponent } from './monitoring.component';
import { MaterialModule } from '../../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import { NgxEchartsModule } from 'ngx-echarts';
import { LinechartComponent } from './chart/linechart/linechart.component';
import { BarchartComponent } from './chart/barchart/barchart.component';
import { PiechartComponent } from './chart/piechart/piechart.component';
import { ChartsModule } from 'ng2-charts';
import { CardspawnerComponent } from './cardspawner/cardspawner.component';


@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    MonitoringRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AmazingTimePickerModule,
    NgxEchartsModule
  ],
  declarations: [
    MonitoringComponent,
    LinechartComponent,
    BarchartComponent,
    PiechartComponent,
    CardspawnerComponent
  ],
  exports: [
    MonitoringComponent
  ],
  entryComponents: [
    LinechartComponent,
    BarchartComponent,
    PiechartComponent
    ]
})
export class MonitoringModule { }

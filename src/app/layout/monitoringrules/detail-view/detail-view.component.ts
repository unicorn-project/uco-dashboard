import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MonitoringPluginService } from 'src/app/shared/services/monitoring-plugin.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Plugin, AddPlugin } from 'src/app/shared/entities/Plugins';
import { MatDialogRef, MatDialog } from '@angular/material';
import { AddMetricComponent } from '../../dialogues/add-metric/add-metric.component';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss'],
  providers: [MonitoringPluginService]
})
export class DetailViewComponent implements OnInit, OnDestroy {

  sub: any;
  pluginID: number;
  pluginName: string;
  plugin: Plugin;
  pluginTypes: string[] = ['Http', 'Socket', 'Download Conf'];
  metricDataTable = [];
  metricColoumnsToDisplay: string[] = ['friendlyName', 'unit'];

  form: FormGroup;
  nonEditable: boolean;
  editable: boolean;

  addMetricDialogRef: MatDialogRef<AddMetricComponent>;

  constructor( private route: ActivatedRoute,
               private pluginService: MonitoringPluginService,
               private dialog: MatDialog) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.pluginID = params['pluginID'];
    });

    this.nonEditable = true;
    this.editable = false;

    // this.createPluginDetailForm(this.nonEditable);
    this.pluginService.getPluginsByID(this.pluginID).subscribe(pluginData => {
      this.plugin = pluginData;
      this.pluginName = pluginData.name;

      console.log(pluginData);

      this.form = new FormGroup({
        name: new FormControl({
          value: pluginData.name,
          disabled: this.nonEditable
        }),
        moduleName: new FormControl({
          value: pluginData.moduleName,
          disabled: this.nonEditable
        }),
        downloadUrl: new FormControl({
          value: pluginData.downloadURL,
          disabled: this.nonEditable
        }),
        pluginType: new FormControl({
          value: pluginData.pluginType,
          disabled: this.nonEditable
        })
      });

      console.log(pluginData);
      // get metrices by Link
      this.pluginService.getMetricsByLink(pluginData._links.metrics.href).subscribe(metricsData => {
        this.metricDataTable = metricsData._embedded.metrics;
        });
    });
  }

  createPluginDetailForm(editableFlag: boolean) {
    this.pluginService.getPluginsByID(this.pluginID).subscribe(pluginData => {
      this.plugin = pluginData;
      this.pluginName = pluginData.name;

      this.form = new FormGroup({
        name: new FormControl({
          value: pluginData.name,
          disabled: editableFlag
        }),
        moduleName: new FormControl({
          value: pluginData.moduleName,
          disabled: editableFlag
        }),
        downloadUrl: new FormControl({
          value: pluginData.downloadURL,
          disabled: editableFlag
        }),
        pluginType: new FormControl({
          value: pluginData.pluginType,
          disabled: editableFlag
        })
      });
    });
  }

  editPlugin() {
    this.nonEditable = false;
    this.createPluginDetailForm(this.nonEditable);
  }

  saveEdit() {

    const editPlugin: AddPlugin = new AddPlugin(
      this.pluginID,
      this.form.controls['name'].value,
      this.form.controls['moduleName'].value,
      this.form.controls['downloadURL'].value,
      this.form.controls['pluginType'].value,
      '',
      '',
      false,
      false,
      false
    );

    console.log(editPlugin);

    this.pluginService.getPluginsByID(this.pluginID).subscribe(pluginData => {
    });
  }

  cancelEdit() {
    this.nonEditable = true;
    this.createPluginDetailForm(this.nonEditable);
  }

  addMetric() {
    this.addMetricDialogRef = this.dialog.open(AddMetricComponent, {
      hasBackdrop: true,
      width: '60%'
    });

    this.addMetricDialogRef.afterClosed()
      .pipe(filter( name => name))
      .subscribe(data => {
        console.log(data);
      });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}

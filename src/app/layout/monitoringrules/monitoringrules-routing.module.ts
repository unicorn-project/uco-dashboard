import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MonitoringrulesComponent } from './monitoringrules.component';
import { DetailViewComponent } from './detail-view/detail-view.component';
import { EditViewComponent } from './edit-view/edit-view.component';

const routes: Routes = [
  { path: '', component: MonitoringrulesComponent },
  { path: 'details/:pluginID', component: DetailViewComponent },
  { path: 'edit/:pluginID', component: EditViewComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoringrulesRoutingModule { }

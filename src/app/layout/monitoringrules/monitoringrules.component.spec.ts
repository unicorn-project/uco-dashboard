import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringrulesComponent } from './monitoringrules.component';

describe('MonitoringrulesComponent', () => {
  let component: MonitoringrulesComponent;
  let fixture: ComponentFixture<MonitoringrulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringrulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringrulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

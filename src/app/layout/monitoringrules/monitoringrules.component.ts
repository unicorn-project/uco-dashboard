import { Component, OnInit, ViewChild } from '@angular/core';
import { MonitoringPluginService } from 'src/app/shared/services/monitoring-plugin.service';
import { Plugin } from 'src/app/shared/entities/Plugins';
import { Router } from '@angular/router';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-monitoringrules',
  templateUrl: './monitoringrules.component.html',
  styleUrls: ['./monitoringrules.component.css'],
  providers: [MonitoringPluginService]
})
export class MonitoringrulesComponent implements OnInit {

  pluginTableData = [];
  pluginDataSource = new MatTableDataSource();
  columnsToDisplay: string[] = ['name', 'moduleName', 'dateCreated', 'metricsCounter', 'default', 'immutable', 'publicPlugin', 'actions'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  noPluginsAvailable: boolean;
  noComponentsAvailableMessage: boolean;

  constructor( private pluginService: MonitoringPluginService,
               private router: Router) { }

  ngOnInit() {
    this.noPluginsAvailable = false;
    this.noComponentsAvailableMessage = true;
    // get plugins
    this.pluginService.getPlugins().subscribe(pluginData => {
      console.log(pluginData);
      if (pluginData._embedded.plugins.length === 0) {
        this.noPluginsAvailable = true;
      }

      this.pluginTableData = pluginData._embedded.plugins;

      for (let i = 0; i < pluginData._embedded.plugins.length; i++) {
        this.pluginTableData[i].dateCreated = this.pluginTableData[i].dateCreated.toString().slice(0, 19);
        // get metrics of plugin to fill counter
        const plugin: Plugin = pluginData._embedded.plugins[i];
        this.pluginService.getMetricsByLink(plugin._links.metrics.href).subscribe(metricData => {
          this.pluginTableData[i].metricsCounter = metricData._embedded.metrics.length;
        });
      }
      this.pluginDataSource.data = this.pluginTableData;
      this.pluginDataSource.paginator = this.paginator;
      this.pluginDataSource.sort = this.sort;
    });
  }

  // filter table
  applyTableFilter(filterValue: string) {
    this.pluginDataSource.filter = filterValue.trim().toLowerCase();
  }

  openAddPluginDialog() {}

  openDetailView(plugin: Plugin) {
    this.router.navigate(['/dashboard/monitoringrules/details', plugin.pluginID]);
  }

  openEditView(plugin: Plugin) {
    this.router.navigate(['/dashboard/monitoringrules/edit', plugin.pluginID]);
  }

  delete(plugin: Plugin) {
    // todo: implement delete in monitoring rules service
  }

}

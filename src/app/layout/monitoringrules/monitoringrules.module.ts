import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonitoringrulesRoutingModule } from './monitoringrules-routing.module';
import { MonitoringrulesComponent } from './monitoringrules.component';
import { DetailViewComponent } from './detail-view/detail-view.component';
import { MaterialModule } from 'src/app/material.module';
import { EditViewComponent } from './edit-view/edit-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [MonitoringrulesComponent, DetailViewComponent, EditViewComponent],
  imports: [
    CommonModule,
    MonitoringrulesRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class MonitoringrulesModule { }

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { ResourcesService } from 'src/app/shared/services/resources.service';
import { AddProvider } from 'src/app/shared/entities/Provider';

@Component({
  selector: 'app-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss'],
  providers: [ResourcesService]
})
export class DetailViewComponent implements OnInit, OnDestroy {

  sub: any;
  providerName: string;
  providerID: number;

  form: FormGroup;

  // flags
  providerType: string;
  nonEditableResource: boolean;
  editableResource: boolean;

  constructor(  private route: ActivatedRoute,
                private router: Router,
                private resourceService: ResourcesService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.providerID = params['resourceID'];
    });

    // initialize editMode
    this.nonEditableResource = true;
    this.editableResource = false;

    // get provider Data
    this.resourceService.getProviderByID(this.providerID).subscribe(providerData => {
      this.providerName = providerData.name;
      this.providerType = providerData.providerType.name;

        this.form = new FormGroup ({
          name: new FormControl({
            value: providerData.name,
            disabled: this.nonEditableResource}),
          resourceType: new FormControl({
            value: this.providerType,
            disabled: true}),
            imageID: new FormControl({
              value: providerData.imageID,
              disabled: this.nonEditableResource
            }),
            networkID : new FormControl({
              value: providerData.networkID,
              disabled: this.nonEditableResource
            }),
            nexusIP: new FormControl({
              value: providerData.nexusIP,
              disabled: this.nonEditableResource
            }),
            proxy: new FormControl({
              value: providerData.proxy,
              disabled: this.nonEditableResource
            }),
            endpoint: new FormControl({
              value: providerData.endpoint,
              disabled: this.nonEditableResource
            }),
            domain: new FormControl({
              value: providerData.domain,
              disabled: this.nonEditableResource
            }),
            username: new FormControl({
              value: providerData.username,
              disabled: this.nonEditableResource
            }),
            password: new FormControl({
              value: providerData.password,
              disabled: this.nonEditableResource
            }),
            project: new FormControl({
              value: providerData.project,
              disabled: this.nonEditableResource
            }),
            publicKey: new FormControl({
              value: providerData.publicKey,
              disabled: this.nonEditableResource
            }),
            privateKey: new FormControl({
              value: providerData.privateKey,
              disabled: this.nonEditableResource
            }),
            meshIdentifier: new FormControl({
              value: providerData.meshIdentifier,
              disabled: this.nonEditableResource
            })
            // regions: new FormControl({
            //   value: providerData.,
            //   disabled: this.nonEditableResource
            // }),
        });
    });
  }

  editResource() {
    this.nonEditableResource = false;
    this.editableResource = true;

     // get provider Data
     this.resourceService.getProviderByID(this.providerID).subscribe(providerData => {
      this.providerName = providerData.name;

      // get provider Type
      this.resourceService.getProviderTypeByLink(providerData._links.providerType.href).subscribe(typeData => {
        this.providerType = typeData.name;

        // create formGroups
        this.form = new FormGroup ({
          name: new FormControl({
            value: providerData.name,
            disabled: this.nonEditableResource}),
          resourceType: new FormControl({
            value: this.providerType,
            disabled: true}),
          imageID: new FormControl({
            value: providerData.imageID,
            disabled: this.nonEditableResource
          }),
          networkID : new FormControl({
            value: providerData.networkID,
            disabled: this.nonEditableResource
          }),
          nexusIP: new FormControl({
            value: providerData.nexusIP,
            disabled: this.nonEditableResource
          }),
          proxy: new FormControl({
            value: providerData.proxy,
            disabled: this.nonEditableResource
          }),
          endpoint: new FormControl({
            value: providerData.networkID,
            disabled: this.nonEditableResource
          }),
          domain: new FormControl({
            value: providerData.domain,
            disabled: this.nonEditableResource
          }),
          username: new FormControl({
            value: providerData.username,
            disabled: this.nonEditableResource
          }),
          password: new FormControl({
            value: providerData.password,
            disabled: this.nonEditableResource
          }),
          project: new FormControl({
            value: providerData.project,
            disabled: this.nonEditableResource
          }),
          publicKey: new FormControl({
            value: providerData.publicKey,
            disabled: this.nonEditableResource
          }),
          privateKey: new FormControl({
            value: providerData.privateKey,
            disabled: this.nonEditableResource
          }),
          meshIdentifier: new FormControl({
            value: providerData.meshIdentifier,
            disabled: this.nonEditableResource
          })
          // regions: new FormControl({
          //   value: providerData.,
          //   disabled: this.nonEditableResource
          // }),
        });
        console.log(this.form);
      });

    });
  }

  saveEditedValues() {
    this.resourceService.getProviderByID(this.providerID).subscribe(providerData => {
      this.providerName = providerData.name;

      // get provider Type
      this.resourceService.getProviderTypeByLink(providerData._links.providerType.href).subscribe(typeData => {
        this.providerType = typeData.name;

        const provider: AddProvider = new AddProvider(
          true,
          '',
          this.form.controls['domain'].value,
          this.form.controls['endpoint'].value,
          this.form.controls['imageID'].value,
          this.form.controls['meshIdentifier'].value,
          this.form.controls['name'].value,
          this.form.controls['networkID'].value,
          this.form.controls['nexusIP'].value,
          this.form.controls['password'].value,
          this.form.controls['privateKey'].value,
          this.form.controls['project'].value,
          this.providerID,
          '/api/v2/providerTypes/' + typeData.id,
          this.form.controls['proxy'].value,
          this.form.controls['privateKey'].value,
          this.form.controls['username'].value
        );

        this.resourceService.editProvider(provider).subscribe( addProviderData => {
          console.log(addProviderData);
          this.router.navigate(['/dashboard/cloudresources']);
        });
      });
    });

  }

  cancelEdit() {
    this.nonEditableResource = true;
    this.editableResource = false;

    // get provider Data
    this.resourceService.getProviderByID(this.providerID).subscribe(providerData => {
      this.providerName = providerData.name;

      // get provider Type
      this.resourceService.getProviderTypeByLink(providerData._links.providerType.href).subscribe(typeData => {
        this.providerType = typeData.name;

        // create formGroups
        this.form = new FormGroup ({
          name: new FormControl({
            value: providerData.name,
            disabled: this.nonEditableResource}),
          resourceType: new FormControl({
            value: this.providerType,
            disabled: true}),
          imageID: new FormControl({
            value: providerData.imageID,
            disabled: this.nonEditableResource
          }),
          networkID : new FormControl({
            value: providerData.networkID,
            disabled: this.nonEditableResource
          }),
          nexusIP: new FormControl({
            value: providerData.nexusIP,
            disabled: this.nonEditableResource
          }),
          proxy: new FormControl({
            value: providerData.proxy,
            disabled: this.nonEditableResource
          }),
          endpoint: new FormControl({
            value: providerData.networkID,
            disabled: this.nonEditableResource
          }),
          domain: new FormControl({
            value: providerData.domain,
            disabled: this.nonEditableResource
          }),
          username: new FormControl({
            value: providerData.username,
            disabled: this.nonEditableResource
          }),
          password: new FormControl({
            value: providerData.password,
            disabled: this.nonEditableResource
          }),
          project: new FormControl({
            value: providerData.project,
            disabled: this.nonEditableResource
          }),
          publicKey: new FormControl({
            value: providerData.publicKey,
            disabled: this.nonEditableResource
          }),
          privateKey: new FormControl({
            value: providerData.privateKey,
            disabled: this.nonEditableResource
          }),
          meshIdentifier: new FormControl({
            value: providerData.meshIdentifier,
            disabled: this.nonEditableResource
          })
          // regions: new FormControl({
          //   value: providerData.,
          //   disabled: this.nonEditableResource
          // }),
        });
        console.log(this.form);
      });
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ResourcesService } from 'src/app/shared/services/resources.service';
import { AddProvider } from 'src/app/shared/entities/Provider';

@Component({
  selector: 'app-edit-view',
  templateUrl: './edit-view.component.html',
  styleUrls: ['./edit-view.component.scss'],
  providers: [ResourcesService]
})
export class EditViewComponent implements OnInit, OnDestroy {

  sub: any;
  providerName: string;
  providerID: number;

  form: FormGroup;
  // flags
  providerType: string;

  constructor(  private route: ActivatedRoute,
                private resourceService: ResourcesService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.providerID = params['resourceID'];
    });

    // get provider Data
    this.resourceService.getProviderByID(this.providerID).subscribe(providerData => {
      this.providerName = providerData.name;

      // get provider Type
      this.resourceService.getProviderTypeByLink(providerData._links.providerType.href).subscribe(typeData => {
        this.providerType = typeData.name;

        // create formGroups

        this.form = new FormGroup ({
          name: new FormControl({
            value: providerData.name,
            disabled: false}),
          resourceType: new FormControl({
            value: this.providerType,
            disabled: true}),
            imageID: new FormControl({
              value: providerData.imageID,
              disabled: false
            }),
            networkID : new FormControl({
              value: providerData.networkID,
              disabled: false
            }),
            nexusIP: new FormControl({
              value: providerData.nexusIP,
              disabled: false
            }),
            proxy: new FormControl({
              value: providerData.proxy,
              disabled: false
            }),
            endpoint: new FormControl({
              value: providerData.endpoint,
              disabled: false
            }),
            domain: new FormControl({
              value: providerData.domain,
              disabled: false
            }),
            username: new FormControl({
              value: providerData.username,
              disabled: false
            }),
            password: new FormControl({
              value: providerData.password,
              disabled: false
            }),
            project: new FormControl({
              value: providerData.project,
              disabled: false
            }),
            publicKey: new FormControl({
              value: providerData.publicKey,
              disabled: false
            }),
            privateKey: new FormControl({
              value: providerData.privateKey,
              disabled: false
            }),
            meshIdentifier: new FormControl({
              value: providerData.meshIdentifier,
              disabled: false
            })
            // regions: new FormControl({
            //   value: providerData.,
            //   disabled: this.nonEditableResource
            // }),
        });
      });

    });
  }

  saveEditedValues() {
    this.resourceService.getProviderByID(this.providerID).subscribe(providerData => {
      this.providerName = providerData.name;

      // get provider Type
      this.resourceService.getProviderTypeByLink(providerData._links.providerType.href).subscribe(typeData => {
        this.providerType = typeData.name;

        const provider: AddProvider = new AddProvider(
          true,
          '',
          this.form.controls['domain'].value,
          this.form.controls['endpoint'].value,
          this.form.controls['imageID'].value,
          this.form.controls['meshIdentifier'].value,
          this.form.controls['name'].value,
          this.form.controls['networkID'].value,
          this.form.controls['nexusIP'].value,
          this.form.controls['password'].value,
          this.form.controls['privateKey'].value,
          this.form.controls['project'].value,
          this.providerID,
          '/api/v2/providerTypes/' + typeData.id,
          this.form.controls['proxy'].value,
          this.form.controls['privateKey'].value,
          this.form.controls['username'].value
        );

        this.resourceService.editProvider(provider).subscribe( addProviderData => {
          console.log(addProviderData);
        });
      });
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}

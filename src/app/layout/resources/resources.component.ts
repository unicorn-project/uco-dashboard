import { Component, OnInit, ViewChild } from '@angular/core';
import { ResourcesService } from 'src/app/shared/services/resources.service';
import { Provider, AddNewProvider } from 'src/app/shared/entities/Provider';
import { MatDialogRef, MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { AddNewProviderComponent } from '../dialogues/add-new-provider/add-new-provider.component';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.css'],
  providers: [ResourcesService]
})
export class ResourcesComponent implements OnInit {

  resourceTableData = [];
  resourceSourceData = new MatTableDataSource();
  displayedColumns = ['ResourceName', 'Type', 'Status', 'dateCreated', 'default', 'actions'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  addNewProviderDialogRef: MatDialogRef<AddNewProviderComponent>;

  constructor(  private resourceService: ResourcesService,
                private dialog: MatDialog,
                private router: Router) { }

  ngOnInit() {
    // get all resources available
    this.getAllProvidersAndSetResourceTableData();
  }

  getAllProvidersAndSetResourceTableData() {
    this.resourceService.getProviders().subscribe(providers => {
      this.resourceTableData = providers;

      for (let i = 0; i < providers.length; i++) {
        this.resourceTableData[i].type = providers[0].providerType.friendlyName;
        this.resourceTableData[i].dateCreated = this.resourceTableData[i].dateCreated.toString().slice(0, 19);
      }

      this.resourceSourceData.data = this.resourceTableData;
      this.resourceSourceData.paginator = this.paginator;
      this.resourceSourceData.sort = this.sort;
    });
  }

  // filter table
  applyTableFilter(filterValue: string) {
    this.resourceSourceData.filter = filterValue.trim().toLowerCase();
  }
  // set provider to default
  setToDefault(provider: Provider) {
    // set to default
    this.resourceService.setToDefaultProvider(provider.providerID).subscribe(() => {
      // get new table
      this.getAllProvidersAndSetResourceTableData();
    });
  }

  // delete resource
  deleteResource(provider: Provider) {
    this.resourceService.deleteProvider(provider.providerID).subscribe(() => {
      // get new table
      this.getAllProvidersAndSetResourceTableData();
    });
  }



  // add new resource
  openAddNewResource() {
    this.addNewProviderDialogRef = this.dialog.open(AddNewProviderComponent, {
      hasBackdrop: true,
      data: '',
      width: '80%'
    });

    this.addNewProviderDialogRef
      .afterClosed()
      .pipe(filter(name => name))
      .subscribe(newProviderDialogData => {
        // check is the provider already exists
        // add new resource to database
        const providerTypeUrl: string = 'api/v2/providerTypes/' + newProviderDialogData.resourceType.id;
        const newProvider: AddNewProvider = new AddNewProvider(
          true,
          false,
          newProviderDialogData.domain,
          newProviderDialogData.endpoint,
          newProviderDialogData.imageID,
          newProviderDialogData.meshIdentifier,
          newProviderDialogData.name,
          newProviderDialogData.networkID,
          newProviderDialogData.nexusIP,
          newProviderDialogData.password,
          newProviderDialogData.privateKey,
          newProviderDialogData.project,
          providerTypeUrl,
          newProviderDialogData.proxy,
          newProviderDialogData.publicKey,
          newProviderDialogData.username
        );
        this.resourceService.addProvider(newProvider).subscribe(newProviderList => {
          this.getAllProvidersAndSetResourceTableData();
        });
      });
  }

  // edit existing resource dialog
  openEditResourceDialog(provider: Provider) {
    this.router.navigate(['/dashboard/cloudresources/edit/', provider.providerID]);

  }

  openResourceDetails(provider: Provider) {
    this.router.navigate(['/dashboard/cloudresources/details/', provider.providerID]);
  }

}

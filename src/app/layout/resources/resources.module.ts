import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResourcesRoutingModule } from './resources-routing.module';
import { ResourcesComponent } from './resources.component';
import { MaterialModule } from 'src/app/material.module';
import { DetailViewComponent } from './detail-view/detail-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditViewComponent } from './edit-view/edit-view.component';

@NgModule({
  declarations: [ResourcesComponent, DetailViewComponent, EditViewComponent],
  imports: [
    CommonModule,
    ResourcesRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ResourcesModule { }

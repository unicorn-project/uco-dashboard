import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SshkeysComponent } from './sshkeys.component';
import { DetailViewComponent } from './detail-view/detail-view.component';
import { EditViewComponent } from './edit-view/edit-view.component';

const routes: Routes = [
  { path: '', component: SshkeysComponent },
  { path: 'detail/:sshID', component: DetailViewComponent},
  { path: 'edit/:sshID', component: EditViewComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SshkeysRoutingModule { }

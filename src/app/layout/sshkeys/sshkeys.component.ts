import { Component, OnInit, ViewChild } from '@angular/core';
import { SshkeysService } from 'src/app/shared/services/sshkeys.service';
import { MatTableDataSource, MatDialogRef, MatDialog, MatSnackBar, MatPaginator, MatSort } from '@angular/material';
import { SSHKey } from 'src/app/shared/entities/SSHKey';
import { AddnewsshkeydialogComponent } from '../dialogues/addnewsshkeydialog/addnewsshkeydialog.component';
import { filter } from 'rxjs/operators';
import { EditSshKeyDialogComponent } from '../dialogues/edit-ssh-key-dialog/edit-ssh-key-dialog.component';
import { DeleteSshKeyDialogComponent } from '../dialogues/delete-ssh-key-dialog/delete-ssh-key-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sshkeys',
  templateUrl: './sshkeys.component.html',
  styleUrls: ['./sshkeys.component.scss']
})
export class SshkeysComponent implements OnInit {

  sshTableData = [];
  dataSource = new MatTableDataSource();
  displayedColumns = ['SSHKeyName', 'dateCreated', 'default', 'actions'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  addNewSSHKeyDialogRef: MatDialogRef<AddnewsshkeydialogComponent>;
  editSSHKeyDialogRef: MatDialogRef<EditSshKeyDialogComponent>;
  deleteSSHKeyDialogRef: MatDialogRef<DeleteSshKeyDialogComponent>;

  constructor( private sshService: SshkeysService,
               private dialog: MatDialog,
               private snackbar: MatSnackBar,
               private router: Router) { }

  ngOnInit() {

    // get all sshKey
    this.getSSHKeyListAndSetDataTable();
  }

  getSSHKeyListAndSetDataTable () {
    this.sshService.getAllSshKeys().subscribe(sshKeys => {
      this.sshTableData = sshKeys;

      for (let i = 0; i < this.sshTableData.length; i++) {
        this.sshTableData[i].dateCreated = this.sshTableData[i].dateCreated.toString().slice(0, 19);
      }
      this.dataSource.data = this.sshTableData;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  makeKeyDefault(sshKey: SSHKey) {
    this.sshService.setToDefault(sshKey.id).subscribe(data => {
      this.getSSHKeyListAndSetDataTable();
    });
  }

  // filter table
  applyTableFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openEditSSHKeyView(ssh: SSHKey) {
    this.router.navigate(['/dashboard/sshkeys/edit', ssh.id]);
  }

  openDetailSSHKeyView(ssh: SSHKey) {
    this.router.navigate(['/dashboard/sshkeys/detail', ssh.id]);
  }

  deleteSSH(sshKey: SSHKey) {
    this.sshService.deleteSSHKey(sshKey.id).subscribe(data => {
      this.getSSHKeyListAndSetDataTable();
    });
  }

  openAddSSHKeyDialog() {
    this.addNewSSHKeyDialogRef = this.dialog.open(AddnewsshkeydialogComponent, {
      hasBackdrop: true
    });

    this.addNewSSHKeyDialogRef
      .afterClosed()
      .pipe(filter(name => name))
      .subscribe( data => {
        const newSSH = {
          friendlyName: data.name,
          sshKey: data.sshkey,
          defaultSSH: data.isDefault
        };

        // add to database
        this.sshService.addNewSSHKey(newSSH).subscribe( newSSHData => {
          this.sshService.getAllSshKeys().subscribe(sshKeys => {
            this.sshTableData = sshKeys;

            for (let i = 0; i < this.sshTableData.length; i++) {
              this.sshTableData[i].dateCreated = this.sshTableData[i].dateCreated.toString().slice(0, 19);
            }
          });
        });

        // snackbar to open
        this.snackbar.open('SSH Key was added.', '', {
          verticalPosition: 'bottom',
          duration: 2000,
          panelClass: ['unicorn-snackbar']
        });
      });
  }

  openEditSSHKeyDialog(sshkey: SSHKey) {

    this.editSSHKeyDialogRef = this.dialog.open(EditSshKeyDialogComponent, {
      hasBackdrop: true,
      data: sshkey
    });

    this.editSSHKeyDialogRef
      .afterClosed()
      .pipe(filter(name => name))
      .subscribe( data => {
        const updatedSSH = new SSHKey(
          sshkey.id,
          data.name,
          data.sshkey,
          data.isDefault
        );
        console.log(updatedSSH);

        this.sshService.updateSSHKey(sshkey.id, updatedSSH).subscribe( deleteData => {
          this.sshService.getAllSshKeys().subscribe(sshKeys => {
            this.sshTableData = sshKeys;

            for (let i = 0; i < this.sshTableData.length; i++) {
              this.sshTableData[i].dateCreated = this.sshTableData[i].dateCreated.toString().slice(0, 19);
            }
          });
        });
      });
  }

  openDeleteSSHKeyDialog(sshkey: SSHKey) {
    this.deleteSSHKeyDialogRef = this.dialog.open(DeleteSshKeyDialogComponent, {
      hasBackdrop: true,
      data: sshkey
    });

    this.deleteSSHKeyDialogRef
      .afterClosed()
      .subscribe(data => {
        this.sshService.deleteSSHKey(sshkey.id).subscribe(deleteData => {
          this.sshService.getAllSshKeys().subscribe(sshKeys => {
            this.sshTableData = sshKeys;

            for (let i = 0; i < this.sshTableData.length; i++) {
              this.sshTableData[i].dateCreated = this.sshTableData[i].dateCreated.toString().slice(0, 19);
            }
          });
        });

         // snackbar to open
         this.snackbar.open('SSH Key was deleted.', '', {
          verticalPosition: 'bottom',
          duration: 2000,
          panelClass: ['unicorn-snackbar']
        });
      });
  }

  openDetailView() {}

}

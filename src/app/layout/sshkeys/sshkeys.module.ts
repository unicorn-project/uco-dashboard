import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SshkeysRoutingModule } from './sshkeys-routing.module';
import { SshkeysComponent } from './sshkeys.component';
import { MaterialModule } from 'src/app/material.module';
import { DetailViewComponent } from './detail-view/detail-view.component';
import { EditViewComponent } from './edit-view/edit-view.component';

@NgModule({
  declarations: [SshkeysComponent, DetailViewComponent, EditViewComponent],
  imports: [
    CommonModule,
    SshkeysRoutingModule,
    MaterialModule
  ]
})
export class SshkeysModule { }

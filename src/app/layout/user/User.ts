export class User {

    id: number;
    username: string;
    password: string;
    email: string;
    role: string;
    phone: string;
    detailRow: boolean;


    constructor(id: number, username: string, password: string, email: string, role: string, phone: string) {

      this.id = id;
      this.username = username;
      this.password = password;
      this.email = email;
      this.role = role;
      this.phone = phone;
      this.detailRow = true;
    }


  }

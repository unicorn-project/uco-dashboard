import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../../../shared/entities/User';
import { UserService } from '../../../shared/services/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss'],
  providers: [UserService]
})
export class UserCreateComponent implements OnInit, OnDestroy {

  id: number;
  user: User;
  userForm: FormGroup;
  private sub: any;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private userService: UserService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {this.id = params['id'];
    });

  // create new user form
    this.userForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.pattern('[^@]*@[^ @]*')]),
      role: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required)
    });

  // update user with existing id
    if (this.id) {
      this.userService.findById(this.id).subscribe(
        user => {
          this.id = user.id;
          this.userForm.patchValue({
            username: user.username,
            password: user.password,
            email: user.email
          });
        }, error => {
          console.log(error);
        }
          );
    }
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  onSubmit() {
    if (this.userForm.valid) {
    // check if const or let user
      if (this.id) {
        const user: User = new User(
        this.id,
        this.userForm.controls['username'].value,
        this.userForm.controls['firstname'].value,
        this.userForm.controls['lastname'].value,
        this.userForm.controls['email'].value,
        this.userForm.controls['phone'].value,
        );

      this.userService.updateUser(user).subscribe();

    } else {
      const user: User = new User(null,
        this.userForm.controls['username'].value,
        this.userForm.controls['firstname'].value,
        this.userForm.controls['lastname'].value,
        this.userForm.controls['email'].value,
        this.userForm.controls['phone'].value,
        );

      this.userService.saveUser(user).subscribe();
    }
    this.userForm.reset();
    this.router.navigate(['/user']);

    }
  }

  redirectUserPage() {
    this.router.navigate(['/user']);
  }


}

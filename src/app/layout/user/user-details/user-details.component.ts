import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../../shared/services/user.service';
import { User } from '../../../shared/entities/User';
import { MatDialogRef, MatDialog } from '@angular/material';
import { EditUserProfileDialogComponent } from '../../dialogues/edit-user-profile-dialog/edit-user-profile-dialog.component';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss'],
  providers: [UserService]
})
export class UserDetailsComponent implements OnInit {

  username: string;
  sub: any;
  actualUser: User;
  displayedColumns: string[] = ['attribute', 'data'];
  dataSource: any;

  // dialogs
  editUserProfilDialogRef: MatDialogRef<EditUserProfileDialogComponent>;

  constructor( private route: ActivatedRoute,
               private userService: UserService,
               private dialog: MatDialog) {
    this.sub = this.route.params.subscribe(params => {
      this.username = params['username'];
    });
   }

  ngOnInit() {
    this.userService.findAllUsers().subscribe( userdata => {

      // find user data
      for (let i = 0; i < userdata._embedded.users.length; i++) {
        if (userdata._embedded.users[i].username === this.username) {
          this.actualUser = userdata._embedded.users[i];
          break;
        }
      }
      const DATA = [
        {attribute: 'First Name:', data: this.actualUser.firstName},
        {attribute: 'Last Name:', data: this.actualUser.lastName},
        {attribute: 'Email Address:', data: this.actualUser.email},
        {attribute: 'Phone Number:', data: this.actualUser.phone}
      ];

      this.dataSource = DATA;
    });
  }

  openEditUserProfileDialog() {
    this.editUserProfilDialogRef = this.dialog.open(EditUserProfileDialogComponent, {
      hasBackdrop: true,
      width: '80%',
      data: this.actualUser
    });

    this.editUserProfilDialogRef
      .afterClosed()
        .pipe(filter(email => email))
        .subscribe( data => {
          const user: User = this.actualUser;
          user.firstName = data.firstname;
          user.lastName = data.lastname;
          user.email = data.email;
          user.phone = data.phone;
          user.notificationEmailEnabled = data.emailnotifications;
          user.notificationWebEnabled = data.webnotifications;

          /*this.userService.updateUser(user, this.actualUser.id).subscribe(editdata => {
            this.userService.findById(this.actualUser.id).subscribe( newuser => {
              this.actualUser = newuser;
            });
          });*/
        });
  }

}

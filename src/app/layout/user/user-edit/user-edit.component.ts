import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { User } from '../../../shared/entities/User';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  user: User;
  form: FormGroup;

  constructor(
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({});
    this.form.addControl('firstName', this.formBuilder.control(''));
    this.form.addControl('lastName', this.formBuilder.control(''));
    this.form.addControl('phone', this.formBuilder.control(''));
    this.form.addControl('newPassword', this.formBuilder.control(''));
    this.form.addControl('verifyPassword', this.formBuilder.control(''));

    this.authenticationService.getAuthenticatedUser().subscribe(
      user => {
        this.user = user;

        this.form.setValue(
          {
            firstName: user.firstName,
            lastName: user.lastName,
            phone: user.phone,
            newPassword: '',
            verifyPassword: '',
          }
        );
      }
    );
  }

  submit(): void {
    const user = {
      id: this.user.id,
      username: this.user.username,
      firstName: this.form.controls['firstName'].value,
      lastName: this.form.controls['lastName'].value,
      email: this.user.email,
      phone: this.form.controls['phone'].value,
      password: this.form.controls['newPassword'].value,
      newPassword: this.form.controls['newPassword'].value,
      verifyPassword: this.form.controls['verifyPassword'].value,
      country: {
        id: this.user.country.id,
      },
      organization: {
        id: this.user.organization.id,
        name: this.user.organization.name,
      },
      role: this.user.role,
    };

    this.userService.updateUser(user).subscribe();
  }
}

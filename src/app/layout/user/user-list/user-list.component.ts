/**
 * this is responsible for calling services and get the user details
 **/
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../../shared/entities/User';
import { UserService } from '../../../shared/services/user.service';
import { Router } from '@angular/router';
import { trigger, state, style, transition } from '../../../../../node_modules/@angular/animations';
import { DataSource } from '../../../../../node_modules/@angular/cdk/collections';
import { Observable} from 'rxjs/observable';
import 'rxjs/add/observable/of';
import { of } from '../../../../../node_modules/rxjs/observable/of';
import { detailedExpand } from '../../../router.animations';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogRef } from '../../../../../node_modules/@angular/material';
import { merge } from '../../../../../node_modules/rxjs/observable/merge';
import { startWith, switchMap } from '../../../../../node_modules/rxjs/operators';
import { AddUserDialogComponent } from '../../dialogues/add-user-dialog/add-user-dialog.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  providers: [UserService],
  animations: [detailedExpand()]
})

export class UserListComponent implements OnInit {

  users: User[];
  expandedElement: any;
  displayedColumns = ['User', 'email'];
  userDatabase: UserDatabase | null;
  dataSource = new MatTableDataSource();
  rows = [];

  addNewUserDialogRef: MatDialogRef<AddUserDialogComponent>;

  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');


  constructor(private userService: UserService,
              private router: Router,
              private dialog: MatDialog) { }

  // get all users when component is loading
  ngOnInit() {
    this.userDatabase = new UserDatabase(this.userService);
    this.userDatabase.getAllUsers().subscribe(data => {
      this.dataSource.data = data._embedded.users;
  });
}

  // method getAllUsers
  getAllUsers() {
    this.userService.findAllUsers()
      .subscribe(data => {
        this.users = data._embedded.users;
      },
      err => {
        console.log(err);
      }
    );
  }

  // redirect to new user page
  redirectNewUserPage() {
    this.router.navigate(['/user/create']);
  }

  // edit user page
  editUserPage(user: User) {
    if (user) {
      this.router.navigate(['/user/edit', user.id]);
    }
  }

  // delete user
  deleteUser(user: User) {
    if (user) {
      this.userService.deleteUserById(user.id).subscribe(
      data => {
        this.getAllUsers();
        this.router.navigate(['/user']);
      });
    }
  }
  // show all apps of a user
  // first step: go back to applications list
  listUserApps(user: User) {
    this.router.navigate(['/application/userapps']);
  }

  openAddUserDialog() {
    // open the dialog
    this.addNewUserDialogRef = this.dialog.open(AddUserDialogComponent, {
      hasBackdrop: true
    });

    // after closing the dialog
    this.addNewUserDialogRef
      .afterClosed()
      .subscribe(result => {
        const newUser = new User( null,
                                  result.username,
                                  result.firstname,
                                  result.lastname,
                                  result.email,
                                  result.phone,
        );
        this.userService.saveUser(newUser).subscribe();
        this.userService.findAllUsers().subscribe(
          data => console.log(data)
        );

        this.userDatabase = new UserDatabase(this.userService);
        this.userDatabase.getAllUsers().subscribe(data => {
        this.dataSource.data = data._embedded.users;

        console.log(this.dataSource.data);
        });
      });
  }

}

export class UserDatabase {
  constructor(private userService: UserService) {}

  getAllUsers() {
    return this.userService.findAllUsers();
  }
}

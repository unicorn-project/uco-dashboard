import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UserRoutingModule } from './user-routing.module';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from '../../shared/services/auth-interceptor';
import { AuthenticationService } from '../../shared/services/authentication.service';

import { MaterialModule } from '../../material.module';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserService } from 'src/app/shared/services/user.service';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  declarations: [UserEditComponent],
  providers: [AuthenticationService, UserService,
    {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }]
})
export class UserModule { }

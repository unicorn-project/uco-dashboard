import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { AuthenticationService } from '../shared/services/authentication.service';
import { routerTransition } from '../router.animations';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [AuthenticationService],
  animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

  model: any = {};
  loading = false;
  error = '';

  constructor(private router: Router,
              private authService: AuthenticationService,
              private snackbar: MatSnackBar) { }

  ngOnInit() {
    // reset login status
    this.authService.logout();
  }

  login () {
    this.authService.login(this.model.username, this.model.password).subscribe(
      isLoggedIn => {
        if (isLoggedIn) {
          localStorage.setItem('isLoggedin', 'true');
          this.router.navigate(['dashboard']);
        } else {
          this.snackbar.open(
            'Login failed',
            '',
            {
              duration: 1500
            }
          );
        }
      },
      () => {
        this.error = 'username or password is incorrect';
      }
    );
  }

  goToRegistrationPage () {
    this.router.navigate(['registration']);
  }

}


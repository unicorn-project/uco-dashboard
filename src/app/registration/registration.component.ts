import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../shared/services/authentication.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { User } from '../shared/entities/User';
import { routerTransition } from '../router.animations';
import { Country } from '../shared/entities/Countries';
import { MatSnackBar } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';
import { RestResponseSPA } from '../shared/entities/RestResponseSPA';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  animations: [routerTransition()],
  providers: []
})
export class RegistrationComponent implements OnInit {

  registerMessage: string;
  registerForm: FormGroup;
  user: User;

  CountryList: String[] = [];
  Countries: Country[];

  constructor(private router: Router,
              private authService: AuthenticationService,
              private snackbar: MatSnackBar) { }

  ngOnInit() {

    // get countries from database
    /*this.sharedService.getCountries().subscribe(countries => {
      this.Countries = countries._embedded.countries;
      for (let i = 0; i < this.Countries.length; i++) {
        this.CountryList.push(this.Countries[i].name);
      }
    });*/
    this.CountryList.push('Germany');

        // create new user form
        this.registerForm = new FormGroup({
          password: new FormControl('', Validators.required),
          email: new FormControl('', [Validators.required, Validators.pattern('[^@]*@[^ @]*')]),
          username: new FormControl('', Validators.required),
          firstname: new FormControl('', Validators.required),
          lastname: new FormControl('', Validators.required),
          phone: new FormControl('', Validators.required),
          country: new FormControl('', Validators.required)

        });
  }

  registerUser () {
    // logout before registration due to header interception
    this.authService.logout();

    // create new user to be saved
    if (this.registerForm.valid) {

      const user = {
        email: this.registerForm.controls['email'].value,
        firstName: this.registerForm.controls['firstname'].value,
        lastName: this.registerForm.controls['lastname'].value,
        phone: this.registerForm.controls['phone'].value,
        username: this.registerForm.controls['username'].value,
        country: {
          name: this.registerForm.controls['country'].value,
        },
        password: this.registerForm.controls['password'].value,
      };

        this.authService.register(user).subscribe(
          () => {
            this.registerMessage = 'registered successful';
            this.snackbar.open(this.registerMessage, 'Ok', {
              duration: 1500
            });
            this.router.navigate(['/login']);
          },
          (error: HttpErrorResponse) => {
            this.snackbar.open(
              (error.error as RestResponseSPA<void>).message,
              '',
              {
                duration: 1500
              }
            );
          }
        );
    } else {
      this.registerMessage = 'Form not valid';
      this.snackbar.open(this.registerMessage);
    }
  }

  goToLoginPage () {
    this.router.navigate(['/']);

  }

}

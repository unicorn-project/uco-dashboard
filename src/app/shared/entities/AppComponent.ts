import { HealthCheck } from './HealthCheck';
import { Interface } from './Interfaces';
import { EnvironmentalVariable } from './EnvironmentalVariables';
import { GraphLink } from './GraphLinks';

export class AppComponent {

    id: number;
    name: string;
    scaling: string;
    architecture: string;
    capabilityAdds: string[];
    capabilityDrops: string[];
    command: string;
    publicComponent: boolean;
    dockerCredentialsUsing:	boolean;
    dockerCustomRegistry: boolean;
    dockerImage: string;
    dockerRegistry: string;
    dockerUsername: string;
    hexID: string;
    hostname: string;
    lfInterfaceType: string;
    iconBase64: string;
    iconContent: string;
    iconContentType: string;
    iconFilename: string;
    iconPath: string;
    dateCreated: Date;
    lastModified: Date;

    environmentalVariables: EnvironmentalVariable[];
    requirement: ComponentRequirement;
    healthCheck: HealthCheck;
    exposedInterfaces: Interface[];
    requiredInterfaces: GraphLink[];

    constructor(id: number,
        name: string,
        scaling: string,
        architecture: string,
        publicComponent: boolean,
        dockerRegistry: string,
        dockerImage: string,
        iconContentType: any,
        iconFilename: string,
        lfInterfaceType: string,
        iconContent: any,
        iconBase64: any,
        iconPath: string,
        dateCreated: Date,
        lastModified: Date) {

        this.id = id;
        this.name = name;
        this.scaling = scaling;
        this.architecture = architecture;
        this.publicComponent = publicComponent;
        this.dockerRegistry = dockerRegistry;
        this.dockerImage = dockerImage;
        this.iconContentType = iconContentType;
        this.iconFilename = iconFilename;
        this.lfInterfaceType = lfInterfaceType;
        this.iconContent = iconContent;
        this.iconBase64 = iconBase64;
        this.iconPath = iconPath;
        this.dateCreated = dateCreated;
        this.lastModified = lastModified;

    }
}

export class ComponentRequirement {
    requirementID: number;
    vCPUs: number;
    ram: number;
    storage: number;
    hypervisorType: string;
    gpuRequired: boolean;
    dateCreated: Date;
    lastModified: Date;
}

export class ComponentLabelsOb {
    _embedded: ComponentLabelsEmbedded;
}

export class ComponentLabelsEmbedded {
    labels: ComponentLabel[];
}

export class ComponentLabel {
    labelID: number;
    name: string;
    dateCreated: Date;
    lastModified: Date;
}

import { ComponentNode } from './ComponentNode';
import { GraphLinkNode } from './GraphLinks';
import { User } from './User';

export class Application {

    id: number;
    dateCreated: Date;
    lastModified: Date;
    name: string;
    hexID: string;
    publicApplication: boolean;

    organization: string;
    // organization: Organization; // TODO: create entity?
    // TODO: PUT requires object with foreign key and not string, doesn't it?

    allowEdit: boolean;
    allowDelete: boolean;

    componentNodes: ComponentNode[];
    graphLinkNodes: GraphLinkNode[];
    user: User;

    constructor(name: string, publicApplication: boolean) {
        this.name = name;
        this.publicApplication = publicApplication;
    }
}

// TODO: what to do with it? is not present in the backend at all
export class ApplicationOwner {

    name: string;

    constructor(name: string) {
        this.name = name;
    }
}

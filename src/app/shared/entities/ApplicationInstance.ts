import { Application } from './Application';
import { ComponentNodeInstance } from './ComponentNodes';
import { Provider } from './Provider';
import { User } from './User';

export class ApplicationInstance {
    applicationInstanceID: number;
    dateCreated: Date;
    dateDeployed: Date;
    deploymentTimestamp: number;
    description: string;
    grafanaDashboardUUID: string;
    lastModified: Date;
    name: string;
    hexID: string;
    applicationName: string;
    // provider: number;
    // slice: number;
    overlay: boolean;
    status: string;
    application: Application;
    applicationInstanceQuota: ApplicationInstanceQuota;
    componentNodeInstances: ComponentNodeInstance[];
    // constraints: Constraint[]; // TODO: create entity?
    // graphLinkNodeInstances: GraphLinkNodeInstance[]; // TODO: create entity?
    // organization: Organization[]; // TODO: create entity?
    // profiles: Profile[]; // TODO: create entity?
    provider: Provider;
    // runtimePolicies: RuntimePolicy[] // TODO: create entity?
    // securityPolicies: SecurityPolicy[] // TODO: create entity?
    // slice: Slice; // TODO: create entity?
    user: User;
    _links: {
        self: {
            href: string;
        },
        applicationInstance: {
            href: string;
        },
        componentNodeInstances: {
            href: string;
        },
        applicationInstanceQuota: {
            href: string;
        },
        application: {
            href: string;
        },
        user: {
            href: string;
        },
        provider: {
            href: string;
        },
        constraints: {
            href: string;
        },
        graphLinkNodeInstances: {
            href: string;
        },
        runtimePolicies: {
            href: string;
        }
    };

    constructor(name: string,
        applicationName: string
        ) {
            this.name = name;
            this.applicationName = applicationName;
        }
}

// Instance Quota
export class ApplicationInstanceQuotaOb {
    _embedded: EmbeddedInstanceQuota;
}

export class EmbeddedInstanceQuota {
    applicationInstanceQuota: ApplicationInstanceQuota[];
}

export class ApplicationInstanceQuota {
    id: number;
    virtualCPUs: string;
    memory: string;
    storage: string;
    dateCreated: Date;
    lastModified: Date;
    _links: {
        applicationInstance: {
            href: string;
            templated: boolean;
        }
    };
}

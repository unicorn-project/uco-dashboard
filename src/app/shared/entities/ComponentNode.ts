import { Application } from './Application';
import { AppComponent as ApplicationComponent } from './AppComponent';

export class ComponentNode {
    application: Application;
    component: ApplicationComponent;

    componentNodeID: number;
    name: string;
    hexID: string;
}

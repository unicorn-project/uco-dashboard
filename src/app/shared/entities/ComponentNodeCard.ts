export class ComponentNodeCard {
    name: string;
    componentName: string;
    componentNodeID: number;
    ssh: string;
    provider: string;
    region: string;
    ips: boolean;
    ids: boolean;
    ipsrules: string[];
    minWorker: number;
    maxWorker: number;
    plugins: string[];
    http: string;
    healthCommand: string;
    healthTimeInterval: string;
    overrideCommand: string;
    interfaceName: string;
    interfacePort: number;
    environmentKey: string;
    environemntValue: string;
    devices: string[];


    constructor(
        name: string,
        componentName: string,
        componentNodeID: number,
        ssh: string,
        provider: string,
        region: string,
        ips: boolean,
        ids: boolean,
        ipsrules: string[],
        minWorker: number,
        maxWorker: number,
        plugins: string[],
        http: string,
        healthCommand: string,
        healthTimeInterval: string,
        overrideCommand: string,
        interfaceName: string,
        interfacePort: number,
        environmentKey: string,
        environemntValue: string,
        devices: string[]
        ) {
            this.name = name;
            this.componentName = componentName;
            this.componentNodeID = componentNodeID;
            this.ssh = ssh;
            this.provider = provider;
            this.region = region;
            this.ids = ids;
            this.ips = ips;
            this.ipsrules = ipsrules;
            this.minWorker = minWorker;
            this.maxWorker = maxWorker;
            this.plugins = plugins;
            this.http = http;
            this.healthCommand = healthCommand;
            this.healthTimeInterval = healthTimeInterval;
            this.overrideCommand = overrideCommand;
            this.interfaceName = interfaceName;
            this.interfacePort = interfacePort;
            this.environmentKey = environmentKey;
            this.environemntValue = environemntValue;
            this.devices = devices;
        }
}

export class ComponentNodeInstanceIP {
    componentNodeInstanceIPID: number;
    componentNodeInstanceID: number;
    applicationInstanceID: number;
    graphInstanceID: number;
    componentNodeInstanceName: string;
    ip: string;
    type: string;
    network: string;
    dateCreated: Date;
    lastModified: Date;
}

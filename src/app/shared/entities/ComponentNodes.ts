import { ComponentNode } from './ComponentNode';
import { ComponentNodeInstanceIP } from './ComponentNodeInstanceIP';

// ComponentNodeInstances

export class ComponentNodeInstanceOb {
    _embedded: EmbeddedComponentNodeInstances;
}

export class EmbeddedComponentNodeInstances {
    componentNodeInstances: ComponentNodeInstance[];
}

export class ComponentNodeInstance {
    componentNodeInstanceID: number;
    name: string;
    hexID: string;
    command: string;
    statusIDS: boolean;
    statusIPS: boolean;
    minimumWorkers: number;
    maximumWorkers: number;
    dateCreated: Date;
    lastModified: Date;
    loadBalancer: boolean;

    componentNode: ComponentNode;
    componentNodeInstanceIPs: ComponentNodeInstanceIP[];
    componentNodeInstanceStatuses: ComponentNodeInstanceStatus[];

    _links: {
        self: {
            href: string;
        },
        applicationInstance: {
            href: string;
            templated: boolean
        },
        componentNode: {
            href: string;
            templated: boolean
        },
        provider: {
            href: string;
        },
        flavorInstance: {
            href: string;
        },
        loadBalancedBy: {
            href: string;
        },
        pluginInstances: {
            href: string;
        },
        healthCheckInstance: {
            href: string;
        },
        deviceInstances: {
            href: string;
        },
        sshKey: {
            href: string;
        },
        interfaceInstances: {
            href: string;
        },
        componentNodeInstanceAlerts: {
            href: string;
        },
        environmentalVariableInstances: {
            href: string;
        },
        volumeInstances: {
            href: string;
        },
        componentNodeInstanceIPs: {
            href: string;
        },
        iDRuleSetInstances: {
            href: string;
        },
        locationInstances: {
            href: string;
        },
        componentNodeInstanceStatuses: {
            href: string;
        }
    };
}

export class ComponentNodeInstanceStatus {
    componentNodeInstanceStatusID: number;
    componentNodeInstanceID: number;
    componentNodeInstanceName: string;
    applicationInstanceID: number;
    reportedChange: string;
    message: string;
    status: string;
    dateCreated: Date;
    lastModified: Date;

    // Not present in any of REST responses, so probably not needed to be here
    // applicationInstance: ApplicationInstance;
    // componentNodeInstance: ComponentNodeInstance;
}

export class ComponentNodeInstanceAlertsOb {
    _embedded: EmbeddedCompNodeInstAlert;
}

export class EmbeddedCompNodeInstAlert {
    componentNodeInstanceAlerts: ComponentNodeInstanceAlert[];
}

export class ComponentNodeInstanceAlert {
    componentNodeInstanceAlertID: number;
    dateCreated: Date;
    lastModified: Date;
    message: string;
    status: string;
}

export class EnvironmentalVariablesInstanceOb {
    _embedded: EmbeddedEnvironmentalVariable;
}

export class EmbeddedEnvironmentalVariable {
    environmentalVariableInstances: EnvironmentalVariableInstance[];
}

export class EnvironmentalVariableInstance {
    environmentalVariableInstanceID: number;
    key: string;
    value: string;
    dateCreated: Date;
    lastModified: Date;
}

export class CountryOb {
    _embedded: Embedded;
}

export class Embedded {
    countries: Country;
}

export class Country {
    id: number;
    name: string;
    alpha2: string;
    alpha3: string;
    countryCode: string;
    iso3166: string;
    region: string;
    subRegion: string;
    regionCode: string;
    subRegionCode: string;
    dateCreated: Date;
}

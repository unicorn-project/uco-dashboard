export class DashboardConfigDataOb {
    dashboardConfigData: DashboardConfigData[];
}

export class DashboardConfigData {
    seeAppInstanceTable: boolean;
    seeAppTable: boolean;
    seeCompTable: boolean;
    seeNoAppInstances: boolean;
    seeNoApps: boolean;
    seeNoComponents: boolean;
    seeNoPublicComponents: boolean;
    seeNoPrivateComponents: boolean;
    seeNoPublicApps: boolean;
    seeNoPrivateApps: boolean;

    constructor(
        seeAppInstanceTable: boolean,
        seeAppTable: boolean,
        seeCompTable: boolean,
        seeNoAppInstances: boolean,
        seeNoApps: boolean,
        seeNoComponents: boolean,
        seeNoPublicComponents: boolean,
        seeNoPrivateComponents: boolean,
        seeNoPublicApps: boolean,
        seeNoPrivateApps: boolean) {
            this.seeAppInstanceTable = seeAppInstanceTable;
            this.seeAppTable = seeAppTable;
            this.seeCompTable = seeCompTable;
            this.seeNoAppInstances = seeNoAppInstances;
            this.seeNoApps = seeNoApps;
            this.seeNoComponents = seeNoComponents;
            this.seeNoPublicComponents = seeNoPublicComponents;
            this.seeNoPrivateComponents = seeNoPrivateComponents;
            this.seeNoPublicApps = seeNoPublicApps;
            this.seeNoPrivateApps = seeNoPrivateApps;
        }
}

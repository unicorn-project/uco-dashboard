export class DeviceOb {
    _embedded: EmbeddedDevice;
}

export class EmbeddedDevice {
    devices: Device[];
}

export class Device {
    id: number;
}

export class DeviceInstanceOb {
    _embedded: EmbeddedDeviceInstance;
}

export class EmbeddedDeviceInstance {
    deviceInstances: DeviceInstance[];
}

export class DeviceInstance {
    id: number;
}

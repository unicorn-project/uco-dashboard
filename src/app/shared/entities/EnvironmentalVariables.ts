export class EnvironmentalVariablesOb {
    _embedded: EmbeddedEnvironmentalVariables;
}

export class EmbeddedEnvironmentalVariables {
    environmentalVariables: EnvironmentalVariable[];
}

export class EnvironmentalVariable {
    environmentalVariableID: number;
    key: string;
    value: string;
    dateCreated: Date;
    lastModified: Date;
}

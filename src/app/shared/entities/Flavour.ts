export class FlavourInstance {
    flavourID: number;
    vCPUs: number;
    ram: number;
    storage: number;
    dateCreated: Date;
    lastModified: Date;
}

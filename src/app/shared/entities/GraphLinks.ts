import { Interface } from './Interfaces';

export class GraphLinksOb {
    _embedded: EmbeddedGraphLink;
}

export class EmbeddedGraphLink {
    graphLinks: GraphLink[];
}

export class GraphLink {
    graphLinkID: number;
    friendlyName: string;
    interfaceObj: Interface;
    dateCreated: Date;
    lastModified: Date;
}

// Graph Link Nodes
export class GraphLinkNodesOb {
    _embedded: EmbeddedGraphLinkNodes;
}

export class EmbeddedGraphLinkNodes {
    graphLinkNodes: GraphLinkNode[];
}

export class GraphLinkNode {
    graphLinkNodeID: number;
    dateCreated: Date;
    lastModified: Date;
    _links: {
        self: {
            href: string;
        },
        graphLinkNode: {
            href: string;
            templated: boolean;
        },
        graphLink: {
            href: string;
            templated: boolean;
        },
        application: {
            href: string;
            templated: boolean;
        },
        componentNodeFrom: {
            href: string;
            templated: boolean;
        },
        componentNodeTo: {
            href: string;
            templated: boolean;
        }
    };
}

export class HealthCheckOb {
    _embedded: EmbeddedHealthCheck;
}

export class EmbeddedHealthCheck {
    healthChecks: HealthCheck[];
}

export class HealthCheck {
    healthCheckID: number;
    name: string;
    httpURL: string;
    args: string;
    interval: string;
    dateCreated: Date;
    lastModified: Date;
    _links: {
        healthCheck: {
            href: string;
        },
        component: {
            href: string;
        }
    };
}

export class HealthCheckInstanceOb {
    _embedded: EmbeddedHealthCheckInstance;
}

export class EmbeddedHealthCheckInstance {
    healthCheckInstances: HealthCheckInstance[];
}

export class HealthCheckInstance {
    healthCheckInstanceID: number;
    name: string;
    httpURL: string;
    args: string;
    interval: string;
    dateCreated: Date;
    lastModified: Date;
    _links: {
        componentNodeInstance: {
            href: string;
        },
        healthCheck: {
            href: string;
        }
    };
}

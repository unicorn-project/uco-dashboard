export class IDRuleSetOb {
    _embedded: EmbeddedIDRuleSet;
}

export class EmbeddedIDRuleSet {
    iDRuleSets: IDRuleSet [];
}

export class IDRuleSet {
    ruleSetID: number;
    name: string;
    dateCreated: Date;
    lastModified: Date;
    predefinedRuleSet: boolean;
    rulesCounter: string;
    _links: {
        self: {
            href: string;
        };
        iDRuleSet: {
            href: string;
        };
        user: {
            href: string;
        };
        iDRules: {
            href: string;
        }
    };

    constructor (
        name: string,
        predefinedRuleSet: boolean
    ) {
        this.name = name;
        this.predefinedRuleSet = predefinedRuleSet;
    }
}

export class IDRulesOb {
    _embedded: EmbeddedIDRules;
}

export class EmbeddedIDRules {
    iDRules: IDRule [];
}

export class IDRule {
    ruleID: number;
    name: string;
    dateCreated: Date;
    lastModified: Date;
    _links: {
        self: {
            href: string;
        };
        iDRule: {
            href: string;
        };
        idRuleSet: {
            href: string;
        }
    };

    constructor (
        name: string
    ) {
        this.name = name;
    }
}

export class IDSRuleSetInstanceOb {
    _embedded: EmbeddedIDSRuleSetInstance;
}

export class EmbeddedIDSRuleSetInstance {
    iDRuleSetInstances: IDRuleSetInstance[];
}

export class IDRuleSetInstance {
    ruleSetInstanceID: number;
    name: string;
    dateCreated: Date;
    lastModified: Date;
    idRuleSetID: number;
}

export class InterfaceOb {
    _embedded: EmbeddedInterface;
    page: InterfacePage;
}

export class InterfacePage {
    size: number;
    totalElements: number;
    totalPages: number;
    number: number;
}

export class EmbeddedInterface {
    interfaces: Interface[];
}

export class Interface {
    interfaceID: number;
    name: string;
    port: string;
    vna: string;
    interfaceType: string;
    transmissionProtocol: string;
    componentID: number;
    dateCreated: Date;
    lastModified: Date;

    constructor (interfaceID: number,
        name: string,
        port: string,
        interfaceType: string,
        transmissionProtocol: string,
        dateCreated: Date,
        lastModified: Date) {

            this.interfaceID = interfaceID;
            this.name = name;
            this.port = port;
            this.interfaceType = interfaceType;
            this.transmissionProtocol = transmissionProtocol;
            this.dateCreated = dateCreated;
            this.lastModified = lastModified;
        }
}

export class InterfaceInstanceOb {
    _embedded: EmbeddedInterfaceInstance;
}

export class EmbeddedInterfaceInstance {
    interfaceInstances: InterfaceInstance[];
}

export class InterfaceInstance {
    interfaceInstanceID: number;
    name: string;
    port: string;
    dateCreated: Date;
    lastModified: Date;

    constructor (interfaceInstanceID: number,
        name: string,
        port: string) {
            this.interfaceInstanceID = interfaceInstanceID;
            this.name = name;
            this.port = port;
        }
}

/**
 * Does not exist in reality, is here to abstract response format of `/list` calls.
 */
export class ListReturnObject<T> {
    content: T[];
    pageable: {
        sort: {
            sorted: boolean;
            unsorted: boolean;
        };
        pageSize: number;
        pageNumber: number;
        offset: number;
        paged: boolean;
        unpaged: boolean;
    };
    last: boolean;
    totalPages: number;
    totalElements: number;
    first: boolean;
    sort: {
        sorted: boolean;
        unsorted: boolean;
    };
    numberOfElements: number;
    size: number;
    number: number;
}

export class MetricOb {
    _embedded: EmbeddedMetric;
}

export class EmbeddedMetric {
    metrics: Metric[];
}

export class Metric {
    metricID: number;
    name: string;
    friendlyName: string;
    unit: string;
    dateCreated: Date;
    lastModified: Date;
    _links: {
        plugin: {
            href: string;
        }
    };

    constructor (metricID: number,
        name: string,
        friendlyName: string,
        unit: string) {
            this.metricID = metricID;
            this.name = name;
            this.friendlyName = friendlyName;
            this.unit = unit;
        }
}

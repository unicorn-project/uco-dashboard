export class NotificationOb {
    _embedded: EmbeddedNotification;
}

export class EmbeddedNotification {
    notifications: Notification[];
}

export class Notification {
    notificationID: number;
    classes: string;
    component_type: string;
    is_dismissed: number;
    entity_id: string;
    message: string;
    notification_type: string;
    is_read: number;
    redirect_url: string;
    retries: number;
    is_seen: number;
    status: number;
    timestamp: number;
    when_date: Date;
    notification_template: number;
    receiver: number;
    sender: number;
    user: number;
}

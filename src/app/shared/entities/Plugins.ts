export class PluginOb {
    _embedded: EmbeddedPlugin;
    page: PluginPage;
}

export class PluginPage {
    number: number;
    size: number;
    totalElements: number;
    totalPages: number;
}

export class EmbeddedPlugin {
    plugins: Plugin[];
}

export class Plugin {
    pluginID: number;
    name: string;
    moduleName: string;
    downloadURL: string;
    pluginType: string;
    port: string;
    endpoint: string;
    publicPlugin: boolean;
    immutablePlugin: boolean;
    defaultPlugin: boolean;
    dateCreated: Date;
    lastModified: Date;
    metricsCounter: null;
    _links: {
        user: {
            href: string;
        },
        metrics: {
            href: string;
        }
    };
}

export class AddPlugin {
    pluginID: number;
    name: string;
    moduleName: string;
    downloadURL: string;
    pluginType: string;
    port: string;
    endpoint: string;
    publicPlugin: boolean;
    immutablePlugin: boolean;
    defaultPlugin: boolean;

    constructor(
        pluginID: number,
        name: string,
        moduleName: string,
        downloadURL: string,
        pluginType: string,
        port: string,
        endpoint: string,
        publicPlugin: boolean,
        immutablePlugin: boolean,
        defaultPlugin: boolean
    ) {
        this.pluginID = pluginID;
        this.name = name;
        this.moduleName = moduleName;
        this.downloadURL = downloadURL;
        this.pluginType = pluginType;
        this.port = port;
        this.endpoint = endpoint;
        this.publicPlugin = publicPlugin;
        this.immutablePlugin = immutablePlugin;
        this.defaultPlugin = defaultPlugin;
    }
}

export class PluginInstanceOb {
    _embedded: EmbeddedPluginInstance;
    page: PluginPage;
}

export class EmbeddedPluginInstance {
    pluginInstances: PluginInstance[];
}

export class PluginInstance {
    pluginInstanceID: number;
    name: string;
    moduleName: string;
    immutablePlugin: boolean;
    deletedPlugin: false;
    dateCreated: Date;
    lastModified: Date;
}

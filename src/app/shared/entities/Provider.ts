export class ProviderOb {
    _embedded: EmbeddedProvider;
}

export class EmbeddedProvider {
    providers: Provider[];
}

export class Provider {
    providerID: number;
    name: string;
    endpoint: string;
    proxy: string;
    status: string;
    username: string;
    password: string;
    publicKey: string;
    privateKey: string;
    imageID: string;
    networkID: string;
    externalNetworkID: string;
    nexusIP: string;
    domain: string;
    meshIdentifier: string;
    project: string;
    internalProvider: boolean;
    defaultProvider: boolean;
    dateCreated: Date;
    lastModified: Date;

    providerType: ProviderType;

    _links: {
        providerType: {
            href: string;
        },
        user: {
            href: string;
        },
        providerQuota: {
            href: string;
        }
        regions: {
            href: string;
        }
    };
}

export class ProviderTypesOb {
    _embedded: EmbeddedProviderTypes;
}

export class EmbeddedProviderTypes {
    providerTypes: ProviderType[];
}

export class ProviderType {
    id: number;
    name: string;
    friendlyName: string;
    adapterImplementation: string;
    enabled: boolean;
    dateCreated: Date;
    lastModified: Date;
}

export class AddProvider {
    canSubmit: boolean;
    defaultProvider: string;
    domain: string;
    endpoint: string;
    imageID: string;
    meshIdentifier: string;
    name: string;
    networkID: string;
    nexus: string;
    password: string;
    privateKey: string;
    project: string;
    providerID: number;
    providerType: string;
    proxy: string;
    publicKey: string;
    username: string;

    constructor (
        canSubmit: boolean,
        defaultProvider: string,
        domain: string,
        endpoint: string,
        imageID: string,
        meshIdentifier: string,
        name: string,
        networkID: string,
        nexus: string,
        password: string,
        privateKey: string,
        project: string,
        providerID: number,
        providerType: string,
        proxy: string,
        publicKey: string,
        username: string
    ) {
        this.canSubmit = canSubmit;
        this.defaultProvider = defaultProvider;
        this.domain = domain;
        this.endpoint = endpoint;
        this.imageID = imageID;
        this.meshIdentifier = meshIdentifier;
        this.name = name;
        this.networkID = networkID;
        this.nexus = nexus;
        this.password = password;
        this.privateKey = privateKey;
        this.project = project;
        this.providerID = providerID;
        this.providerType = providerType;
        this.proxy = proxy;
        this.publicKey = publicKey;
        this.username = username;
    }
}

export class AddNewProvider {
    canSubmit: boolean;
    defaultProvider: boolean;
    domain: string;
    endpoint: string;
    imageID: string;
    meshIdentifier: string;
    name: string;
    networkID: string;
    nexus: string;
    password: string;
    privateKey: string;
    project: string;
    providerType: string;
    proxy: string;
    publicKey: string;
    username: string;

    constructor (
        canSubmit: boolean,
        defaultProvider: boolean,
        domain: string,
        endpoint: string,
        imageID: string,
        meshIdentifier: string,
        name: string,
        networkID: string,
        nexus: string,
        password: string,
        privateKey: string,
        project: string,
        providerType: string,
        proxy: string,
        publicKey: string,
        username: string
    ) {
        this.canSubmit = canSubmit;
        this.defaultProvider = defaultProvider;
        this.domain = domain;
        this.endpoint = endpoint;
        this.imageID = imageID;
        this.meshIdentifier = meshIdentifier;
        this.name = name;
        this.networkID = networkID;
        this.nexus = nexus;
        this.password = password;
        this.privateKey = privateKey;
        this.project = project;
        this.providerType = providerType;
        this.proxy = proxy;
        this.publicKey = publicKey;
        this.username = username;
    }
}

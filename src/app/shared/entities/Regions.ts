export class RegionsOb {
    _embedded: EmbeddedRegion;
}

export class EmbeddedRegion {
    regions: Region[];
}

export class Region {
    regionID: number;
    name: string;
    dateCreated: Date;
    lastModified: Date;
}

export class LocationInstance {
    locationInstanceID: number;
    region: string;
    dateCreated: Date;
    lastModified: Date;
}

/**
 * Not that much existing entity, but is present in API docs, and represents JSON wrapper around response
 */
export class RestResponseSPA<T> {
    code: string;
    message: string;
    returnobject: T;
}

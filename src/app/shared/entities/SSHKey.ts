export class SSHKey {

    id: number;
    friendlyName: string;
    sshKey: string;
    defaultSSH: boolean;
    dateCreated: Date;
    lastModified: Date;
    _links: {
        user: string;
    };

    constructor(id: number, friendlyName: string, sshKey: string, defaultSSH: boolean) {
        this.id = id;
        this.friendlyName = friendlyName;
        this.sshKey = sshKey;
        this.defaultSSH = defaultSSH;
    }

}

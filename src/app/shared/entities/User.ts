import { Country } from './Countries';

export class UserOb {
  _embedded: UserEmbedded;
  _links: UserLinks;
}

export class UserEmbedded {
  users: User[];
}

export class UserLinks {
  profile: Link;
  search: Link;
  self: Link;
}

export class Link {
  href: string;
}

export class User {
  country: Country;
  organization: { // Organization class
    id: number,
    name: string;
    // ...
  };

  dateCreated: Date;
  email: string;
  enabled: boolean;
  firstLogin: boolean;
  firstName: string;
  id: number;
  lastName: string;
  notificationEmailEnabled: boolean;
  notificationWebEnabled: boolean;
  phone: string;
  role: string;
  username: string;
  password: string;

  constructor(
    id: number,
    username: string,
    firstName: string,
    lastName: string,
    email: string,
    phone: string) {

      this.id = id;
      this.username = username;
      this.email = email;
      this.phone = phone;
      this.firstName = firstName;
      this.lastName = lastName;
    }


}

export class UserCountry {
  name: string;
}

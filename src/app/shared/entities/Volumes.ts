export class VolumeOb {
    _embedded: EmbeddedVolume;
}

export class EmbeddedVolume {
    volumes: Volume[];
}

export class Volume {
    id: string;
}

export class VolumeInstanceOb {
    _embedded: EmbeddedVolumeInstance;
}

export class EmbeddedVolumeInstance {
    volumeInstances: VolumeInstance[];
}

export class VolumeInstance {
    id: number;
}

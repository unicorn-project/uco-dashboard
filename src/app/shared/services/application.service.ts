import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Application } from '../entities/Application';

import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../app.config';
import { User } from '../entities/User';
import { RestResponseSPA } from '../entities/RestResponseSPA';
import { ListReturnObject } from '../entities/ListReturnObject';
import { getPaginatedList } from './utils';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class ApplicationService {

  private appUrl = AppConfig.settings.apiServer.metadata + '/api/v1/application';

  constructor(private http: HttpClient) { }

  // get apps
  getApps(): Observable<Application[]> {
    // return getPaginatedList<Application>(this.http, this.appUrl + '/list');
    return this.http.post<RestResponseSPA<ListReturnObject<Application>>>(this.appUrl + '/list', {})
      .map(response => response.returnobject.content);
  }

  // save Apps
  saveApp(formData: any): Observable<void> {
    const requestBody = {
      name: formData.appName,
      publicApplication: formData.isPublic,
      componentNodes: formData.selectedComponents.map(selectedComponent => (
          {
            name: selectedComponent.name,
            component: {
              id: selectedComponent.id
            }
          }
        )
      ),
      graphLinkNodes: formData.graphLinkNodes,
    };

    return this.http.post<void>(this.appUrl, requestBody);
  }

  // update apps
  updateApp (id: number, app: Application): Observable<any> {
    // TODO: callers need to be updated on the format of ubitech
    return this.http.put<Application>(this.appUrl + '/' + id, app, httpOptions);

  }

/* required minimal payload for PUT
{
  "id": 14,
  "name": "343"
}
*/
  // get app by ID
  getAppById (applicationId: number): Observable<Application> {
    return this.http.get<RestResponseSPA<Application>>(this.appUrl + '/' + applicationId)
      .map(response => response.returnobject);
  }

  // get App by name
  getAppByName (appName: string): Observable<Application> {
    return this.http.post<RestResponseSPA<ListReturnObject<Application>>>(this.appUrl + '/list', {name: appName})
      .map(response => response.returnobject.content[0]);
  }

  // TODO: seems to be doable, but better shift ApplicationInstance part to new backend first and remove some shitty code from callers
  // get by Link from whereever
  getAppByLink (appInstanceID: number): Observable<Application> {
    return this.http.get<Application>(this.appUrl + '/fetchByInstance'); // TODO: unknown how does work - in documentation no parameters are specified
  }

  // delete application
  deleteApp (appId: number): Observable<Application> {
    return this.http.delete<Application>(this.appUrl + '/' + appId);
  }
}

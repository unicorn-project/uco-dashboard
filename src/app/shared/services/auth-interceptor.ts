import { Injectable } from '@angular/core';
import { HttpRequest, HttpInterceptor, HttpEvent, HttpHandler } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(public authService: AuthenticationService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const authToken = this.authService.getToken();

        let authReq;
        if (!authToken) {
            authReq = req;
        } else {
            authReq = req.clone({headers: req.headers.set('Authorization', '' + authToken)});
        }
        return next.handle(authReq);
    }

}

import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
// import 'rxjs/Rx';
// import { CookieService, CookieOptionsProvider } from 'ngx-cookie';
// import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks'

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { User } from '../entities/User';
import { AppConfig } from '../../app.config';
import { RestResponseSPA } from '../entities/RestResponseSPA';
// import { AppConfigContainer } from '../../app.config';

// const httpOptions = {
//  headers: new HttpHeaders({'Content-Type': 'application/json'})
// };

@Injectable()
export class AuthenticationService {

  // `AppConfig.settings.apiServer.metadata` should be used here too,
  // but access to the config file requires authentication,
  // so AuthenticationService (this class) is called, and requires the URL,
  // which in turn requires the config file to be loaded... --> circle dependency
  private authUrl = 'http://212.101.173.78:8080' + '/api/v1/auth';

  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor (private http: HttpClient) { }
// for old user management - working!
  /*login(username: string, password: string): Observable<User> {
    // send credentials and return fitting user
    return this.http.post<User>(this.authUrl, {username: username, password: password});

  }*/

  // for ubitech usermanagement without - working!
  login(username: string, password: string): Observable<boolean> {
    // send credentials and return fitting user
    return this.http.post<HttpResponse<any>>
      (this.authUrl + '/login', {username: username, password: password}, {headers: this.headers, observe: 'response'})/*;
  }*/
// tutorial version
/*login(username: string, password: string): Observable<boolean> {
  // send credentials and return fitting user
  return this.http.post<HttpResponse<any>>
    (this.authUrl + '/login', {username: username, password: password}, {headers: this.headers, observe: 'response'})*/
    .map((response: HttpResponse<any>) => {
  // login successful if there's a jwt token in the response
 const token = response.headers.get('Authorization'); // response.json() && response.json().token;
  if (token) {
      // store username and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));

      // return true to indicate successful login
      return true;
  } else {
      // return false to indicate failed login
      return false;
  }
}).catch((error: any) => Observable.throw(error.json().error || 'Server error'));
}

getToken(): String {
  const currentUser = JSON.parse(localStorage.getItem('currentUser'));
  const token = currentUser && currentUser.token;
  return token;
}

logout(): void {
    // clear token remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }

register (user): Observable<void> {
  return this.http.post<RestResponseSPA<void>>(this.authUrl + '/register', user)
    .map(response => response.returnobject);
}

getCurrentUser(): string {
  const current = JSON.parse(localStorage.getItem('currentUser'));
  const currentUser = current && current.username;
  return currentUser;
}

  getAuthenticatedUser(): Observable<User> {
    return this.http.get<RestResponseSPA<User>>(this.authUrl + '/user')
      .map(response => response.returnobject);
  }
}

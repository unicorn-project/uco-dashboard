import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { MonitoringCard } from '../../layout/monitoring/Monitoring';
import { DashboardCard } from '../../layout/dashboard/dashboardcard/DashboardCard';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class CardsService {

  private api = 'http://dashboard.unicorn.dcsresearch.cas.de:9090/cards';
  private dashboardApi = 'http://dashboard.unicorn.dcsresearch.cas.de:9090/dashboardcards';

  constructor(private http: HttpClient) { }

  getCards(): Observable<MonitoringCard[]> {
    return this.http.get<MonitoringCard[]>(this.api);
  }

  addCard(card: MonitoringCard) {
    return this.http.post<MonitoringCard>(this.api, card);
  }

  updateCard(id: number, card: MonitoringCard) {
    return this.http.put(this.api + '/' + id, card, httpOptions);
  }

  removeCard(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.api + '/' + id);
  }

  /********************** dashboard cards *********************/
  getDashboardCards(): Observable<DashboardCard[]> {
    return this.http.get<DashboardCard[]>(this.dashboardApi);
  }

  addDashboardCard(card: DashboardCard): Observable<DashboardCard> {
    return this.http.post<DashboardCard>(this.dashboardApi, card);
  }

  deleteDashboardCard(cardId: number): Observable<boolean> {
    return this.http.delete<boolean>(this.dashboardApi + '/' + cardId);
  }

}

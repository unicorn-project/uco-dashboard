import { Injectable,
  ComponentFactoryResolver,
  ViewChild,
  Input,
  ApplicationRef,
  Injector,
  EmbeddedViewRef,
  ViewContainerRef,
  ComponentRef} from '@angular/core';
import { ChartItem } from '../../layout/monitoring/chart/chartitem';
import { ChartComponent } from '../../layout/monitoring/chart/chart.component';

@Injectable()
export class ChartService {
  @Input() chart: ChartItem;
  @ViewChild('viewContainerRef', { read: ViewContainerRef }) VCR: ViewContainerRef;

  index = 0;
  componentsReferences = [];

  compRef: any;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private appRef: ApplicationRef,
              private injector: Injector) { }

  loadComponent(chart: ChartItem) {
    // this.currentChartIndex = (this.currentChartIndex + 1) % this.charts.length;
    // let chartItem = this.charts[this.currentChartIndex];


    console.log(chart);
    let compFac =
    this.componentFactoryResolver
      .resolveComponentFactory(chart.component); //.create(this.injector);

    let compRef: ComponentRef<ChartComponent> = this.VCR.createComponent(compFac);
    let currentComponent = <ChartComponent>compRef.instance;
    currentComponent.data = chart.data;
   // (<ChartComponent>compRef.instance).data = chart.data;

    currentComponent.selfRef = currentComponent;
    currentComponent.index = ++this.index;

    currentComponent.compInteraction = this;
    this.componentsReferences.push(compRef);

    // this.appRef.attachView(this.compRef.hostView);

    /* const domElem = (this.compRef.hostView as EmbeddedViewRef<any>)
      .rootNodes[0] as HTMLElement;

    document.body.appendChild(domElem);
    */
  }

  remove(index: number) {
    if (this.VCR.length < 1) {
      return;
    }

    let compRef = this.componentsReferences.filter(x => x.instance.index === index)[0];
    let component: ChartComponent = <ChartComponent>compRef.instance;

    let vcrIndex: number = this.VCR.indexOf(compRef);

    this.VCR.remove(vcrIndex);

    this.componentsReferences = this.componentsReferences.filter(x => x.instance.index !== index);
  }

}

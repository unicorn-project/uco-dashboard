import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppComponent as Component, ComponentRequirement, ComponentLabelsOb, AppComponent } from '../entities/AppComponent';
import { ComponentNode } from '../entities/ComponentNode';
import { ComponentNodeInstanceOb,
  ComponentNodeInstanceAlertsOb,
  ComponentNodeInstance, EnvironmentalVariablesInstanceOb, ComponentNodeInstanceAlert } from '../entities/ComponentNodes';
import { ListReturnObject } from '../entities/ListReturnObject';
import { RestResponseSPA } from '../entities/RestResponseSPA';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../app.config';
import { Interface, InterfaceInstanceOb, InterfaceOb } from '../entities/Interfaces';
import { PluginInstanceOb, PluginOb } from '../entities/Plugins';
import { VolumeInstanceOb, VolumeOb } from '../entities/Volumes';
import { DeviceInstanceOb, Device } from '../entities/Devices';
import { FlavourInstance } from '../entities/Flavour';
import { SSHKey } from '../entities/SSHKey';
import { Provider } from '../entities/Provider';
import { LocationInstance } from '../entities/Regions';
import { HealthCheckInstance, HealthCheck } from '../entities/HealthCheck';
import { IDSRuleSetInstanceOb } from '../entities/IdsRule';
import { EnvironmentalVariable } from '../entities/EnvironmentalVariables';
import { GraphLink, GraphLinksOb } from '../entities/GraphLinks';
import { getPaginatedList } from './utils';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})

export class ComponentsService {


  protected apiData = AppConfig.settings.apiServer.metadata;
  private componentUrl = AppConfig.settings.apiServer.metadata + '/api/v1/component';
  private appUrl = this.apiData + ':8080/api/v2/applications'; // TODO: muss raus
  private compNodeInstanceUrl = this.apiData + ':8080/api/v2/componentNodeInstances';
  private compNodeUrl = this.apiData + ':8080/api/v2/componentNodes';
  private interfaceURL = this.apiData + ':8080/api/v2/interfaces';

  constructor(private http: HttpClient) { }

    /*********************** components services ************************************/
    getComponents(): Observable<Component[]> {
      // return getPaginatedList<Component>(this.http, this.componentUrl + '/list');
      return this.http.post<RestResponseSPA<ListReturnObject<Component>>>(this.componentUrl + '/list', {})
        .map(response => response.returnobject.content);
    }

    addComponent(formData: any): Observable<void> {
      // Send as few data as possible
      const requestBody = {
        architecture: formData.architecture,
        name: formData.componentName,
        scaling: formData.elastProfile,
        publicComponent: formData.publicComponent,
        dockerRegistry: formData.dockerRegistry,
        dockerImage: formData.dockerImage,
        dockerCustomRegistry: formData.dockerCustomRegistry,
        dockerCredentialsUsing: formData.dockerCredentialsUsing,
        dockerUsername: formData.dockerUsername,
        dockerPassword: formData.dockerPassword,
        requirement: {
          gpuRequired: formData.gpuEnabled,
          hypervisorType: formData.hyperVisorType,
          ram: formData.ram,
          storage: formData.storage,
          vCPUs: formData.vCPUs
        },
        healthCheck: {
            args: formData.healthCommand,
            httpURL: formData.healthHttp,
            interval: formData.healthTimeInterval
        },
        environmentalVariables: formData.environmentalVariables,
        exposedInterfaces: formData.exposedInterfaces
      };

      return this.http.post<void>(this.componentUrl, requestBody);
    }

    updateComponent(id: number, formData: any): Observable<void> {
      const requestBody = {
        id: id,
        architecture: formData.architecture,
        name: formData.componentName,
        scaling: formData.elastProfile,
        publicComponent: formData.publicComponent,
        dockerImage: formData.dockerImage,
        requirement: {
          gpuRequired: formData.gpuEnabled,
          hypervisorType: formData.hyperVisorType,
          ram: formData.ram,
          storage: formData.storage,
          vCPUs: formData.vCPUs
        },
        healthCheck: {
            args: formData.healthCommand,
            httpURL: formData.healthHttp,
            interval: formData.healthTimeInterval
        },
        environmentalVariables: formData.environmentalVariables,
        exposedInterfaces: formData.exposedInterfaces,
        requiredInterfaces: formData.requiredInterfaces,
        devices: [],
      };

      return this.http.put<void>(this.componentUrl, requestBody, httpOptions);
    }

    delete(appId: number): Observable<AppComponent> {
      return this.http.delete<AppComponent>(this.componentUrl + '/' + appId);
    }

    addComponentEnvironmentVariablesByLink (envVar: {}, environmentVariableLink: string): Observable<EnvironmentalVariable> {
      return this.http.post<EnvironmentalVariable>(environmentVariableLink, envVar, httpOptions);
    }

    addComponentRequiredInterfacesByLink (reqInterface: {}, requiredInterfaceLink: string): Observable<GraphLinksOb> {
      return this.http.post<GraphLinksOb>(requiredInterfaceLink, reqInterface, httpOptions);
    }

    addComponentExposedInterfacesByLink (expInterface: {}, exposedInterfaceLink: string): Observable<InterfaceOb> {
      return this.http.post<InterfaceOb>(exposedInterfaceLink, expInterface, httpOptions);
    }

    addComponentMonitoringPluginByLink (monPlugin: {}, monitoringPluginLink: string): Observable<PluginOb> {
      return this.http.post<PluginOb>(monitoringPluginLink, monPlugin, httpOptions);
    }

    addComponentVolumesByLink (volume: {}, volumeLink: string): Observable<VolumeOb> {
      return this.http.post<VolumeOb>(volumeLink, volume, httpOptions);
    }

    addComponentDevicesByLink (device: {}, deviceLink: string): Observable<Device> {
      return this.http.post<Device>(deviceLink, device, httpOptions);
    }

    addComponentLabelsByLink (labels: {}, labelLink: string): Observable<ComponentLabelsOb> {
      return this.http.post<ComponentLabelsOb>(labelLink, labels, httpOptions);
    }

    // get component by id
    getComponentById (componentId: number): Observable<Component> {
      return this.http.get<RestResponseSPA<Component>>(this.componentUrl + '/' + componentId)
        .map((response: RestResponseSPA<Component>) => response.returnobject);
    }

    getComponentsSatisfyingInterface(componentId: number, interfaceId: number): Observable<Component[]> {
      return this.http.get<RestResponseSPA<Component[]>>(this.componentUrl + '/' + componentId + '/candidates/' + interfaceId)
        .map(response => response.returnobject);
    }

    // get component to componentNode
    getComponentToComponentNode (componentNodeID: number): Observable<Component> {
      return this.http.get<Component>(this.compNodeUrl + '/' + componentNodeID + '/component');
    }

    // get componentNode by link
    getComponentNodeByLink(componentNodeLink: string): Observable<ComponentNode> {
      return this.http.get<ComponentNode>(componentNodeLink);
    }

    // get componentNode by componentNodeInstances
    getComponentNodeToComponentNodeInstance(componentNodeInstanceID: number): Observable<ComponentNode> {
      return this.http.get<ComponentNode>(this.compNodeInstanceUrl + '/' + componentNodeInstanceID + '/componentNode');
    }

    // get component node instance by link
    getComponentNodeInstanceByLink (componentNodeInstancesLink: string): Observable<ComponentNodeInstanceOb> {
      return this.http.get<ComponentNodeInstanceOb> (componentNodeInstancesLink);
    }

    getComponentNodeInstanceAlertByLink (compNodeInstAlertLink: string): Observable<ComponentNodeInstanceAlertsOb> {
      return this.http.get<ComponentNodeInstanceAlertsOb> (compNodeInstAlertLink);
    }

    getComponentNodeInstanceByID (componentNodeInstanceID: number): Observable<ComponentNodeInstance> {
      return this.http.get<ComponentNodeInstance> (this.compNodeInstanceUrl + '/' + componentNodeInstanceID);
    }

    // patch component node instance
    patchComponentNodeInstance (patchCompNodeInst: {}, compNodeInstanceID: number): Observable<ComponentNodeInstance> {
      return this.http.patch<ComponentNodeInstance> (this.compNodeInstanceUrl + '/' + compNodeInstanceID, patchCompNodeInst, httpOptions);
    }

    // get componentNodeInstance interfaces by Link
    getComponentNodeInstanceInterfacesByLink (componentNodeInstanceInterfaceInstancesLink: string): Observable<InterfaceInstanceOb> {
      return this.http.get<InterfaceInstanceOb>(componentNodeInstanceInterfaceInstancesLink);
    }

    // patch interface instances
    patchComponentNodeInstanceInterfaceInstance (patchInterfaceInstance: {}, compNodeInstanceID: number): Observable<InterfaceInstanceOb> {
      return this.http
      .patch<InterfaceInstanceOb>(this.compNodeInstanceUrl + '/' + compNodeInstanceID + '/interfaceInstances', patchInterfaceInstance, httpOptions);
    }

    // get environmental variables
    getComponentNodeInstanceEnvironmentalVariablesByLink (compNodeInstanceEnvironmentalVariablesLink: string): Observable<EnvironmentalVariablesInstanceOb> {
      return this.http.get<EnvironmentalVariablesInstanceOb>(compNodeInstanceEnvironmentalVariablesLink);
    }

    // patch environmentalVariables
    patchComponentNodeInstanceEnvironementalVariables (patchEnvVar: {}, compNodeInstanceID: number):
       Observable<EnvironmentalVariablesInstanceOb> {
        return this.http
        .patch<EnvironmentalVariablesInstanceOb>(this.compNodeInstanceUrl + '/' + compNodeInstanceID + '/environmentalVariables', patchEnvVar, httpOptions);
    }

    // get plugin instances
    getComponentNodeInstancePluginInstancesByLink (compNodePluginInstanceLink: string): Observable<PluginInstanceOb> {
      return this.http.get<PluginInstanceOb>(compNodePluginInstanceLink);
    }

    // patch plugins
    patchComponentNodeInstancePluginInstances (patchPluginInstance: {}, compNodeInstanceID: number): Observable<PluginInstanceOb> {
      return this.http.patch<PluginInstanceOb>(this.compNodeInstanceUrl + '/' + compNodeInstanceID + '/pluginInstances', patchPluginInstance, httpOptions);
    }

    // get volume instances
    getComponentNodesInstanceVolumeInstanceByLink (compNodeVolumeInstanceLink: string): Observable<VolumeInstanceOb> {
      return this.http.get<VolumeInstanceOb>(compNodeVolumeInstanceLink);
    }

    // get device instances
    getComponentNodesInstanceDeviceInstanceByLink (compNodeDeviceInstanceLink: string): Observable<DeviceInstanceOb> {
      return this.http.get<DeviceInstanceOb>(compNodeDeviceInstanceLink);
    }

    // get flavour instances
    getComponentNodesInstanceFlavourInstanceByLink (compNodeFlavourInstanceLink: string): Observable<FlavourInstance> {
      return this.http.get<FlavourInstance>(compNodeFlavourInstanceLink);
    }

    // patch Flavor data
    patchComponentNodeInstanceFlavourInstance (patchFlavourInstance: {}, compNodeInstanceID: number): Observable<FlavourInstance> {
      return this.http.patch<FlavourInstance>(this.compNodeInstanceUrl + '/' + compNodeInstanceID + '/flavorInstance', patchFlavourInstance, httpOptions);
    }

    // get ssh key
    getComponentNodesInstanceSSHKeyByLink (compNodeSSHKeyLink: string): Observable<SSHKey> {
      return this.http.get<SSHKey>(compNodeSSHKeyLink);
    }

    // patch ssh key into database
    patchComponentNodesInstanceSSHKey (compNodeInstanceID: number, patchSSHKey: {}): Observable<SSHKey> {
      return this.http.patch<SSHKey>(this.compNodeInstanceUrl + '/' + compNodeInstanceID + '/sshKey', patchSSHKey, httpOptions);
    }

    // get provider
    getComponentNodesInstanceProviderByLink (compNodeProviderLink: string): Observable<Provider> {
      return this.http.get<Provider>(compNodeProviderLink);
    }
    // get location Instances
    getComponentNodesInstanceLocationInstanceByLink (compNodeLocationInstanceLink: string): Observable<LocationInstance> {
      return this.http.get<LocationInstance>(compNodeLocationInstanceLink);
    }

    // patch location instances
    patchComponentNodesInstanceLocationInstance (compNodeInstanceID: string, patchLocation: {}): Observable<LocationInstance> {
      return this.http.patch<LocationInstance>(this.compNodeInstanceUrl + '/' + compNodeInstanceID + '/locationInstances', patchLocation, httpOptions);
    }

    // get health check information
    getComponentNodeInstanceHealthCheckInstanceByLink (compNodeInstanceHealthCheckInstanceLink: string): Observable<HealthCheckInstance> {
      return this.http.get<HealthCheckInstance>(compNodeInstanceHealthCheckInstanceLink);
    }

    // patch health check
    patchComponentNodeInstanceHealthCheckInstance (patchHealthCheckInstance: {}, compNodeInstanceID: number): Observable<HealthCheckInstance> {
      return this.http
        .patch<HealthCheckInstance>(this.compNodeInstanceUrl + '/' + compNodeInstanceID + '/healthCheckInstance', patchHealthCheckInstance, httpOptions);
    }

    // get component node alerts
    getComponentNodeInstanceAlertsByLink (compNodeInstanceAlertLink: string): Observable<ComponentNodeInstanceAlert> {
      return this.http.get<ComponentNodeInstanceAlert>(compNodeInstanceAlertLink);
    }

    // get idsRuleSets By Link
    getComponentNodeInstanceIDSRuleSetsByLink (compNodeInstanceIDSRuleSetsLink: string): Observable<IDSRuleSetInstanceOb> {
      return this.http.get<IDSRuleSetInstanceOb>(compNodeInstanceIDSRuleSetsLink);
    }

    // patch rule set
    patchComponentNodeInstanceIDSRuleSets (patchRuleSet: {}, compNodeInstanceID: number): Observable<IDSRuleSetInstanceOb> {
      return this.http.patch<IDSRuleSetInstanceOb>(this.compNodeInstanceUrl + '/' + compNodeInstanceID + '/iDRuleSetInstances', patchRuleSet, httpOptions);
    }

    getInterfacesByPage(page: number): Observable<InterfaceOb> {
      return this.http.get<InterfaceOb>(this.interfaceURL + '?page=' + page);
    }
  }

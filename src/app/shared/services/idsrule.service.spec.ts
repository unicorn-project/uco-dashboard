import { TestBed } from '@angular/core/testing';

import { IdsruleService } from './idsrule.service';

describe('IdsruleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IdsruleService = TestBed.get(IdsruleService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AppConfig } from 'src/app/app.config';
import { Observable } from 'rxjs';
import { IDRulesOb, IDRule, IDRuleSetOb, IDRuleSet } from '../entities/IdsRule';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})

export class IdsruleService {

  protected apiData = AppConfig.settings.apiServer.metadata;
  private ruleSetUrl = this.apiData + ':8080/api/v2/iDRuleSets';
  private ruleUrl = this.apiData + ':8080/api/v2/iDRules';

  constructor(private http: HttpClient) { }

  // get rules
  getIdsRules(): Observable<IDRulesOb> {
    return this.http.get<IDRulesOb>(this.ruleUrl);
  }

  // getIdsRule by link
  getIdsRulesByLink(idsruleLink: string): Observable<IDRulesOb> {
    return this.http.get<IDRulesOb>(idsruleLink);
  }

  // delete idsrule
  deleteIDSRule(idsRuleID: number): Observable<IDRule> {
    return this.http.delete<IDRule>(this.ruleUrl + '/' + idsRuleID);
  }

  // getIDS Rulesets
  getIDSRuleSets(): Observable<IDRuleSetOb> {
    return this.http.get<IDRuleSetOb>(this.ruleSetUrl);
  }

  getIDSRuleSetByLink(idsRuleSetLink: string): Observable<IDRuleSet> {
    return this.http.get<IDRuleSet>(idsRuleSetLink);
  }

  getIDSRuleSetByID (idsRuleSetID: number): Observable<IDRuleSet> {
    return this.http.get<IDRuleSet>(this.ruleSetUrl + '/' + idsRuleSetID);
  }

  deleteIDSRuleSet(idsRuleSetID: number): Observable<IDRuleSet> {
    return this.http.delete<IDRuleSet>(this.ruleSetUrl + '/' + idsRuleSetID);
  }

  editIDSRulesOfIDSRuleSet(idsRules: IDRule[], idsRuleSetID: number): Observable<IDRuleSet> {
    return this.http.patch<IDRuleSet>(this.ruleSetUrl + '/' + idsRuleSetID + '/iDRules', idsRules, httpOptions);
  }

  addIDSRuleSet(idsRuleSet: {}): Observable<IDRuleSetOb> {
    return this.http.post<IDRuleSetOb>(this.ruleSetUrl, idsRuleSet, httpOptions);
  }
}

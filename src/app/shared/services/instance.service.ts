import { Injectable } from '@angular/core';
import { AppConfig } from 'src/app/app.config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApplicationInstance, ApplicationInstanceQuota } from '../entities/ApplicationInstance';
import { ListReturnObject } from '../entities/ListReturnObject';
import { RestResponseSPA } from '../entities/RestResponseSPA';
import { filter } from 'rxjs/operators';
import { ComponentNodeInstanceStatus } from '../entities/ComponentNodes';
import { getPaginatedList } from './utils';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class InstanceService {

  private applicationInstanceUrl = AppConfig.settings.apiServer.metadata + '/api/v1/applicationinstance';

  constructor(  private http: HttpClient) { }

  getAppInstances(): Observable<ApplicationInstance[]> {
    // return getPaginatedList<ApplicationInstance>(this.http, this.applicationInstanceUrl + '/list');
    return this.http.post<RestResponseSPA<ListReturnObject<ApplicationInstance>>>(this.applicationInstanceUrl + '/list', {})
      .map(response => response.returnobject.content);
  }

  getAppInstanceById(applicationInstanceId: number): Observable<ApplicationInstance> {
    return this.http.get<RestResponseSPA<ApplicationInstance>>(this.applicationInstanceUrl + '/' + applicationInstanceId)
      .map(response => response.returnobject);
  }

  getApplicationInstancesByApplicationId(applicationId: number): Observable<ApplicationInstance[]> {
    return this.getAppInstances()
      .map(applicationInstances => applicationInstances.filter(applicationInstance => applicationInstance.application.id == applicationId));
  }

  saveAppInstance (instance: {}): Observable<ApplicationInstance> {
    return this.http.post<ApplicationInstance>(this.applicationInstanceUrl, instance);
  }

  // get application instance quota by link
  getAppInstanceQuotaByLink (instanceQuotaLink: string): Observable<ApplicationInstanceQuota> {
    return this.http.get<ApplicationInstanceQuota> (instanceQuotaLink);
  }

  // search for instance by name
  searchInstanceByName (instanceName: string): Observable<ApplicationInstance> {
    return this.http.get<ApplicationInstance> (this.applicationInstanceUrl + '/search/findByName/?name=' + instanceName);
  }

  // delete instance
  deleteInstanceByID (instanceID: number): Observable<boolean> {
    return this.http.delete<boolean>(this.applicationInstanceUrl + '/' + instanceID);
  }

  deployInstanceByID (instanceID: number): Observable<ApplicationInstance> { // TODO: does it return ApplicationInstance or void?
    return this.http.post<ApplicationInstance>(this.applicationInstanceUrl + '/' + instanceID + '/request/deployment', {}, httpOptions);
  }

  undeploy(applicationInstance: ApplicationInstance): Observable<void> {
    return this.http.post<void>(this.applicationInstanceUrl + '/' + applicationInstance.applicationInstanceID + '/request/undeployment', {});
  }

  getApplicationInstanceStatuses(applicationInstanceId: number): Observable<ComponentNodeInstanceStatus[]> {
    return getPaginatedList<ComponentNodeInstanceStatus>(this.http, this.applicationInstanceUrl + '/' + applicationInstanceId + '/statuses');
  }
}

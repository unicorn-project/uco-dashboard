import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../../app.config';
import { Observable } from 'rxjs/Observable';
import { Interface } from '../entities/Interfaces';
import { ListReturnObject } from '../entities/ListReturnObject';
import { RestResponseSPA } from '../entities/RestResponseSPA';

@Injectable()
export class InterfaceService {

  private appUrl = AppConfig.settings.apiServer.metadata + '/api/v1/interface';

  constructor(private http: HttpClient) { }

  getInterfaces(): Observable<Interface[]> {
    return this.http.post<RestResponseSPA<ListReturnObject<Interface>>>(this.appUrl + '/list', {})
      .map(response => response.returnobject.content);
  }
}

import { Injectable, Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { DashboardConfigData } from '../entities/DashboardConfigData';

@Injectable()
export class LocalStorageService {

  private readonly DASHBOARD_DATA_STORAGE_KEY = 'dashboardData';

  constructor(@Inject(LOCAL_STORAGE) private localStorage: StorageService) { }

  getDashboardConfig(): DashboardConfigData {
    let dashboardConfig = this.localStorage.get(this.DASHBOARD_DATA_STORAGE_KEY) as DashboardConfigData;

    if (!dashboardConfig) {
       // Defaults for first launch (copied from mock data)
      dashboardConfig = new DashboardConfigData(true, true, true, true, true, true, true, true, true, null);

      this.editDashboardConfig(dashboardConfig);
    }

    return dashboardConfig;
  }

  editDashboardConfig(newConfigData: DashboardConfigData): void {
    this.localStorage.set(this.DASHBOARD_DATA_STORAGE_KEY, newConfigData);
  }
}

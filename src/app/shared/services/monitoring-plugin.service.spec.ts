import { TestBed } from '@angular/core/testing';

import { MonitoringPluginService } from './monitoring-plugin.service';

describe('MonitoringPluginService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MonitoringPluginService = TestBed.get(MonitoringPluginService);
    expect(service).toBeTruthy();
  });
});

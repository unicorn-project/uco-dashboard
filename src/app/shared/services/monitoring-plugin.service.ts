import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AppConfig } from 'src/app/app.config';
import { Observable } from 'rxjs';
import { PluginOb, Plugin, PluginInstanceOb } from '../entities/Plugins';
import { MetricOb } from '../entities/Metric';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class MonitoringPluginService {

  protected apiData = AppConfig.settings.apiServer.metadata;
  private pluginUrl = this.apiData + ':8080/api/v2/plugins?page=';
  private pluginUrlID = this.apiData + ':8080/api/v2/plugins';
  private metricUrl = this.apiData + ':8080/api/v2/metrics';
  private pluginInstanceUrlID = this.apiData + ':8080/api/v2/pluginInstances';
  private pluginInstanceUrl = this.apiData + ':8080/api/v2/pluginInstances?page=';

  constructor( private http: HttpClient) {}

  getPlugins(): Observable<PluginOb> {
    return this.http.get<PluginOb>(this.pluginUrl + 0);
  }

  getPluginsByPage(pageNumber: number): Observable<PluginOb> {
    return this.http.get<PluginOb>(this.pluginUrl + pageNumber);
  }

  getPluginsByID(pluginID: number): Observable<Plugin> {
    return this.http.get<Plugin>(this.pluginUrlID + '/' + pluginID);
  }

  getMetricsByLink(metricLink): Observable<MetricOb> {
    return this.http.get<MetricOb>(metricLink);
  }

  getMetrics(): Observable<MetricOb> {
    return this.http.get<MetricOb>(this.metricUrl);
  }

  getPluginInstances(): Observable<PluginInstanceOb> {
    return this.http.get<PluginInstanceOb>(this.pluginInstanceUrl);
  }

  getPluginInstancesByPage (pageNumber: number): Observable<PluginInstanceOb> {
    return this.http.get<PluginInstanceOb>(this.pluginInstanceUrl + pageNumber);
  }
}

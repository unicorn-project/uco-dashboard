import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from '../../../../node_modules/rxjs/Observable';
import { Monitoring } from '../../layout/monitoring/Monitoring';
import { AppConfig } from '../../app.config';

@Injectable()
export class MonitoringService {

  protected apiData = AppConfig.settings.devServer.monitoring;
  apiQuery: string;
  constructor(private http: HttpClient) { }

  getMonitoringData(): Observable<Monitoring> {
    return this.http.get<Monitoring>(this.apiData);
  }

  getMonitoringDataFromQuery(query: string): Observable<Monitoring> {
    this.apiQuery = query;
    return this.http.get<Monitoring>(this.apiQuery);
  }
  getMonitoringDataWithQuery(query: string): Observable<Monitoring> {
    // this.apiQuery = this.apiData + query;
    return this.http.get<Monitoring>(query);
  }

}

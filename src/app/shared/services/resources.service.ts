import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AppConfig } from 'src/app/app.config';
import { Observable } from 'rxjs';
import { Provider, ProviderOb, ProviderType, AddProvider, ProviderTypesOb, AddNewProvider } from '../entities/Provider';
import { RegionsOb } from '../entities/Regions';
import { ListReturnObject } from '../entities/ListReturnObject';
import { RestResponseSPA } from '../entities/RestResponseSPA';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ResourcesService {

  private resourcesUrl = AppConfig.settings.apiServer.metadata + '/api/v1/provider';
  private resourceTypeUrl = AppConfig.settings.apiServer.metadata + '/api/v1/providertype';

  constructor( private http: HttpClient) { }

  // get providers
  getProviders (): Observable<Provider[]> {
    return this.http.post<RestResponseSPA<ListReturnObject<Provider>>>(this.resourcesUrl + '/list', {})
      .map(response => response.returnobject.content);
  }

  // get provider by link
  getProviderByLink (providerLink: string): Observable<Provider> {
    return this.http.get<Provider>(providerLink);
  }

  // get provider by ID
  getProviderByID (providerID: number): Observable<Provider> {
    return this.http.get<RestResponseSPA<Provider>>(this.resourcesUrl + '/' + providerID)
    .map(response => response.returnobject);
  }

  // add new provider
  addProvider (provider: AddNewProvider): Observable<Provider> {
    return this.http.post<Provider>(this.resourcesUrl, provider);
  }

  // edit provider
  editProvider (provider: AddProvider): Observable<AddProvider> {
    const id = provider.providerID;
    console.log(provider);
    return this.http.patch<AddProvider>(this.resourcesUrl + '/' + id, provider, httpOptions);
  }

  // make default
  setToDefaultProvider (providerID: number): Observable<Provider> {
    return this.http.post<Provider>(this.resourcesUrl + '/' + providerID + '/default', {});
  }

  // delete provider
  deleteProvider (providerID: number): Observable<Provider> {
    return this.http.delete<Provider>(this.resourcesUrl + '/' + providerID);
  }

  // get provider type by link
  getProviderTypeByLink (providerTypeLink: string): Observable<ProviderType> {
    return this.http.get<ProviderType>(providerTypeLink);
  }

  getProviderTypes (): Observable<ProviderTypesOb> {
    return this.http.get<ProviderTypesOb>(this.resourceTypeUrl);
  }

  // get provider regions by Link
  getProviderRegionsByLink(regionLink: string): Observable<RegionsOb> {
    return this.http.get<RegionsOb>(regionLink);
  }

}

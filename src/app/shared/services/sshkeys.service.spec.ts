import { TestBed } from '@angular/core/testing';

import { SshkeysService } from './sshkeys.service';

describe('SshkeysService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SshkeysService = TestBed.get(SshkeysService);
    expect(service).toBeTruthy();
  });
});

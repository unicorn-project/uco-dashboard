import { Injectable } from '@angular/core';
import { AppConfig } from '../../app.config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SSHKey } from '../entities/SSHKey';
import { RestResponseSPA } from '../entities/RestResponseSPA';
import { ListReturnObject } from '../entities/ListReturnObject';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class SshkeysService {

  private sshkeyUrl = AppConfig.settings.apiServer.metadata + '/api/v1/sshkey';

  constructor( private http: HttpClient) { }

  // get ssh keys
  getAllSshKeys(): Observable<SSHKey[]> {
    return this.http.post<RestResponseSPA<ListReturnObject<SSHKey>>>(this.sshkeyUrl + '/list', {})
      .map(response => response.returnobject.content);
  }

  // add a new key
  addNewSSHKey(sshkey: {}): Observable<SSHKey> {
    return this.http.post<SSHKey>(this.sshkeyUrl, sshkey);
  }

  // update an existing key
  updateSSHKey(id: number, sshkey: SSHKey): Observable<SSHKey> {
    return this.http.put<SSHKey>(this.sshkeyUrl + '/' + id, sshkey, httpOptions);
  }

  // delete sshkey
  deleteSSHKey(id: number): Observable<SSHKey> {
    return this.http.delete<SSHKey>(this.sshkeyUrl + '/' + id);
  }

  // make default
  setToDefault(id: number): Observable<SSHKey> {
    return this.http.put<SSHKey>(this.sshkeyUrl + '/' + id + '/default', {});
  }
}

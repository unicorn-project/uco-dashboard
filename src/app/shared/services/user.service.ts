/**
 * Integration to REST service
 **/
import { Injectable } from '@angular/core';
import { User, UserOb } from '../entities/User';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import 'rxjs/add/operator/catch';
import { of } from 'rxjs/observable/of';

import { AuthenticationService } from './authentication.service';

import { Observable } from 'rxjs/Observable';
import { AppConfig } from '../../app.config';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class UserService {
  private usersUrl = AppConfig.settings.apiServer.metadata + '/api/v1/user';

  constructor(private http: HttpClient,
              private authService: AuthenticationService) { }

  // method to find all users
  findAllUsers(): Observable<UserOb> {
    return this.http.get<UserOb>(this.usersUrl);
  }

  // find user by id
  // todo: add error handling
  findById(id: number): Observable <User> {
    return this.http.get<User>(this.usersUrl + '/' + id);
  }

  // find user by username
  findByUsername(username: string): Observable<User> {
    return this.http.get<User>(this.usersUrl + '/' + username);
  }

  // save new user
  saveUser(user: User): Observable<User> {
  // todo: add error handling
    return this.http.post<User>(this.usersUrl, user);
  }

  // delete user by Id
  deleteUserById(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.usersUrl + '/' + id);
  }

  // update user
  // todo: add error handling
  updateUser(user): Observable<void> {
    return this.http.put<void>(this.usersUrl, user, httpOptions);
  }

  // handle error
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      // this.log('${operation} failed: ${error.message}');
      return of(result as T);
    };
  }

}

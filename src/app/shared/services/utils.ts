import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RestResponseSPA } from '../entities/RestResponseSPA';
import { ListReturnObject } from '../entities/ListReturnObject';

function getPaginatedListByPage<E>(httpClient: HttpClient, url: string, pageNumber: number): Observable<E[]> {
    return new Observable<E[]>(
        observer => {
            httpClient.get<RestResponseSPA<ListReturnObject<E>>>(
                url,
                { params: { page: pageNumber.toString() } }
            )
                .map(response => response.returnobject)
                .subscribe(
                    listReturnObject => {
                        const entities = listReturnObject.content;

                        // Second condition is actually not needed, just to be sure
                        if (listReturnObject.last || entities.length === 0) {
                            observer.next(entities);

                            return;
                        }

                        getPaginatedListByPage<E>(httpClient, url, ++pageNumber).subscribe(
                            furtherEntities => {
                                observer.next(entities.concat(furtherEntities));
                            }
                        );
                    }
                );
        }
    );
}

export const getPaginatedList = <E>(httpClient: HttpClient, url: string): Observable<E[]> => getPaginatedListByPage<E>(httpClient, url, 0);
